package th.co.trinityroots.gsbwarehouse.helper;

import android.content.Context;

import th.co.trinityroots.gsbwarehouse.utility.Constant;

/**
 * The class that managing a cache.
 */
public class CacheManager {

    public static void saveLoginCache(Context context, String uid, String password, String partnerJson) {
        SharedData.setStringForKey(context, Constant.UID_KEY, uid);
        SharedData.setStringForKey(context, Constant.PASSWORD_KEY, password);
        SharedData.setStringForKey(context, Constant.CURRENT_PARTNER_KEY, partnerJson);
    }

    public static void clearCache(Context context) {
        // Tmp data
        String serverUrlTmp = SharedData.getStringFromKey(context, Constant.SERVER_ADDRESS_KEY);
        String dbTmp = SharedData.getStringFromKey(context, Constant.DATABASE_KEY);
        String userTmp = SharedData.getStringFromKey(context, Constant.USERNAME_KEY);
        String langTmp = SharedData.getStringFromKey(context, Language.LANG_PREF_KEY);
        String latestUpdateMasterData = SharedData.getStringFromKey(context, Constant.LATEST_SYNC_MASTER_DATA_KEY);
        String latestSyncDO = SharedData.getStringFromKey(context, Constant.LATEST_SYNC_DO_KEY);

        SharedData.clearAll(context);

        // Rollback some data
        SharedData.setStringForKey(context, Constant.SERVER_ADDRESS_KEY, serverUrlTmp);
        SharedData.setStringForKey(context, Constant.DATABASE_KEY, dbTmp);
        SharedData.setStringForKey(context, Constant.USERNAME_KEY, userTmp);
        SharedData.setStringForKey(context, Language.LANG_PREF_KEY, langTmp);
        SharedData.setStringForKey(context, Constant.LATEST_SYNC_MASTER_DATA_KEY, latestUpdateMasterData);
        SharedData.setStringForKey(context, Constant.LATEST_SYNC_DO_KEY, latestSyncDO);
    }

    public static boolean isExpireWithTimestamp(long expireTs) {
        long currentTs = System.currentTimeMillis() / 1000;
        return currentTs > expireTs;
    }

}