package th.co.trinityroots.gsbwarehouse.controller.barcode_scanner;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import io.realm.RealmResults;
import th.co.trinityroots.gsbwarehouse.R;
import th.co.trinityroots.gsbwarehouse.adapter.DeliveryProductAdapter;
import th.co.trinityroots.gsbwarehouse.controller.outgoing_product.DeliveryScanListFragment;
import th.co.trinityroots.gsbwarehouse.controller.outgoing_product.OutgoingProductFragment;
import th.co.trinityroots.gsbwarehouse.enums.ProductAction;
import th.co.trinityroots.gsbwarehouse.helper.ColorHelper;
import th.co.trinityroots.gsbwarehouse.helper.DatabaseManager;
import th.co.trinityroots.gsbwarehouse.helper.LogUtil;
import th.co.trinityroots.gsbwarehouse.model.DeliveryOrder;
import th.co.trinityroots.gsbwarehouse.model.DeliveryProduct;
import th.co.trinityroots.gsbwarehouse.model.ProductTemplate;
import th.co.trinityroots.gsbwarehouse.model.Ratio;
import th.co.trinityroots.gsbwarehouse.model.ScannedDOProduct;
import th.co.trinityroots.gsbwarehouse.model.Setting;
import th.co.trinityroots.gsbwarehouse.utility.CallParams;
import th.co.trinityroots.gsbwarehouse.utility.Constant;

public class DOBarcodeScannerFragment extends Fragment implements View.OnClickListener {

    private TextView mProductNameTv;
    private TextView mProductQtyTv;
    private Button mRatioBtn[] = new Button[5];

    private DatabaseManager databaseManager;
    private RealmResults<Ratio> ratios;
    private RealmResults<ProductTemplate> products;
    private RealmResults<DeliveryProduct> deliveryProducts;
    private RealmResults<ScannedDOProduct> scannedDOProducts;
    private Setting setting;
    private DeliveryOrder standaloneDeliveryOrder;
    private DeliveryProductAdapter deliveryProductAdapter;

    private Integer multiplier;
    private Integer selectedRatioBtnIndex;

    private BroadcastReceiver mReceiver;
    private IntentFilter mFilter;

    public static DOBarcodeScannerFragment newInstance(DeliveryOrder standaloneDeliveryOrder) {
        DOBarcodeScannerFragment f = new DOBarcodeScannerFragment();
        f.standaloneDeliveryOrder = standaloneDeliveryOrder;
        return f;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mFilter = new IntentFilter();
        mFilter.addAction(Constant.SCANNER_INTENT_ACTION);

        mReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent != null &&
                        Constant.SCANNER_INTENT_ACTION.equalsIgnoreCase(intent.getAction()) &&
                        setting != null) {
                    String barcode = intent.getStringExtra(Constant.SCANNER_INTENT_STRING_EXTRA);
                    LogUtil.print("barcode: " + barcode);
                    processBarcode(barcode);
                }
                else {
                    showAlertScanBarcodeFail(getString(R.string.not_found_product_for_this_barcode));
                }
            }
        };
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_dobarcode_scanner, container, false);
        mProductNameTv = (TextView) rootView.findViewById(R.id.productNameTv);
        mProductQtyTv = (TextView) rootView.findViewById(R.id.productQtyTv);
        Button mCompleteBtn = (Button) rootView.findViewById(R.id.completeBtn);
        mRatioBtn[0] = (Button) rootView.findViewById(R.id.ratio1Btn);
        mRatioBtn[1] = (Button) rootView.findViewById(R.id.ratio2Btn);
        mRatioBtn[2] = (Button) rootView.findViewById(R.id.ratio3Btn);
        mRatioBtn[3] = (Button) rootView.findViewById(R.id.ratio4Btn);
        mRatioBtn[4] = (Button) rootView.findViewById(R.id.ratio5Btn);
        ListView deliveryProductLv = (ListView) rootView.findViewById(R.id.deliveryProductLv);

        mCompleteBtn.setOnClickListener(this);
        for (Button btn : mRatioBtn) {
            btn.setOnClickListener(this);
        }

        deliveryProductAdapter = new DeliveryProductAdapter(getActivity());
        deliveryProductLv.setAdapter(deliveryProductAdapter);

        return rootView;
    }

    private void processBarcode(String barcodeNumber) {
        int product_start_index = setting.getProduct_digit_start() - 1;
        int product_end_index = setting.getProduct_digit_end();

        String productBarcode;
        try {
            productBarcode = barcodeNumber.substring(product_start_index, product_end_index);
        } catch (Exception e) {
            Log.d("GSB", "Substring product fail: " + e.getLocalizedMessage());
            productBarcode = null;
        }

        ProductTemplate selectedProduct = null;
        for (ProductTemplate product : products) {
            if (product.getBarcode().equalsIgnoreCase(productBarcode)) {
                selectedProduct = product;
                break;
            }
        }

        // Check the barcode is valid
        if (selectedProduct != null) { // valid barcode
            // Find product's lot
            int lot_start_index = setting.getLot_digit_start() - 1;
            int lot_end_index = setting.getLot_digit_end();

            String lotNumber;
            try {
                lotNumber = barcodeNumber.substring(lot_start_index, lot_end_index);
            } catch (Exception e) {
                Log.d("GSB", "Substring lot fail: " + e.getLocalizedMessage());
                lotNumber = null;
            }

            if (lotNumber != null) { // valid lot's number
                ScannedDOProduct scannedDOProduct = updateScannedDOProduct(selectedProduct, lotNumber);
                if (scannedDOProduct != null) {
                    mProductNameTv.setText(selectedProduct.getName());
                    mProductQtyTv.setText(getString(R.string.total_scan, String.valueOf(scannedDOProduct.getScan_qty())));
                    deliveryProductAdapter.notifyDataSetChanged();
                }
            }
            else { // invalid lot's number
                showAlertScanBarcodeFail(getString(R.string.not_found_product_for_this_barcode));
            }
        }
        else { // invalid barcode
            showAlertScanBarcodeFail(getString(R.string.not_found_product_for_this_barcode));
        }
    }

    private ScannedDOProduct updateScannedDOProduct(ProductTemplate selectedProductTemplate, String lot_number) {
        // Find delivery product with product_id
        DeliveryProduct selectedDeliveryProduct = null;
        for (DeliveryProduct deliveryProduct : deliveryProducts) {
            if (deliveryProduct.getProduct_id().equals(selectedProductTemplate.getId())) {
                selectedDeliveryProduct = deliveryProduct;
                break;
            }
        }

        // Check scanned product is inside this DO
        if (selectedDeliveryProduct == null) {
            showAlertScanBarcodeFail(getString(R.string.no_product_in_this_do));
            return null;
        }

        // Check lot number must not be expired
        boolean expire = isLotExpired(lot_number, selectedDeliveryProduct.getGuide_sn());
        if (expire) {
            showAlertScanBarcodeFail(getString(R.string.expire_lot_number));
            return null;
        }

        for (ScannedDOProduct scannedDOProduct : scannedDOProducts) {
            if (scannedDOProduct.getDeliveryProduct().getProduct_id().equals(selectedProductTemplate.getId()) &&
                    scannedDOProduct.getLot_number().equalsIgnoreCase(lot_number)) {

                // Scan quantity must not more than product_uom_qty
                if (!canAddScannedQty(scannedDOProduct)) {
                    showAlertScanBarcodeFail(getString(R.string.reach_maximum_product_scanned_qty));
                    return null;
                }

                // Exist scanned DO product, only update quantity
                databaseManager.addScannedDOProductQty(scannedDOProduct, multiplier);
                return scannedDOProduct;
            }
        }

        // Check initial multiplier must not more than product_uom_qty
        if (multiplier > selectedDeliveryProduct.getProduct_uom_qty()) {
            showAlertScanBarcodeFail(getString(R.string.reach_maximum_product_scanned_qty));
            return null;
        }

        // No existing product, insert new scanned product
        return databaseManager.addNewScannedDOProduct(databaseManager.copyToStandaloneObject(selectedDeliveryProduct),
                lot_number, multiplier);
    }

    public boolean isLotExpired(String lot_number, String guide_sn) {
        SimpleDateFormat df = new SimpleDateFormat("ddMMyy", Locale.US);
        df.setTimeZone(TimeZone.getTimeZone("Asia/Bangkok"));
        Date date1;
        try {
            if (lot_number == null) {
                date1 = df.parse("");
            }
            else {
                date1 = df.parse(lot_number);
            }
        } catch (ParseException e) {
            return false;
        }

        Date date2;
        try {
            if (guide_sn == null) {
                date2 = df.parse("");
            }
            else {
                date2 = df.parse(guide_sn);
            }
        } catch (ParseException e) {
            return false;
        }

        return date1.before(date2);
    }

    private boolean canAddScannedQty(ScannedDOProduct scannedDOProductForPosition) {
        int scanned_qty = 0;
        for (ScannedDOProduct scannedDOProduct : scannedDOProducts) {
            if (scannedDOProduct.getDeliveryProduct().getProduct_id().equals(scannedDOProductForPosition.getDeliveryProduct().getProduct_id())) {
                scanned_qty += scannedDOProduct.getScan_qty();
            }
        }

        return scanned_qty + multiplier <= scannedDOProductForPosition.getDeliveryProduct().getProduct_uom_qty();
    }

    private void showAlertScanBarcodeFail(String error_message) {
        // Create dialog builder
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        // Set property
        alertDialogBuilder.setTitle(getString(R.string.error))
                .setMessage(error_message)
                .setPositiveButton(getString(R.string.ok), null);

        // create alert dialog
        AlertDialog mAlertDialog = alertDialogBuilder.create();
        // show it
        mAlertDialog.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(mReceiver, mFilter);
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(mReceiver);
    }

    @Override
    public void onStart() {
        super.onStart();

        databaseManager = new DatabaseManager();
        products = databaseManager.query(ProductTemplate.class);
        setting = databaseManager.queryFromId(Setting.class, 1);

        ratios = databaseManager.getRatios(true, ProductAction.DELIVERY_ORDERS.getNumericType(), CallParams.USER_ID);
        ratios.sort("multiplier");

        if (multiplier == null) {
            multiplier = ratios.get(0).getMultiplier();
            selectedRatioBtnIndex = 0;
        }
        mRatioBtn[selectedRatioBtnIndex].setTextColor(ColorHelper.fetchAccentColor(getActivity()));

        for (int i = 0; i < ratios.size(); i++) {
            mRatioBtn[i].setText(ratios.get(i).getRatioTitle());
            mRatioBtn[i].setVisibility(View.VISIBLE);
        }

        deliveryProducts = databaseManager.getDeliveryProductFromDOId(standaloneDeliveryOrder.getId());
        scannedDOProducts = databaseManager.getScannedDOProductsFromDOId(standaloneDeliveryOrder.getId());
        deliveryProductAdapter.updateList(deliveryProducts, scannedDOProducts);
    }

    @Override
    public void onStop() {
        super.onStop();
        databaseManager.close();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.completeBtn:
                DeliveryScanListFragment childFragment = DeliveryScanListFragment.newInstance(standaloneDeliveryOrder);
                OutgoingProductFragment parentFragment = (OutgoingProductFragment) getParentFragment();
                parentFragment.swapChildFragment(childFragment);
                break;

            case R.id.ratio1Btn:
            case R.id.ratio2Btn:
            case R.id.ratio3Btn:
            case R.id.ratio4Btn:
            case R.id.ratio5Btn:
                for (int i = 0; i < ratios.size(); i++) {
                    if (view == mRatioBtn[i]) {
                        multiplier = ratios.get(i).getMultiplier();
                        mRatioBtn[i].setTextColor(ColorHelper.fetchAccentColor(getActivity()));
                        selectedRatioBtnIndex = i;
                    }
                    else {
                        mRatioBtn[i].setTextColor(Color.WHITE);
                    }
                }
        }
    }
}
