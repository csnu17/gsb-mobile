package th.co.trinityroots.gsbwarehouse.controller.outgoing_product;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import io.realm.RealmResults;
import th.co.trinityroots.gsbwarehouse.R;
import th.co.trinityroots.gsbwarehouse.adapter.DOScanListAdapter;
import th.co.trinityroots.gsbwarehouse.controller.MainActivity;
import th.co.trinityroots.gsbwarehouse.controller.barcode_scanner.DOBarcodeScannerFragment;
import th.co.trinityroots.gsbwarehouse.enums.ReferenceStatus;
import th.co.trinityroots.gsbwarehouse.helper.AlertDialogManager;
import th.co.trinityroots.gsbwarehouse.helper.DatabaseManager;
import th.co.trinityroots.gsbwarehouse.helper.DateHelper;
import th.co.trinityroots.gsbwarehouse.model.DeliveryOrder;
import th.co.trinityroots.gsbwarehouse.model.DeliveryProduct;
import th.co.trinityroots.gsbwarehouse.model.ProductTemplate;
import th.co.trinityroots.gsbwarehouse.model.ScannedDOProduct;
import th.co.trinityroots.gsbwarehouse.model.Setting;

public class DeliveryScanListFragment extends Fragment implements View.OnClickListener {

    private DatabaseManager databaseManager;
    private RealmResults<ScannedDOProduct> scannedDOProducts;
    private RealmResults<DeliveryProduct> deliveryProducts;

    private DOScanListAdapter doScanListAdapter;

    private DeliveryOrder standaloneDeliveryOrder;

    public static DeliveryScanListFragment newInstance(DeliveryOrder standaloneDeliveryOrder) {
        DeliveryScanListFragment f = new DeliveryScanListFragment();
        f.standaloneDeliveryOrder = standaloneDeliveryOrder;
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_delivery_scan_list, container, false);
        TextView doNumberTv = (TextView) rootView.findViewById(R.id.doNumberTv);
        TextView doStatusTv = (TextView) rootView.findViewById(R.id.doStatusTv);
        TextView customerNameTv = (TextView) rootView.findViewById(R.id.customerNameTv);
        TextView orderDateTv = (TextView) rootView.findViewById(R.id.orderDateTv);
        TextView dueDateTv = (TextView) rootView.findViewById(R.id.dueDateTv);
        ListView scannedDOProductLv = (ListView) rootView.findViewById(R.id.scannedDOProductLv);
        Button scanBarcodeBtn = (Button) rootView.findViewById(R.id.scanBarcodeBtn);
        Button goBackBtn = (Button) rootView.findViewById(R.id.goBackBtn);
        Button changeDoIdStatusBtn = (Button) rootView.findViewById(R.id.changeDoIdStatusBtn);
        Button reverseOrderBtn = (Button) rootView.findViewById(R.id.reverseOrderBtn);
        Button goBackBtnForPicked = (Button) rootView.findViewById(R.id.goBackBtnForPicked);
        LinearLayout doFooterView = (LinearLayout) rootView.findViewById(R.id.doFooterView);
        LinearLayout footerLLForDOScanListPicked = (LinearLayout) rootView.findViewById(R.id.footerLLForDOScanListPicked);

        doNumberTv.setText(standaloneDeliveryOrder.getOrder_name());
        customerNameTv.setText(getString(R.string.customer, standaloneDeliveryOrder.getPartner_name()));

        String orderDateText = DateHelper.formatDateString(standaloneDeliveryOrder.getDate(), "yy-MM-dd HH:mm:ss",
                "dd/MM/yyyy HH:mm", "Asia/Bangkok");
        orderDateTv.setText(getString(R.string.order_date, orderDateText));

        String dueDateText = DateHelper.formatDateString(standaloneDeliveryOrder.getMin_date(), "yy-MM-dd HH:mm:ss",
                "dd/MM/yyyy HH:mm", "Asia/Bangkok");
        dueDateTv.setText(getString(R.string.due_date, dueDateText));

        if (standaloneDeliveryOrder.getStatus_id() == ReferenceStatus.UNPICKED.getNumericType()) {
            doStatusTv.setText(getString(R.string.reference_status, getString(R.string.picking)));
        }
        else {
            doStatusTv.setText(getString(R.string.reference_status, getString(R.string.picked)));
        }

        doScanListAdapter = new DOScanListAdapter(getActivity(), standaloneDeliveryOrder);
        scannedDOProductLv.setAdapter(doScanListAdapter);

        goBackBtn.setOnClickListener(this);
        scanBarcodeBtn.setOnClickListener(this);
        changeDoIdStatusBtn.setOnClickListener(this);
        reverseOrderBtn.setOnClickListener(this);
        goBackBtnForPicked.setOnClickListener(this);

        // Hidden or show view appropriate with DO's order status
        if (standaloneDeliveryOrder.getStatus_id() == ReferenceStatus.UNPICKED.getNumericType()) {
            footerLLForDOScanListPicked.setVisibility(View.GONE);
            doFooterView.setVisibility(View.VISIBLE);
        }
        else {
            footerLLForDOScanListPicked.setVisibility(View.VISIBLE);
            doFooterView.setVisibility(View.GONE);
        }

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        databaseManager = new DatabaseManager();
        Setting setting = databaseManager.queryFromId(Setting.class, 1);
        scannedDOProducts = databaseManager.getScannedDOProductsFromDOId(standaloneDeliveryOrder.getId());
        deliveryProducts = databaseManager.getDeliveryProductFromDOId(standaloneDeliveryOrder.getId());
        doScanListAdapter.updateList(scannedDOProducts, setting);
    }

    @Override
    public void onStop() {
        super.onStop();
        databaseManager.close();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.goBackBtn:
            case R.id.goBackBtnForPicked:
                MainActivity mainActivity = (MainActivity) getActivity();
                mainActivity.swapFragment();
                break;

            case R.id.scanBarcodeBtn:
                DOBarcodeScannerFragment doBarcodeScannerFragment = DOBarcodeScannerFragment.newInstance(standaloneDeliveryOrder);
                OutgoingProductFragment parentFragment = (OutgoingProductFragment) getParentFragment();
                parentFragment.swapChildFragment(doBarcodeScannerFragment);
                break;

            case R.id.changeDoIdStatusBtn:
            case R.id.reverseOrderBtn:
                changeDoIdStatus();
                break;
        }
    }

    private void changeDoIdStatus() {
        if (scannedDOProducts.size() == 0) {
            AlertDialogManager.showNoticeAlertDialog(getActivity(), getString(R.string.error), getString(R.string.please_scan_product));
            return;
        }

        String message = "";
        if (standaloneDeliveryOrder.getStatus_id() == ReferenceStatus.UNPICKED.getNumericType()) {
            // Check incomplete product
            for (int i = 0; i < deliveryProducts.size(); i++) {
                DeliveryProduct deliveryProduct = deliveryProducts.get(i);
                int scan_qty = 0;

                for (ScannedDOProduct scannedDOProduct : scannedDOProducts) {
                    if (scannedDOProduct.getDeliveryProduct().getId() == deliveryProduct.getId()) {
                        scan_qty = scannedDOProduct.getScan_qty();
                        break;
                    }
                }

                // Product name
                ProductTemplate productTemplate = databaseManager.queryFromId(ProductTemplate.class,
                        deliveryProduct.getProduct_id());
                if (productTemplate != null) {
                    message += (i + 1) + ". " + productTemplate.getName() + " " + scan_qty + "/" +
                            deliveryProduct.getProduct_uom_qty() + "\n";
                }
            }

            message += "\n" + getString(R.string.confirm_change_to_picked, standaloneDeliveryOrder.getOrder_name());
        }
        else {
            message = getString(R.string.confirm_change_to_unpicked, standaloneDeliveryOrder.getOrder_name());
        }

        // Create dialog builder
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        // Set property
        alertDialogBuilder.setTitle(getString(R.string.confirm));
        alertDialogBuilder.setMessage(message)
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        DeliveryOrder deliveryOrder;
                        if (standaloneDeliveryOrder.getStatus_id() == ReferenceStatus.UNPICKED.getNumericType()) {
                            deliveryOrder = databaseManager.changeDOStatus(standaloneDeliveryOrder.getId(),
                                    ReferenceStatus.PICKED.getNumericType());
                        }
                        else {
                            deliveryOrder = databaseManager.changeDOStatus(standaloneDeliveryOrder.getId(),
                                    ReferenceStatus.UNPICKED.getNumericType());
                        }
                        standaloneDeliveryOrder = databaseManager.copyToStandaloneObject(deliveryOrder);

                        DeliveryScanListFragment deliveryScanListFragment = DeliveryScanListFragment.newInstance(standaloneDeliveryOrder);
                        OutgoingProductFragment parentFragment = (OutgoingProductFragment) getParentFragment();
                        parentFragment.clearBackstack();
                        parentFragment.swapChildFragment(deliveryScanListFragment);
                    }
                })
                .setNegativeButton(getString(R.string.cancel), null);

        // create alert dialog
        AlertDialog mAlertDialog = alertDialogBuilder.create();
        // show it
        mAlertDialog.show();
    }

}
