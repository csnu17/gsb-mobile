package th.co.trinityroots.gsbwarehouse.utility;

/**
 * Contain name of the Odoo's model that use in the application.
 */
public class OdooModelName {

    public static final String USERS = "res.users";
    public static final String PARTNER = "res.partner";
    public static final String PRODUCT = "product.product";
    public static final String WAREHOUSE = "stock.warehouse";
    public static final String LOCATION = "stock.location";
    public static final String COMPANY = "res.company";
    public static final String INVENTORY = "stock.inventory";
    public static final String PICKING = "stock.picking";
    public static final String MOVE = "stock.move";

    // Method
    public static final String ANDROID_CREATE_METHOD = "android_create";
    public static final String ANDROID_CREATE_DO = "android_do";

}
