package th.co.trinityroots.gsbwarehouse.enums;

public enum ReferenceStatus {

    UNFINISHED(1),
    FINISHED(2),
    UNPICKED(3),
    PICKED(4),
    UNKNOWN(5);

    private int type;

    ReferenceStatus(int i) {
        this.type = i;
    }

    public int getNumericType() {
        return type;
    }

}
