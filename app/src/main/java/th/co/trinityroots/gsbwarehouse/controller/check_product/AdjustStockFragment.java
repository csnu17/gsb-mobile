package th.co.trinityroots.gsbwarehouse.controller.check_product;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.afollestad.materialdialogs.MaterialDialog;

import io.realm.RealmResults;
import th.co.trinityroots.gsbwarehouse.R;
import th.co.trinityroots.gsbwarehouse.adapter.ReferenceOrderAdapter;
import th.co.trinityroots.gsbwarehouse.controller.MainActivity;
import th.co.trinityroots.gsbwarehouse.enums.ProductAction;
import th.co.trinityroots.gsbwarehouse.helper.AlertDialogManager;
import th.co.trinityroots.gsbwarehouse.helper.DatabaseManager;
import th.co.trinityroots.gsbwarehouse.interfaces.CreateIncomingCallBack;
import th.co.trinityroots.gsbwarehouse.interfaces.CreateInventoryCallBack;
import th.co.trinityroots.gsbwarehouse.model.ReferenceOrder;
import th.co.trinityroots.gsbwarehouse.model.ScannedProduct;
import th.co.trinityroots.gsbwarehouse.utility.CallParams;
import th.co.trinityroots.gsbwarehouse.utility.OdooOperation;

public class AdjustStockFragment extends Fragment {

    private Button updateToOdooBtn;

    private DatabaseManager databaseManager;
    private RealmResults<ReferenceOrder> orders;

    private ReferenceOrderAdapter referenceOrderAdapter;
    private ProductAction mProductAction;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mProductAction = ((MainActivity) getActivity()).getProductAction();

        View rootView = inflater.inflate(R.layout.fragment_adjust_stock, container, false);
        Button adjustStockBtn = (Button) rootView.findViewById(R.id.adjustStockBtn);
        updateToOdooBtn = (Button) rootView.findViewById(R.id.updateToOdooBtn);
        ListView referenceNumberLv = (ListView) rootView.findViewById(R.id.referenceNumberLv);

        referenceOrderAdapter = new ReferenceOrderAdapter(getActivity());
        referenceNumberLv.setAdapter(referenceOrderAdapter);

        referenceNumberLv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                ReferenceOrder order = databaseManager.copyToStandaloneObject(referenceOrderAdapter.getItem(position));
                ProductInRefNoFragment productInRefNoFragment = ProductInRefNoFragment.newInstance(order);
                CheckProductFragment parent = (CheckProductFragment) getParentFragment();
                parent.swapChildFragment(productInRefNoFragment);
            }
        });

        referenceNumberLv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id) {
                final ReferenceOrder selectedOrder = referenceOrderAdapter.getItem(position);

                // Create dialog builder
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                // Set property
                alertDialogBuilder.setTitle(getString(R.string.warning))
                        .setMessage(getString(R.string.confirm_delete_order_msg, selectedOrder.getRef_id()))
                        .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                RealmResults<ScannedProduct> scannedProducts = databaseManager.getScannedProducts(selectedOrder.getId());
                                databaseManager.clearAll(scannedProducts);
                                databaseManager.clear(selectedOrder);
                                referenceOrderAdapter.updateList(orders);

                                if (orders.size() > 0) {
                                    updateToOdooBtn.setVisibility(View.VISIBLE);
                                } else {
                                    updateToOdooBtn.setVisibility(View.GONE);
                                }
                            }
                        })
                        .setNegativeButton(getString(R.string.cancel), null);
                // create alert dialog
                AlertDialog alert = alertDialogBuilder.create();
                // show it
                alert.show();

                return true;
            }
        });

        String actionBtnText;
        if (mProductAction == ProductAction.INCOMING_SHIPMENTS) {
            actionBtnText = getString(R.string.create_new_incoming);
        }
        else {
            actionBtnText = getString(R.string.adjust_stock);
        }
        adjustStockBtn.setText(actionBtnText);

        adjustStockBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FillReferenceFragment childFragment = FillReferenceFragment.newInstance(mProductAction);
                CheckProductFragment parentFragment = (CheckProductFragment) getParentFragment();
                parentFragment.swapChildFragment(childFragment);
            }
        });

        updateToOdooBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Create dialog builder
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                // Set property
                alertDialogBuilder.setTitle(getString(R.string.warning))
                        .setMessage(getString(R.string.confirm_create_inventory_msg))
                        .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                if (mProductAction == ProductAction.INCOMING_SHIPMENTS) {
                                    createIncoming();
                                }
                                else if (mProductAction == ProductAction.INVENTORY_ADJUSTMENTS) {
                                    createInventory();
                                }
                            }
                        })
                        .setNegativeButton(getString(R.string.cancel), null);

                // create alert dialog
                AlertDialog alert = alertDialogBuilder.create();

                // show it
                alert.show();
            }
        });

        return rootView;
    }

    private void createInventory() {
        final MaterialDialog mMaterialDialog = new MaterialDialog.Builder(getActivity())
                .title(R.string.please_wait)
                .content(R.string.update_data_to_system)
                .progress(true, 0)
                .cancelable(false)
                .build();
        mMaterialDialog.show();

        OdooOperation operation = new OdooOperation(getActivity());
        operation.createInventory(new CreateInventoryCallBack() {
            @Override
            public void onSuccess() {
                mMaterialDialog.dismiss();
                AlertDialogManager.showNoticeAlertDialog(getActivity(), getString(R.string.nav_check_product),
                        getString(R.string.create_inventory_success));

                // Refresh data and ListView
                referenceOrderAdapter.updateList(orders);
                if (orders.size() > 0) {
                    updateToOdooBtn.setVisibility(View.VISIBLE);
                } else {
                    updateToOdooBtn.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(String error_msg) {
                mMaterialDialog.dismiss();
                AlertDialogManager.showNoticeAlertDialog(getActivity(), getString(R.string.error), error_msg);

                // Refresh data and ListView
                referenceOrderAdapter.updateList(orders);
                if (orders.size() > 0) {
                    updateToOdooBtn.setVisibility(View.VISIBLE);
                } else {
                    updateToOdooBtn.setVisibility(View.GONE);
                }
            }
        });
    }

    private void createIncoming() {
        final MaterialDialog mMaterialDialog = new MaterialDialog.Builder(getActivity())
                .title(R.string.please_wait)
                .content(R.string.update_data_to_system)
                .progress(true, 0)
                .cancelable(false)
                .build();
        mMaterialDialog.show();

        OdooOperation operation = new OdooOperation(getActivity());
        operation.createIncoming(new CreateIncomingCallBack() {
            @Override
            public void onSuccess() {
                mMaterialDialog.dismiss();
                AlertDialogManager.showNoticeAlertDialog(getActivity(), getString(R.string.nav_incoming_product),
                        getString(R.string.create_incoming_success));

                // Refresh data and ListView
                referenceOrderAdapter.updateList(orders);
                if (orders.size() > 0) {
                    updateToOdooBtn.setVisibility(View.VISIBLE);
                } else {
                    updateToOdooBtn.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(String error_msg) {
                mMaterialDialog.dismiss();
                AlertDialogManager.showNoticeAlertDialog(getActivity(), getString(R.string.error), error_msg);

                // Refresh data and ListView
                referenceOrderAdapter.updateList(orders);
                if (orders.size() > 0) {
                    updateToOdooBtn.setVisibility(View.VISIBLE);
                } else {
                    updateToOdooBtn.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        databaseManager = new DatabaseManager();
        orders = databaseManager.getReferenceOrders(mProductAction.getNumericType(), CallParams.USER_ID);
        referenceOrderAdapter.updateList(orders);

        if (orders.size() > 0) {
            updateToOdooBtn.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        databaseManager.close();
        updateToOdooBtn.setVisibility(View.GONE);
    }
}
