package th.co.trinityroots.gsbwarehouse.helper;

import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Contain mathod that help to work with date, datetime, time.
 */
public class DateHelper {

    public static String getCurrentDateTime(String format, String timezone) {
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.US);
        sdf.setTimeZone(TimeZone.getTimeZone(timezone));
        return sdf.format(new Date());
    }

    public static String formatDateString(String strDate, String oldFormat, String newFormat, String timezone) {
        try {
            SimpleDateFormat df = new SimpleDateFormat(oldFormat, Locale.US);
            df.setTimeZone(TimeZone.getTimeZone(timezone));
            Date newDate = df.parse(strDate);

            df = new SimpleDateFormat(newFormat, Locale.US);
            df.setTimeZone(TimeZone.getTimeZone(timezone));

            return df.format(newDate);
        } catch (ParseException e) {
            Log.d("GSB", "Parse string date error: " + e.getLocalizedMessage());
            return strDate;
        }
    }

}
