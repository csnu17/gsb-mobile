package th.co.trinityroots.gsbwarehouse.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Ratio extends RealmObject {

    @PrimaryKey
    private Integer id;

    private String ratioTitle;
    private Integer multiplier;
    private Boolean active;
    private int product_action_id;
    private int user_id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRatioTitle() {
        return ratioTitle;
    }

    public void setRatioTitle(String ratioTitle) {
        this.ratioTitle = ratioTitle;
    }

    public Integer getMultiplier() {
        return multiplier;
    }

    public void setMultiplier(Integer multiplier) {
        this.multiplier = multiplier;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public int getProduct_action_id() {
        return product_action_id;
    }

    public void setProduct_action_id(int product_action_id) {
        this.product_action_id = product_action_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }
}
