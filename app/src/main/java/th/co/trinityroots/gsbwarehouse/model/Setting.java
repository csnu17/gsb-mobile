package th.co.trinityroots.gsbwarehouse.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import th.co.trinityroots.gsbwarehouse.utility.OdooUtility;

public class Setting extends RealmObject {

    @PrimaryKey
    private Integer id;

    private Integer product_digit_start;
    private Integer product_digit_end;
    private Integer lot_digit_start;
    private Integer lot_digit_end;
    private Boolean back_order;
    private Boolean allow_qty_incoming;
    private Boolean allow_qty_outgoing;
    private Boolean allow_qty_inventory;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getProduct_digit_start() {
        return product_digit_start;
    }

    public void setProduct_digit_start(Integer product_digit_start) {
        this.product_digit_start = product_digit_start;
    }

    public Integer getProduct_digit_end() {
        return product_digit_end;
    }

    public void setProduct_digit_end(Integer product_digit_end) {
        this.product_digit_end = product_digit_end;
    }

    public Integer getLot_digit_start() {
        return lot_digit_start;
    }

    public void setLot_digit_start(Integer lot_digit_start) {
        this.lot_digit_start = lot_digit_start;
    }

    public Integer getLot_digit_end() {
        return lot_digit_end;
    }

    public void setLot_digit_end(Integer lot_digit_end) {
        this.lot_digit_end = lot_digit_end;
    }

    public Boolean getBack_order() {
        return back_order;
    }

    public void setBack_order(Boolean back_order) {
        this.back_order = back_order;
    }

    public Boolean getAllow_qty_incoming() {
        return allow_qty_incoming;
    }

    public void setAllow_qty_incoming(Boolean allow_qty_incoming) {
        this.allow_qty_incoming = allow_qty_incoming;
    }

    public Boolean getAllow_qty_outgoing() {
        return allow_qty_outgoing;
    }

    public void setAllow_qty_outgoing(Boolean allow_qty_outgoing) {
        this.allow_qty_outgoing = allow_qty_outgoing;
    }

    public Boolean getAllow_qty_inventory() {
        return allow_qty_inventory;
    }

    public void setAllow_qty_inventory(Boolean allow_qty_inventory) {
        this.allow_qty_inventory = allow_qty_inventory;
    }

    public static List<Setting> parseData(Object[] classObjs) {
        List<Setting> settings = new ArrayList<>();

        for (Object object : classObjs) {
            @SuppressWarnings("unchecked")
            Map<String, Object> classObj = (Map<String, Object>) object;

            Setting s = new Setting();
            s.setId(OdooUtility.getInteger(classObj, "id"));
            s.setProduct_digit_start(OdooUtility.getInteger(classObj, "product_digit_start"));
            s.setProduct_digit_end(OdooUtility.getInteger(classObj, "product_digit_end"));
            s.setLot_digit_start(OdooUtility.getInteger(classObj, "lot_digit_start"));
            s.setLot_digit_end(OdooUtility.getInteger(classObj, "lot_digit_end"));
            s.setBack_order(OdooUtility.getBoolean(classObj, "back_order"));
            s.setAllow_qty_incoming(OdooUtility.getBoolean(classObj, "allow_qty_incoming"));
            s.setAllow_qty_outgoing(OdooUtility.getBoolean(classObj, "allow_qty_outgoing"));
            s.setAllow_qty_inventory(OdooUtility.getBoolean(classObj, "allow_qty_inventory"));

            settings.add(s);
        }

        return settings;
    }

}
