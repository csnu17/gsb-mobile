package th.co.trinityroots.gsbwarehouse.interfaces;

public interface SyncProductListener {
    void onSuccess();
    void onFailure(String error_msg);
}
