package th.co.trinityroots.gsbwarehouse.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import th.co.trinityroots.gsbwarehouse.utility.OdooUtility;

/**
 * Realm stock location from master data
 */
public class StockLocation extends RealmObject {

    @PrimaryKey
    private Integer id;

    private String name;
    private Integer location_id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getLocation_id() {
        return location_id;
    }

    public void setLocation_id(Integer location_id) {
        this.location_id = location_id;
    }

    public static List<StockLocation> parseData(Object[] classObjs) {
        List<StockLocation> stockLocations = new ArrayList<>();

        for (Object object : classObjs) {
            @SuppressWarnings("unchecked")
            Map<String, Object> classObj = (Map<String, Object>) object;

            StockLocation sl = new StockLocation();
            sl.setId(OdooUtility.getInteger(classObj, "id"));
            sl.setName(OdooUtility.getString(classObj, "name"));

            M2OField location_id_m2OField = OdooUtility.getMany2One(classObj, "location_id");
            sl.setLocation_id(location_id_m2OField.id);

            stockLocations.add(sl);
        }

        return stockLocations;
    }

}
