package th.co.trinityroots.gsbwarehouse.interfaces;

public interface CreateDeliveryOrderCallback {
    void onSuccess();
    void onFailure(String error_msg);
}
