package th.co.trinityroots.gsbwarehouse.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import io.realm.RealmResults;
import th.co.trinityroots.gsbwarehouse.R;
import th.co.trinityroots.gsbwarehouse.enums.ReferenceStatus;
import th.co.trinityroots.gsbwarehouse.helper.AlertDialogManager;
import th.co.trinityroots.gsbwarehouse.helper.DatabaseManager;
import th.co.trinityroots.gsbwarehouse.model.DeliveryOrder;
import th.co.trinityroots.gsbwarehouse.model.ProductTemplate;
import th.co.trinityroots.gsbwarehouse.model.ScannedDOProduct;
import th.co.trinityroots.gsbwarehouse.model.Setting;

public class DOScanListAdapter extends BaseAdapter {

    private List<ScannedDOProduct> dataSet = Collections.emptyList();
    private final LayoutInflater inflater;
    private DeliveryOrder standaloneDeliveryOrder;
    private Context mContext;
    private Setting setting;

    public DOScanListAdapter(Context context, DeliveryOrder standaloneDeliveryOrder) {
        this.inflater = LayoutInflater.from(context);
        this.standaloneDeliveryOrder = standaloneDeliveryOrder;
        mContext = context;
    }

    public void updateList(RealmResults<ScannedDOProduct> dataSet, Setting setting) {
        this.dataSet = dataSet;
        this.setting = setting;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return dataSet.size();
    }

    @Override
    public ScannedDOProduct getItem(int i) {
        return dataSet.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.do_scanned_product_list_item, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        final ScannedDOProduct scannedDOProduct = getItem(position);

        // Product name
        DatabaseManager databaseManager = new DatabaseManager();
        ProductTemplate productTemplate = databaseManager.queryFromId(ProductTemplate.class,
                scannedDOProduct.getDeliveryProduct().getProduct_id());
        if (productTemplate != null) {
            viewHolder.productNameTv.setText(productTemplate.getName());
        }
        else {
            viewHolder.productNameTv.setText("");
        }
        databaseManager.close();

        // Lot
        viewHolder.productLotTv.setText(mContext.getString(R.string.lot_no, scannedDOProduct.getLot_number()));

        // Scanned qty
        String qtyText = String.valueOf(scannedDOProduct.getScan_qty());
        viewHolder.productQtyTv.setText(qtyText);

        if (standaloneDeliveryOrder.getStatus_id() == ReferenceStatus.PICKED.getNumericType()) {
            viewHolder.minusProductTv.setVisibility(View.GONE);
            viewHolder.plusProductTv.setVisibility(View.GONE);
            viewHolder.productQtyTv.setBackgroundColor(Color.TRANSPARENT);
            setPaddingForProductQtyTv(viewHolder.productQtyTv, 0.0f);
        }
        else {
            // - + qty
            boolean allowChangeQty;
            if (setting != null) {
                allowChangeQty = setting.getAllow_qty_outgoing();
            }
            else {
                allowChangeQty = true;
            }

            if (allowChangeQty) {
                viewHolder.minusProductTv.setVisibility(View.VISIBLE);
                viewHolder.plusProductTv.setVisibility(View.VISIBLE);
                viewHolder.productQtyTv.setBackgroundColor(Color.LTGRAY);
                setPaddingForProductQtyTv(viewHolder.productQtyTv, 12.0f);

                viewHolder.minusProductTv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        DatabaseManager databaseManager = new DatabaseManager();
                        databaseManager.removeScannedDOProductQty(scannedDOProduct, 1);
                        if (!scannedDOProduct.isValid()) {
                            notifyDataSetChanged();
                        }
                        else {
                            viewHolder.productQtyTv.setText(String.valueOf(scannedDOProduct.getScan_qty()));
                        }
                        databaseManager.close();
                    }
                });

                viewHolder.plusProductTv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // scanned qty must not more than product_uom_qty
                        if (canAddScannedQty(scannedDOProduct)) {
                            DatabaseManager databaseManager = new DatabaseManager();
                            databaseManager.addScannedDOProductQty(scannedDOProduct, 1);
                            viewHolder.productQtyTv.setText(String.valueOf(scannedDOProduct.getScan_qty()));
                            databaseManager.close();
                        }
                        else {
                            AlertDialogManager.showNoticeAlertDialog(mContext, mContext.getString(R.string.error),
                                    mContext.getString(R.string.reach_maximum_product_scanned_qty));
                        }
                    }
                });
            }
            else {
                viewHolder.minusProductTv.setVisibility(View.GONE);
                viewHolder.plusProductTv.setVisibility(View.GONE);
                viewHolder.productQtyTv.setBackgroundColor(Color.TRANSPARENT);
                setPaddingForProductQtyTv(viewHolder.productQtyTv, 0.0f);
            }
        }

        return convertView;
    }

    private boolean canAddScannedQty(ScannedDOProduct scannedDOProductForPosition) {
        int scanned_qty = 0;
        for (ScannedDOProduct scannedDOProduct : dataSet) {
            if (scannedDOProduct.getDeliveryProduct().getProduct_id().equals(scannedDOProductForPosition.getDeliveryProduct().getProduct_id())) {
                scanned_qty += scannedDOProduct.getScan_qty();

                if (scanned_qty >= scannedDOProductForPosition.getDeliveryProduct().getProduct_uom_qty()) {
                    return false;
                }
            }
        }

        return true;
    }

    private void setPaddingForProductQtyTv(TextView productQtyTv, float sizeInDp) {
        float scale = mContext.getResources().getDisplayMetrics().density;
        int dpAsPixels = (int) (sizeInDp * scale + 0.5f);
        productQtyTv.setPadding(dpAsPixels, 0, dpAsPixels, 0);
    }

    private class ViewHolder {
        TextView productNameTv;
        TextView productLotTv;
        TextView productQtyTv;
        TextView minusProductTv;
        TextView plusProductTv;

        public ViewHolder(View convertView) {
            productNameTv = (TextView) convertView.findViewById(R.id.product_name_tv);
            productLotTv = (TextView) convertView.findViewById(R.id.product_lot_tv);
            productQtyTv = (TextView) convertView.findViewById(R.id.product_qty_tv);
            minusProductTv = (TextView) convertView.findViewById(R.id.minusScannedProductTv);
            plusProductTv = (TextView) convertView.findViewById(R.id.plusScannedProductTv);
        }
    }

}
