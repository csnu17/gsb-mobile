package th.co.trinityroots.gsbwarehouse;

import android.app.Application;

import th.co.trinityroots.gsbwarehouse.helper.DatabaseManager;
import th.co.trinityroots.gsbwarehouse.helper.Language;

/**
 * Override Application configuration class.
 */
public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Language.loadLocale(this);
        DatabaseManager.setupDatabase(this);
//        DatabaseManager.deleteDatabase();
    }

}
