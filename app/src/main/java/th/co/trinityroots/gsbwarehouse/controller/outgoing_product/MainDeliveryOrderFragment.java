package th.co.trinityroots.gsbwarehouse.controller.outgoing_product;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.List;

import io.realm.RealmResults;
import io.realm.Sort;
import th.co.trinityroots.gsbwarehouse.R;
import th.co.trinityroots.gsbwarehouse.adapter.DeliveryOrderAdapter;
import th.co.trinityroots.gsbwarehouse.enums.ReferenceStatus;
import th.co.trinityroots.gsbwarehouse.helper.AlertDialogManager;
import th.co.trinityroots.gsbwarehouse.helper.ConnectionDetector;
import th.co.trinityroots.gsbwarehouse.helper.DatabaseManager;
import th.co.trinityroots.gsbwarehouse.helper.DateHelper;
import th.co.trinityroots.gsbwarehouse.helper.SharedData;
import th.co.trinityroots.gsbwarehouse.interfaces.CreateDeliveryOrderCallback;
import th.co.trinityroots.gsbwarehouse.interfaces.GetDOCallback;
import th.co.trinityroots.gsbwarehouse.model.DeliveryOrder;
import th.co.trinityroots.gsbwarehouse.model.DeliveryProduct;
import th.co.trinityroots.gsbwarehouse.model.ScannedDOProduct;
import th.co.trinityroots.gsbwarehouse.utility.CallParams;
import th.co.trinityroots.gsbwarehouse.utility.Constant;
import th.co.trinityroots.gsbwarehouse.utility.OdooOperation;

public class MainDeliveryOrderFragment extends Fragment {

    private SwipeRefreshLayout swipeContainer;
    private TextView lastSyncDOTv;
    private Button updateToOdooBtn;

    private DeliveryOrderAdapter deliveryOrderAdapter;

    private DatabaseManager databaseManager;
    private RealmResults<DeliveryOrder> deliveryOrders;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getDOData();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main_delivery_order, container, false);
        ListView deliveryOrderLv = (ListView) rootView.findViewById(R.id.deliveryOrderLv);
        swipeContainer = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeContainer);
        lastSyncDOTv = (TextView) rootView.findViewById(R.id.lastSyncDOTv);
        updateToOdooBtn = (Button) rootView.findViewById(R.id.updateToOdooBtn);

        String latestSyncDO = SharedData.getStringFromKey(getActivity(),
                Constant.LATEST_SYNC_DO_KEY, "");
        lastSyncDOTv.setText(getString(R.string.latest_sync_do, latestSyncDO));

        deliveryOrderAdapter = new DeliveryOrderAdapter(getActivity());
        deliveryOrderLv.setAdapter(deliveryOrderAdapter);

        // Setup refresh listener which triggers new data loading
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getDOData();
            }
        });
        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        deliveryOrderLv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                DeliveryOrder standaloneDeliveryOrder = databaseManager.copyToStandaloneObject(deliveryOrderAdapter.getItem(position));

                Fragment child;
                if (standaloneDeliveryOrder.getStatus_id() == ReferenceStatus.UNPICKED.getNumericType()) {
                    child = DeliveryOrderDetailFragment.newInstance(standaloneDeliveryOrder);
                }
                else {
                    child = DeliveryScanListFragment.newInstance(standaloneDeliveryOrder);
                }

                OutgoingProductFragment parent = (OutgoingProductFragment) getParentFragment();
                parent.swapChildFragment(child);
            }
        });

        deliveryOrderLv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id) {
                final DeliveryOrder selectedOrder = deliveryOrderAdapter.getItem(position);

                // Create dialog builder
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                // Set property
                alertDialogBuilder.setTitle(getString(R.string.warning))
                        .setMessage(getString(R.string.confirm_delete_order_msg, selectedOrder.getOrigin()))
                        .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // Clear old scanned do product if need
                                RealmResults<ScannedDOProduct> scannedDOProducts = databaseManager.getScannedDOProductsFromDOId(selectedOrder.getId());
                                if (scannedDOProducts.size() > 0) {
                                    databaseManager.clearAll(scannedDOProducts);
                                }
                                // Clear delivery product if need
                                RealmResults<DeliveryProduct> deliveryProducts = databaseManager.getDeliveryProductFromDOId(selectedOrder.getId());
                                if (deliveryProducts.size() > 0) {
                                    databaseManager.clearAll(deliveryProducts);
                                }
                                databaseManager.clear(selectedOrder);

                                deliveryOrderAdapter.updateList(deliveryOrders);
                                if (deliveryOrders.size() > 0) {
                                    updateToOdooBtn.setVisibility(View.VISIBLE);
                                }
                                else {
                                    updateToOdooBtn.setVisibility(View.GONE);
                                }
                            }
                        })
                        .setNegativeButton(getString(R.string.cancel), null);
                // create alert dialog
                AlertDialog alert = alertDialogBuilder.create();
                // show it
                alert.show();

                return true;
            }
        });

        updateToOdooBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Must has at least a picked order.
                boolean canUpdateToOdoo = false;
                for (DeliveryOrder deliveryOrder : deliveryOrders) {
                    if (deliveryOrder.getStatus_id() == ReferenceStatus.PICKED.getNumericType()) {
                        canUpdateToOdoo = true;
                        break;
                    }
                }
                if (!canUpdateToOdoo) {
                    AlertDialogManager.showNoticeAlertDialog(getActivity(), getString(R.string.error),
                            getString(R.string.no_picked_do_message));
                    return;
                }

                // Check connection
                ConnectionDetector cd = new ConnectionDetector(getActivity());
                if (!cd.isConnected()) {
                    AlertDialogManager.showNoticeAlertDialog(getActivity(), getString(R.string.error),
                            getString(R.string.no_connection));
                    return;
                }

                // Create dialog builder
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                // Set property
                alertDialogBuilder.setTitle(getString(R.string.warning))
                        .setMessage(getString(R.string.confirm_create_do))
                        .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                createDeliveryOrder();
                            }
                        })
                        .setNegativeButton(getString(R.string.cancel), null);

                // create alert dialog
                AlertDialog alert = alertDialogBuilder.create();

                // show it
                alert.show();
            }
        });

        return rootView;
    }

    private void createDeliveryOrder() {
        final MaterialDialog mMaterialDialog = new MaterialDialog.Builder(getActivity())
                .title(R.string.please_wait)
                .content(R.string.update_data_to_system)
                .progress(true, 0)
                .cancelable(false)
                .build();
        mMaterialDialog.show();

        OdooOperation operation = new OdooOperation(getActivity());
        operation.createDeliveryOrder(new CreateDeliveryOrderCallback() {
            @Override
            public void onSuccess() {
                mMaterialDialog.dismiss();
                AlertDialogManager.showNoticeAlertDialog(getActivity(), getString(R.string.nav_outgoing_product),
                        getString(R.string.create_do_success));

                // Refresh data and ListView
                deliveryOrderAdapter.updateList(deliveryOrders);
                if (deliveryOrders.size() > 0) {
                    updateToOdooBtn.setVisibility(View.VISIBLE);
                } else {
                    updateToOdooBtn.setVisibility(View.GONE);
                }

                getDOData();
            }

            @Override
            public void onFailure(String error_msg) {
                mMaterialDialog.dismiss();
                AlertDialogManager.showNoticeAlertDialog(getActivity(), getString(R.string.error), error_msg);

                // Refresh data and ListView
                deliveryOrderAdapter.updateList(deliveryOrders);
                if (deliveryOrders.size() > 0) {
                    updateToOdooBtn.setVisibility(View.VISIBLE);
                } else {
                    updateToOdooBtn.setVisibility(View.GONE);
                }

                getDOData();
            }
        });
    }

    private void getDOData() {
        final MaterialDialog mMaterialDialog = new MaterialDialog.Builder(getActivity())
                .title(R.string.progress_dialog_title)
                .content(R.string.progress_dialog_loading_msg)
                .progress(true, 0)
                .cancelable(false)
                .build();
        mMaterialDialog.show();

        OdooOperation operation = new OdooOperation(getActivity());
        operation.getDOData(new GetDOCallback() {
            @Override
            public void onSuccess(List<String> deleteDOsName) {
                mMaterialDialog.dismiss();
                if (swipeContainer != null) {
                    swipeContainer.setRefreshing(false);
                }

                if (deleteDOsName != null && deleteDOsName.size() > 0) {
                    String message = "";
                    for (int i = 0; i < deleteDOsName.size(); i++) {
                        message += deleteDOsName.get(i);
                        if (i < deleteDOsName.size() - 1) {
                            message += ", ";
                        }
                    }

                    AlertDialogManager.showNoticeAlertDialog(getActivity(), getString(R.string.app_name),
                            getString(R.string.notice_delete_do_msg, message));
                }

                if (deliveryOrderAdapter != null) {
                    deliveryOrderAdapter.updateList(deliveryOrders);

                    if (deliveryOrders.size() > 0) {
                        updateToOdooBtn.setVisibility(View.VISIBLE);
                    }
                    else {
                        updateToOdooBtn.setVisibility(View.GONE);
                    }
                }

                String currentDateAndTime = DateHelper.getCurrentDateTime("dd/MM/yyyy HH:mm", "Asia/Bangkok");
                SharedData.setStringForKey(getActivity(), Constant.LATEST_SYNC_DO_KEY, currentDateAndTime);
                lastSyncDOTv.setText(getString(R.string.latest_sync_do, currentDateAndTime));
            }

            @Override
            public void onFailure(String error_msg) {
                mMaterialDialog.dismiss();
                if (swipeContainer != null) {
                    swipeContainer.setRefreshing(false);
                }
                AlertDialogManager.showNoticeAlertDialog(getActivity(), getString(R.string.error), error_msg);
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        databaseManager = new DatabaseManager();
        deliveryOrders = databaseManager.queryFromUserId(DeliveryOrder.class, CallParams.USER_ID);
        deliveryOrders.sort("sequence", Sort.ASCENDING, "origin", Sort.ASCENDING);
        deliveryOrderAdapter.updateList(deliveryOrders);

        if (deliveryOrders.size() > 0) {
            updateToOdooBtn.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        databaseManager.close();
        updateToOdooBtn.setVisibility(View.GONE);
    }
}
