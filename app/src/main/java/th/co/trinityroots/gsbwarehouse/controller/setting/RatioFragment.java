package th.co.trinityroots.gsbwarehouse.controller.setting;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import io.realm.Realm;
import io.realm.RealmResults;
import th.co.trinityroots.gsbwarehouse.R;
import th.co.trinityroots.gsbwarehouse.adapter.RatioItemAdapter;
import th.co.trinityroots.gsbwarehouse.controller.MainActivity;
import th.co.trinityroots.gsbwarehouse.enums.ProductAction;
import th.co.trinityroots.gsbwarehouse.helper.AlertDialogManager;
import th.co.trinityroots.gsbwarehouse.model.Ratio;
import th.co.trinityroots.gsbwarehouse.utility.CallParams;

public class RatioFragment extends Fragment {

    private ProductAction mProductAction;
    private Realm mRealm;
    private RatioItemAdapter mRatioItemAdapter;
    private RealmResults<Ratio> results;

    public static RatioFragment newInstance(ProductAction productAction) {
        RatioFragment f = new RatioFragment();
        f.mProductAction = productAction;
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.ratio_fragment_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add_new_ratio_menu_id:
                showInputNewRatioView();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void showInputNewRatioView() {
        final EditText input = new EditText(getActivity());
        input.setHint(getString(R.string.ratio_input_hint));
        input.setMaxLines(1);
        input.setSingleLine(true);

        // Create dialog builder
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        // Set property
        alertDialogBuilder.setTitle(getString(R.string.please_fill_ratio))
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String ratioInput = input.getText().toString().trim();
                        String errorMsg = validateForAddingNewRatio(ratioInput);
                        if (errorMsg == null) {
                            addNewObjectToRealm(ratioInput);
                        } else {
                            AlertDialogManager.showNoticeAlertDialog(getActivity(), getString(R.string.error), errorMsg);
                        }
                    }
                })
                .setNegativeButton(getString(R.string.cancel), null);

        // create alert dialog
        AlertDialog mAlertDialog = alertDialogBuilder.create();
        // Add EditText inside AlertDialog
        mAlertDialog.setView(input);
        // show it
        mAlertDialog.show();
    }

    private String validateForAddingNewRatio(String multiplierStr) {
        // Check empty multiplier
        if (TextUtils.isEmpty(multiplierStr)) {
            return getString(R.string.empty_multiplier);
        }

        // Multiplier is accepted only numeric.
        if (!TextUtils.isDigitsOnly(multiplierStr)) {
            return getString(R.string.check_multiplier_numeric_fail);
        }

        Integer multiplier = Integer.parseInt(multiplierStr);
        // Check multiplier has not be 0
        if (multiplier == 0) {
            return getString(R.string.cant_add_0_ratio);
        }

        // Check duplicate ratio
        for (Ratio ratio : results) {
            if (multiplier.equals(ratio.getMultiplier())) {
                return getString(R.string.duplicate_ratio);
            }
        }

        return null;
    }

    private void addNewObjectToRealm(String multiplierStr) {
        Integer nextId = mRealm.allObjects(Ratio.class).max("id").intValue() + 1;
        Integer multiplier = Integer.parseInt(multiplierStr);
        String ratioTitle = "1:" + multiplierStr;

        mRealm.beginTransaction();
        Ratio ratio = mRealm.createObject(Ratio.class);
        ratio.setId(nextId);
        ratio.setRatioTitle(ratioTitle);
        ratio.setMultiplier(multiplier);
        ratio.setActive(false);
        ratio.setProduct_action_id(mProductAction.getNumericType());
        ratio.setUser_id(CallParams.USER_ID);
        mRealm.commitTransaction();
        mRatioItemAdapter.updateList(results);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setToolbarTitle();
        View rootView = inflater.inflate(R.layout.fragment_ratio, container, false);
        ListView mRatioItemLv = (ListView) rootView.findViewById(R.id.ratioItemLv);

        mRatioItemAdapter = new RatioItemAdapter(getActivity());
        mRatioItemLv.setAdapter(mRatioItemAdapter);

        mRatioItemLv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(final AdapterView<?> adapterView, View view, final int position, long id) {
                // Check can delete or not
                if (!canDeleteRatio(position)) {
                    AlertDialogManager.showNoticeAlertDialog(getActivity(), getString(R.string.error),
                            getString(R.string.cant_delete_ratio));
                    return true;
                }

                // Create dialog builder
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                // Set property
                alertDialogBuilder.setTitle(getString(R.string.confirm));

                String ratioTitle = mRatioItemAdapter.getItem(position).getRatioTitle();

                alertDialogBuilder.setMessage(getString(R.string.confirm_delete_ratio, ratioTitle))
                        .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                mRealm.beginTransaction();
                                results.remove(position);
                                mRealm.commitTransaction();
                                mRatioItemAdapter.updateList(results);
                            }
                        })
                        .setNegativeButton(getString(R.string.cancel), null);

                // create alert dialog
                AlertDialog mAlertDialog = alertDialogBuilder.create();
                // show it
                mAlertDialog.show();

                return true;
            }
        });

        return rootView;
    }

    private boolean canDeleteRatio(int position) {
        int countActive = 0;
        Ratio item = results.get(position);
        if (!item.getActive()) {
            return true;
        }
        else {
            for (int i = 0; i < results.size(); i++) {
                Ratio item2 = results.get(i);
                if (item2.getActive()) {
                    countActive++;
                }
            }
        }

        return countActive > 1;
    }

    private void setToolbarTitle() {
        String toolbarTitle;

        switch (mProductAction) {
            case INCOMING_SHIPMENTS:
                toolbarTitle = getString(R.string.incoming_ratio);
                break;
            case DELIVERY_ORDERS:
                toolbarTitle = getString(R.string.delivery_ratio);
                break;
            default:
                toolbarTitle = getString(R.string.adjustment_ratio);
        }

        MainActivity mainActivity = (MainActivity) getActivity();
        mainActivity.setToolbarTitle(toolbarTitle);
    }

    @Override
    public void onStart() {
        super.onStart();
        mRealm = Realm.getDefaultInstance();
        results = mRealm.where(Ratio.class)
                .equalTo("product_action_id", mProductAction.getNumericType())
                .equalTo("user_id", CallParams.USER_ID)
                .findAll();
        results.sort("multiplier");
        mRatioItemAdapter.updateList(results);
    }

    @Override
    public void onStop() {
        super.onStop();
        mRealm.close();
    }
}
