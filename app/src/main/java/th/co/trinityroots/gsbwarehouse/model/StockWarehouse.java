package th.co.trinityroots.gsbwarehouse.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import th.co.trinityroots.gsbwarehouse.utility.OdooUtility;

/**
 * Realm stock warehouse from master data
 */
public class StockWarehouse extends RealmObject {

    @PrimaryKey
    private Integer id;

    private String name;
    private Integer lot_stock_id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getLot_stock_id() {
        return lot_stock_id;
    }

    public void setLot_stock_id(Integer lot_stock_id) {
        this.lot_stock_id = lot_stock_id;
    }

    public static List<StockWarehouse> parseData(Object[] classObjs) {
        List<StockWarehouse> stockWarehouses = new ArrayList<>();

        for (Object object : classObjs) {
            @SuppressWarnings("unchecked")
            Map<String, Object> classObj = (Map<String, Object>) object;

            StockWarehouse sw = new StockWarehouse();
            sw.setId(OdooUtility.getInteger(classObj, "id"));
            sw.setName(OdooUtility.getString(classObj, "name"));

            M2OField lot_stock_id_m2OField = OdooUtility.getMany2One(classObj, "lot_stock_id");
            sw.setLot_stock_id(lot_stock_id_m2OField.id);

            stockWarehouses.add(sw);
        }

        return stockWarehouses;
    }

}
