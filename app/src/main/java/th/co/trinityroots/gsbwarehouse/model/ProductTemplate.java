package th.co.trinityroots.gsbwarehouse.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import th.co.trinityroots.gsbwarehouse.utility.OdooUtility;

/**
 * Realm product template from master data
 */
public class ProductTemplate extends RealmObject {

    @PrimaryKey
    private Integer id;

    private String name;
    private String barcode;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public static List<ProductTemplate> parseData(Object[] classObjs) {
        List<ProductTemplate> productTemplates = new ArrayList<>();

        for (Object object : classObjs) {
            @SuppressWarnings("unchecked")
            Map<String, Object> classObj = (Map<String, Object>) object;

            ProductTemplate pt = new ProductTemplate();
            pt.setId(OdooUtility.getInteger(classObj, "id"));
            pt.setName(OdooUtility.getString(classObj, "name"));
            pt.setBarcode(OdooUtility.getString(classObj, "barcode"));

            productTemplates.add(pt);
        }

        return productTemplates;
    }

}
