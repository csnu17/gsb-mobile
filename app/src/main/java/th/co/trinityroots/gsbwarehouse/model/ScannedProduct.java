package th.co.trinityroots.gsbwarehouse.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ScannedProduct extends RealmObject {

    @PrimaryKey
    private int id;

    private ReferenceOrder referenceOrder;
    private StockLocation location;
    private ProductTemplate product;
    private String lot_number;
    private int quantity;
    private String breadcrumb_text;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ReferenceOrder getReferenceOrder() {
        return referenceOrder;
    }

    public void setReferenceOrder(ReferenceOrder referenceOrder) {
        this.referenceOrder = referenceOrder;
    }

    public StockLocation getLocation() {
        return location;
    }

    public void setLocation(StockLocation location) {
        this.location = location;
    }

    public ProductTemplate getProduct() {
        return product;
    }

    public void setProduct(ProductTemplate product) {
        this.product = product;
    }

    public String getLot_number() {
        return lot_number;
    }

    public void setLot_number(String lot_number) {
        this.lot_number = lot_number;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getBreadcrumb_text() {
        return breadcrumb_text;
    }

    public void setBreadcrumb_text(String breadcrumb_text) {
        this.breadcrumb_text = breadcrumb_text;
    }
}
