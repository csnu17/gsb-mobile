package th.co.trinityroots.gsbwarehouse.controller.setting;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import th.co.trinityroots.gsbwarehouse.R;
import th.co.trinityroots.gsbwarehouse.adapter.ProductRatioAdapter;
import th.co.trinityroots.gsbwarehouse.controller.MainActivity;
import th.co.trinityroots.gsbwarehouse.enums.ProductAction;
import th.co.trinityroots.gsbwarehouse.helper.Language;

public class FirstSettingFragment extends Fragment {

    private TextView mProductRatioTitleTv;
    private TextView mLanguageTitleTv;
    private TextView mCurrentLangTv;

    private AlertDialog mAlertDialog;
    private ProductRatioAdapter adapter;

    private static String languages[];
    private int selectedLangIndex;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languages = new String[] {
                getString(R.string.english),
                getString(R.string.thai)
        };

        selectedLangIndex = Language.isTH(getActivity()) ? 1 : 0;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Set toolbar's title
        MainActivity mainActivity = (MainActivity) getActivity();
        mainActivity.setToolbarTitle(getString(R.string.nav_setting));

        View rootView = inflater.inflate(R.layout.fragment_first_setting, container, false);
        ListView mProductRatioLv = (ListView) rootView.findViewById(R.id.productRatioLv);
        RelativeLayout rl = (RelativeLayout) rootView.findViewById(R.id.langRl);
        mCurrentLangTv = (TextView) rootView.findViewById(R.id.currentLangTv);
        mProductRatioTitleTv = (TextView) rootView.findViewById(R.id.productRatioTitleTv);
        mLanguageTitleTv = (TextView) rootView.findViewById(R.id.languageTitleTv);

        setLabel();

        mProductRatioLv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                ProductAction productAction;
                switch (position) {
                    case 0:
                        productAction = ProductAction.INCOMING_SHIPMENTS;
                        break;
                    case 1:
                        productAction = ProductAction.DELIVERY_ORDERS;
                        break;
                    default:
                        productAction = ProductAction.INVENTORY_ADJUSTMENTS;
                }

                RatioFragment childFragment = RatioFragment.newInstance(productAction);
                SettingFragment parentFragment = (SettingFragment) getParentFragment();
                parentFragment.swapChildFragment(childFragment);
            }
        });
        adapter = new ProductRatioAdapter(getActivity());
        mProductRatioLv.setAdapter(adapter);

        rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Create dialog builder
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

                // Set property
                alertDialogBuilder.setTitle(getString(R.string.please_select_lang))
                        .setSingleChoiceItems(languages, selectedLangIndex, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                if (i == 0) { // EN
                                    Language.changeLang(getActivity(), Language.LANG_EN);
                                } else { // TH
                                    Language.changeLang(getActivity(), Language.LANG_TH);
                                }

                                setLabel();
                                adapter.updateDataSet(getActivity());
                                MainActivity mainActivity = (MainActivity) getActivity();
                                mainActivity.updateLabel();

                                selectedLangIndex = i;
                                mAlertDialog.dismiss();
                            }
                        });

                // create alert dialog
                mAlertDialog = alertDialogBuilder.create();
                // show it
                mAlertDialog.show();
            }
        });

        return rootView;
    }

    private void setLabel() {
        mCurrentLangTv.setText(getString(R.string.current_lang));
        mProductRatioTitleTv.setText(getString(R.string.products_ratio));
        mLanguageTitleTv.setText(getString(R.string.language));
    }

}
