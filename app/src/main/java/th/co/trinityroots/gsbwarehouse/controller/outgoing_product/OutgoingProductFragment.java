package th.co.trinityroots.gsbwarehouse.controller.outgoing_product;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import th.co.trinityroots.gsbwarehouse.R;

public class OutgoingProductFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_outgoing_product, container, false);

        if (rootView.findViewById(R.id.delivery_order_fragment_container) != null) {
            if (savedInstanceState != null) {
                return null;
            }

            MainDeliveryOrderFragment f = new MainDeliveryOrderFragment();
            getChildFragmentManager().beginTransaction().add(R.id.delivery_order_fragment_container, f).commit();
        }

        return rootView;
    }

    public void swapChildFragment(Fragment childFragment) {
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.replace(R.id.delivery_order_fragment_container, childFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public void clearBackstack() {
        getChildFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

}
