package th.co.trinityroots.gsbwarehouse.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import io.realm.RealmResults;
import th.co.trinityroots.gsbwarehouse.R;
import th.co.trinityroots.gsbwarehouse.helper.DatabaseManager;
import th.co.trinityroots.gsbwarehouse.helper.PackageManager;
import th.co.trinityroots.gsbwarehouse.model.DeliveryOrder;
import th.co.trinityroots.gsbwarehouse.model.ReferenceOrderStatus;

public class DeliveryOrderAdapter extends BaseAdapter {

    private List<DeliveryOrder> dataSet = Collections.emptyList();
    private final LayoutInflater inflater;
    private Context mContext;

    public DeliveryOrderAdapter(Context context) {
        this.inflater = LayoutInflater.from(context);
        mContext = context;
    }

    public void updateList(RealmResults<DeliveryOrder> deliveryOrders) {
        this.dataSet = deliveryOrders;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return dataSet.size();
    }

    @Override
    public DeliveryOrder getItem(int i) {
        return dataSet.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.do_list_item, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        DeliveryOrder deliveryOrder = getItem(position);
        viewHolder.referenceIdTv.setText(deliveryOrder.getOrigin());
        viewHolder.sequenceTv.setText(String.valueOf(deliveryOrder.getSequence()));

        String customerNameStr = mContext.getString(R.string.customer, deliveryOrder.getPartner_name());
        viewHolder.customerNameTv.setText(customerNameStr);

        DatabaseManager databaseManager = new DatabaseManager();
        String do_status_key = databaseManager.queryFromId(ReferenceOrderStatus.class, deliveryOrder.getStatus_id()).getName();
        String do_status_text = mContext.getString(R.string.reference_status,
                mContext.getString(PackageManager.getStringIdentifier(mContext, do_status_key)));
        viewHolder.referenceStatusTv.setText(do_status_text);
        databaseManager.close();

        return convertView;
    }

    private class ViewHolder {
        TextView referenceIdTv;
        TextView referenceStatusTv;
        TextView customerNameTv;
        TextView sequenceTv;

        public ViewHolder(View convertView) {
            referenceIdTv = (TextView) convertView.findViewById(R.id.referenceIdTv);
            referenceStatusTv = (TextView) convertView.findViewById(R.id.referenceStatusTv);
            customerNameTv = (TextView) convertView.findViewById(R.id.customerNameTv);
            sequenceTv = (TextView) convertView.findViewById(R.id.sequenceTv);
        }
    }

}
