package th.co.trinityroots.gsbwarehouse.controller.check_product;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import io.realm.Realm;
import io.realm.RealmResults;
import th.co.trinityroots.gsbwarehouse.R;
import th.co.trinityroots.gsbwarehouse.adapter.WarehouseAdapter;
import th.co.trinityroots.gsbwarehouse.helper.AlertDialogManager;
import th.co.trinityroots.gsbwarehouse.model.ReferenceOrder;
import th.co.trinityroots.gsbwarehouse.model.StockLocation;
import th.co.trinityroots.gsbwarehouse.model.StockWarehouse;

public class WarehouseFragment extends Fragment {

    private WarehouseAdapter adapter;
    private ReferenceOrder order;
    private Realm realm;
    private View bottomLineView;

    public static WarehouseFragment newInstance(ReferenceOrder order) {
        WarehouseFragment f = new WarehouseFragment();
        f.order = order;
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_warehouse, container, false);
        ListView warehouseLv = (ListView) rootView.findViewById(R.id.warehouseLv);
        bottomLineView = rootView.findViewById(R.id.bottomLineView);

        // Create Adapter and set to ListView
        adapter = new WarehouseAdapter(getActivity());
        warehouseLv.setAdapter(adapter);
        warehouseLv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                StockWarehouse selectedWarehouse = realm.copyFromRealm(adapter.getItem(i));

                // Update warehouse_id in order
                ReferenceOrder referenceOrder = realm.where(ReferenceOrder.class).equalTo("id", order.getId()).findFirst();
                realm.beginTransaction();
                referenceOrder.setWarehouse_id(selectedWarehouse.getId());
                realm.commitTransaction();

                RealmResults<StockLocation> nextResult = realm.where(StockLocation.class).equalTo("location_id",
                        selectedWarehouse.getLot_stock_id()).findAll();

                if (nextResult.size() > 0) {
                    order = realm.copyFromRealm(referenceOrder);

                    LocationFragment childFragment = LocationFragment.newInstance(order, selectedWarehouse,
                            null, selectedWarehouse.getName());
                    CheckProductFragment parentFragment = (CheckProductFragment) getParentFragment();
                    parentFragment.swapChildFragment(childFragment);
                } else {
                    AlertDialogManager.showNoticeAlertDialog(getActivity(), getString(R.string.app_name),
                            getString(R.string.not_found_location_for_warehouse));
                }
            }
        });

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        realm = Realm.getDefaultInstance();
        RealmResults<StockWarehouse> result = realm.where(StockWarehouse.class).findAll();
        adapter.updateList(result);

        if (result.size() == 0) {
            bottomLineView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        realm.close();
        bottomLineView.setVisibility(View.VISIBLE);
    }
}
