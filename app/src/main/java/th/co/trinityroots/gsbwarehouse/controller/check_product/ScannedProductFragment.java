package th.co.trinityroots.gsbwarehouse.controller.check_product;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import io.realm.RealmResults;
import th.co.trinityroots.gsbwarehouse.R;
import th.co.trinityroots.gsbwarehouse.adapter.ScannedProductAdapter;
import th.co.trinityroots.gsbwarehouse.enums.ReferenceStatus;
import th.co.trinityroots.gsbwarehouse.helper.AlertDialogManager;
import th.co.trinityroots.gsbwarehouse.helper.DatabaseManager;
import th.co.trinityroots.gsbwarehouse.model.ReferenceOrder;
import th.co.trinityroots.gsbwarehouse.model.ScannedProduct;
import th.co.trinityroots.gsbwarehouse.model.Setting;
import th.co.trinityroots.gsbwarehouse.model.StockLocation;
import th.co.trinityroots.gsbwarehouse.model.StockWarehouse;

public class ScannedProductFragment extends Fragment {

    private DatabaseManager mDatabaseManager;
    private RealmResults<ScannedProduct> scannedProducts;

    private ScannedProductAdapter scannedProductAdapter;

    private ReferenceOrder order;
    private StockWarehouse warehouse;
    private StockLocation location;
    private String breadcrumb_text;

    public static ScannedProductFragment newInstance(ReferenceOrder order, StockWarehouse warehouse,
                                                     StockLocation location, String breadcrumb_text) {
        ScannedProductFragment f = new ScannedProductFragment();
        f.order = order;
        f.warehouse = warehouse;
        f.breadcrumb_text = breadcrumb_text;
        f.location = location;
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_scanned_product, container, false);
        TextView mWarehouseAndLocationTv = (TextView) rootView.findViewById(R.id.warehouseAndLocationTv);
        ListView scannedProductLv = (ListView) rootView.findViewById(R.id.scannedProductLv);
        Button markAsFinishedBtn = (Button) rootView.findViewById(R.id.markFinishBtn);
        Button nextLocationBtn = (Button) rootView.findViewById(R.id.nextLocationBtn);
        Button scanBarcodeBtn = (Button) rootView.findViewById(R.id.scanBarcodeBtn);

        mWarehouseAndLocationTv.setText(breadcrumb_text);
        scannedProductAdapter = new ScannedProductAdapter(getActivity());
        scannedProductLv.setAdapter(scannedProductAdapter);

        markAsFinishedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                markFinishedConfirm();
            }
        });

        nextLocationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LocationFragment locationFragment = LocationFragment.newInstance(order, warehouse, null, warehouse.getName());
                CheckProductFragment checkProductFragment = (CheckProductFragment) getParentFragment();
                checkProductFragment.swapChildFragment(locationFragment);
            }
        });

        scanBarcodeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CheckProductFragment parent = (CheckProductFragment) getParentFragment();
                parent.popBackstackImmediate();
            }
        });

        return rootView;
    }

    private void markFinishedConfirm() {
        // Check can finish Reference or not.
        if (scannedProducts.size() == 0) {
            AlertDialogManager.showNoticeAlertDialog(getActivity(), getString(R.string.error), getString(R.string.please_scan_product));
            return;
        }

        // Create dialog builder
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        // Set property
        alertDialogBuilder.setTitle(getString(R.string.confirm));
        alertDialogBuilder.setMessage(getString(R.string.mark_finished_confirm, order.getRef_id()))
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ReferenceOrder returnOrder = mDatabaseManager.updateReferenceOrder(order.getId(),
                                ReferenceStatus.FINISHED.getNumericType());
                        order = mDatabaseManager.copyToStandaloneObject(returnOrder);

                        ProductInRefNoFragment fragment = ProductInRefNoFragment.newInstance(order);
                        CheckProductFragment parent = (CheckProductFragment) getParentFragment();
                        parent.clearBackstack();
                        parent.swapChildFragment(fragment);
                    }
                })
                .setNegativeButton(getString(R.string.cancel), null);

        // create alert dialog
        AlertDialog mAlertDialog = alertDialogBuilder.create();
        // show it
        mAlertDialog.show();
    }

    @Override
    public void onStart() {
        super.onStart();
        mDatabaseManager = new DatabaseManager();
        scannedProducts = mDatabaseManager.getScannedProducts(order.getId(), location.getId());
        Setting setting = mDatabaseManager.queryFromId(Setting.class, 1);
        scannedProductAdapter.updateList(scannedProducts, setting);
    }

    @Override
    public void onStop() {
        super.onStop();
        mDatabaseManager.close();
    }

}
