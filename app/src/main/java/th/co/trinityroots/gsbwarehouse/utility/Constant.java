package th.co.trinityroots.gsbwarehouse.utility;

/**
 * Constant string
 */
public class Constant {

    // Preference key
    public static final String SERVER_ADDRESS_KEY = "server_address_key";
    public static final String DATABASE_KEY = "database_key";
    public static final String USERNAME_KEY = "username_key";
    public static final String PASSWORD_KEY = "password_key";
    public static final String UID_KEY = "uid_key";
    public static final String CURRENT_PARTNER_KEY = "current_partner_key";
    public static final String LATEST_SYNC_MASTER_DATA_KEY = "latest_sync_master_data_key";
    public static final String LATEST_SYNC_DO_KEY = "latest_sync_do_key";

    // Intent key
    public static final String PARTNER_PUT_EXTRA_KEY = "partner";
    public static final String SCANNER_INTENT_ACTION = "android.intent.ACTION_DECODE_DATA";
    public static final String SCANNER_INTENT_STRING_EXTRA = "barcode_string";

    // Other
    public static final long LOGIN_SESSION_DURATION = 60 * 60 * 24 * 30 * 3; // 3 months
    public static final int TIME_INTERVAL = 2000; // # milliseconds, desired time passed between two back presses.

}
