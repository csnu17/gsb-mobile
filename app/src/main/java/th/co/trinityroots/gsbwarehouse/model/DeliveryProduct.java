package th.co.trinityroots.gsbwarehouse.model;

import java.util.Map;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import th.co.trinityroots.gsbwarehouse.utility.OdooUtility;

public class DeliveryProduct extends RealmObject {

    @PrimaryKey
    private int id;

    private int order_id;
    private Integer product_id;
    private long product_uom_qty;
    private int guide_loc;
    private String guide_sn;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOrder_id() {
        return order_id;
    }

    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }

    public Integer getProduct_id() {
        return product_id;
    }

    public void setProduct_id(Integer product_id) {
        this.product_id = product_id;
    }

    public long getProduct_uom_qty() {
        return product_uom_qty;
    }

    public void setProduct_uom_qty(long product_uom_qty) {
        this.product_uom_qty = product_uom_qty;
    }

    public int getGuide_loc() {
        return guide_loc;
    }

    public void setGuide_loc(int guide_loc) {
        this.guide_loc = guide_loc;
    }

    public String getGuide_sn() {
        return guide_sn;
    }

    public void setGuide_sn(String guide_sn) {
        this.guide_sn = guide_sn;
    }

    public static DeliveryProduct parseData(Integer order_id, Map<String, Object> dataMap) {
        DeliveryProduct deliveryProduct = new DeliveryProduct();
        deliveryProduct.setId(OdooUtility.getInteger(dataMap, "id"));
        deliveryProduct.setOrder_id(order_id);
        deliveryProduct.setProduct_id(OdooUtility.getMany2One(dataMap, "product_id").id);
        deliveryProduct.setProduct_uom_qty(OdooUtility.getDouble(dataMap, "product_uom_qty").longValue());
        deliveryProduct.setGuide_loc(OdooUtility.getInteger(dataMap, "guide_loc"));
        deliveryProduct.setGuide_sn(OdooUtility.getString(dataMap, "guide_sn"));
        return deliveryProduct;
    }
}
