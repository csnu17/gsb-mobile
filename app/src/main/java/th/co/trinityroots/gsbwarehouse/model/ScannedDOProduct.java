package th.co.trinityroots.gsbwarehouse.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ScannedDOProduct extends RealmObject {

    @PrimaryKey
    private int id;

    private DeliveryProduct deliveryProduct;
    private String lot_number;
    private int scan_qty;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public DeliveryProduct getDeliveryProduct() {
        return deliveryProduct;
    }

    public void setDeliveryProduct(DeliveryProduct deliveryProduct) {
        this.deliveryProduct = deliveryProduct;
    }

    public String getLot_number() {
        return lot_number;
    }

    public void setLot_number(String lot_number) {
        this.lot_number = lot_number;
    }

    public int getScan_qty() {
        return scan_qty;
    }

    public void setScan_qty(int scan_qty) {
        this.scan_qty = scan_qty;
    }
}
