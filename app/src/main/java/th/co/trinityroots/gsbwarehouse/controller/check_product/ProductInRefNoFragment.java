package th.co.trinityroots.gsbwarehouse.controller.check_product;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import io.realm.RealmResults;
import th.co.trinityroots.gsbwarehouse.R;
import th.co.trinityroots.gsbwarehouse.adapter.ProductInRefIdAdapter;
import th.co.trinityroots.gsbwarehouse.controller.MainActivity;
import th.co.trinityroots.gsbwarehouse.enums.ReferenceStatus;
import th.co.trinityroots.gsbwarehouse.helper.AlertDialogManager;
import th.co.trinityroots.gsbwarehouse.helper.DatabaseManager;
import th.co.trinityroots.gsbwarehouse.model.ReferenceOrder;
import th.co.trinityroots.gsbwarehouse.model.ScannedProduct;
import th.co.trinityroots.gsbwarehouse.model.Setting;
import th.co.trinityroots.gsbwarehouse.model.StockWarehouse;

public class ProductInRefNoFragment extends Fragment implements View.OnClickListener {

    private Button goBackBtn;
    private Button markAsFinishBtn;
    private Button nextLocationBtn;

    private DatabaseManager mDatabaseManager;
    private RealmResults<ScannedProduct> scannedProducts;
    private ReferenceOrder order;

    private ProductInRefIdAdapter mAdapter;

    public static ProductInRefNoFragment newInstance(ReferenceOrder order) {
        ProductInRefNoFragment fragment = new ProductInRefNoFragment();
        fragment.order = order;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_product_in_ref_no, container, false);
        TextView refNumberTv = (TextView) rootView.findViewById(R.id.refNumberTv);
        ListView allProductsInRefNumberLv = (ListView) rootView.findViewById(R.id.allProductsInRefNumberLv);
        goBackBtn = (Button) rootView.findViewById(R.id.goBackBtn);
        markAsFinishBtn = (Button) rootView.findViewById(R.id.markAsFinishBtn);
        nextLocationBtn = (Button) rootView.findViewById(R.id.nextLocationBtn);

        refNumberTv.setText(order.getRef_id());

        mAdapter = new ProductInRefIdAdapter(getActivity());
        allProductsInRefNumberLv.setAdapter(mAdapter);

        goBackBtn.setOnClickListener(this);
        nextLocationBtn.setOnClickListener(this);
        markAsFinishBtn.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        mDatabaseManager = new DatabaseManager();
        scannedProducts = mDatabaseManager.getScannedProducts(order.getId());
        Setting setting = mDatabaseManager.queryFromId(Setting.class, 1);
        mAdapter.updateList(scannedProducts, setting);

        if (order.getRef_status_id() == ReferenceStatus.FINISHED.getNumericType()) {
            nextLocationBtn.setVisibility(View.GONE);
            markAsFinishBtn.setVisibility(View.GONE);
            goBackBtn.setVisibility(View.VISIBLE);
        }
        else {
            nextLocationBtn.setVisibility(View.VISIBLE);
            markAsFinishBtn.setVisibility(View.VISIBLE);
            goBackBtn.setVisibility(View.GONE);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        mDatabaseManager.close();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.goBackBtn:
                MainActivity mainActivity = (MainActivity) getActivity();
                mainActivity.swapFragment();
                break;

            case R.id.markAsFinishBtn:
                markFinishedConfirm();
                break;

            case R.id.nextLocationBtn:
                StockWarehouse realmWarehouse = mDatabaseManager.queryFromId(StockWarehouse.class, order.getWarehouse_id());
                StockWarehouse warehouse = mDatabaseManager.copyToStandaloneObject(realmWarehouse);

                LocationFragment locationFragment = LocationFragment.newInstance(order, warehouse, null, warehouse.getName());
                CheckProductFragment checkProductFragment = (CheckProductFragment) getParentFragment();
                checkProductFragment.swapChildFragment(locationFragment);
                break;
        }
    }

    private void markFinishedConfirm() {
        // Check can finish Reference or not.
        if (scannedProducts.size() == 0) {
            AlertDialogManager.showNoticeAlertDialog(getActivity(), getString(R.string.error), getString(R.string.please_scan_product));
            return;
        }

        // Create dialog builder
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        // Set property
        alertDialogBuilder.setTitle(getString(R.string.confirm));
        alertDialogBuilder.setMessage(getString(R.string.mark_finished_confirm, order.getRef_id()))
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ReferenceOrder returnOrder = mDatabaseManager.updateReferenceOrder(order.getId()
                                , ReferenceStatus.FINISHED.getNumericType());
                        order = mDatabaseManager.copyToStandaloneObject(returnOrder);

                        ProductInRefNoFragment fragment = ProductInRefNoFragment.newInstance(order);
                        CheckProductFragment parent = (CheckProductFragment) getParentFragment();
                        parent.clearBackstack();
                        parent.swapChildFragment(fragment);
                    }
                })
                .setNegativeButton(getString(R.string.cancel), null);

        // create alert dialog
        AlertDialog mAlertDialog = alertDialogBuilder.create();
        // show it
        mAlertDialog.show();
    }

}
