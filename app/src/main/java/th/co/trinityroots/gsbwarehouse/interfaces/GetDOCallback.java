package th.co.trinityroots.gsbwarehouse.interfaces;

import java.util.List;

public interface GetDOCallback {
    void onSuccess(List<String> deleteDOsName);
    void onFailure(String error_msg);
}
