package th.co.trinityroots.gsbwarehouse.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import io.realm.RealmResults;
import th.co.trinityroots.gsbwarehouse.R;
import th.co.trinityroots.gsbwarehouse.enums.ProductAction;
import th.co.trinityroots.gsbwarehouse.helper.DatabaseManager;
import th.co.trinityroots.gsbwarehouse.model.ScannedProduct;
import th.co.trinityroots.gsbwarehouse.model.Setting;

public class ScannedProductAdapter extends BaseAdapter {

    private List<ScannedProduct> dataSet = Collections.emptyList();
    private final LayoutInflater inflater;
    private Context mContext;
    private Setting setting;

    public ScannedProductAdapter(Context context) {
        this.inflater = LayoutInflater.from(context);
        mContext = context;
    }

    public void updateList(RealmResults<ScannedProduct> dataSet, Setting setting) {
        this.dataSet = dataSet;
        this.setting = setting;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return dataSet.size();
    }

    @Override
    public ScannedProduct getItem(int i) {
        return dataSet.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.scanned_product_item, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        final ScannedProduct scannedProduct = getItem(position);
        viewHolder.productNameTv.setText(scannedProduct.getProduct().getName());
        viewHolder.productLotTv.setText(mContext.getString(R.string.prefix_product_lot, scannedProduct.getLot_number()));
        viewHolder.productQtyTv.setText(String.valueOf(scannedProduct.getQuantity()));

        boolean allowChangeQty;
        int product_action_id = scannedProduct.getReferenceOrder().getProduct_action_id();
        if (product_action_id == ProductAction.INCOMING_SHIPMENTS.getNumericType()) {
            allowChangeQty = setting.getAllow_qty_incoming();
        }
        else if (product_action_id == ProductAction.DELIVERY_ORDERS.getNumericType()) {
            allowChangeQty = setting.getAllow_qty_outgoing();
        }
        else {
            allowChangeQty = setting.getAllow_qty_inventory();
        }

        if (allowChangeQty) {
            viewHolder.minusProductTv.setVisibility(View.VISIBLE);
            viewHolder.plusProductTv.setVisibility(View.VISIBLE);
            viewHolder.productQtyTv.setBackgroundColor(Color.LTGRAY);

            viewHolder.minusProductTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DatabaseManager databaseManager = new DatabaseManager();
                    databaseManager.removeScannedProductQty(scannedProduct);
                    if (!scannedProduct.isValid()) {
                        notifyDataSetChanged();
                    }
                    else {
                        viewHolder.productQtyTv.setText(String.valueOf(scannedProduct.getQuantity()));
                    }
                    databaseManager.close();
                }
            });

            viewHolder.plusProductTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DatabaseManager databaseManager = new DatabaseManager();
                    databaseManager.addScannedProductQty(scannedProduct);
                    viewHolder.productQtyTv.setText(String.valueOf(scannedProduct.getQuantity()));
                    databaseManager.close();
                }
            });
        }
        else {
            viewHolder.minusProductTv.setVisibility(View.GONE);
            viewHolder.plusProductTv.setVisibility(View.GONE);
            viewHolder.productQtyTv.setBackgroundColor(Color.TRANSPARENT);
        }

        return convertView;
    }

    private class ViewHolder {
        TextView productNameTv;
        TextView productLotTv;
        TextView productQtyTv;
        TextView minusProductTv;
        TextView plusProductTv;

        public ViewHolder(View convertView) {
            productNameTv = (TextView) convertView.findViewById(R.id.product_name_tv);
            productLotTv = (TextView) convertView.findViewById(R.id.product_lot_tv);
            productQtyTv = (TextView) convertView.findViewById(R.id.product_qty_tv);
            minusProductTv = (TextView) convertView.findViewById(R.id.minusScannedProductTv);
            plusProductTv = (TextView) convertView.findViewById(R.id.plusScannedProductTv);
        }
    }

}
