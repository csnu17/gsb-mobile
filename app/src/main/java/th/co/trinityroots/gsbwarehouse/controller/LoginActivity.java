package th.co.trinityroots.gsbwarehouse.controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.Gson;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.timroes.axmlrpc.XMLRPCCallback;
import de.timroes.axmlrpc.XMLRPCException;
import de.timroes.axmlrpc.XMLRPCServerException;
import th.co.trinityroots.gsbwarehouse.R;
import th.co.trinityroots.gsbwarehouse.helper.AlertDialogManager;
import th.co.trinityroots.gsbwarehouse.helper.CacheManager;
import th.co.trinityroots.gsbwarehouse.helper.ConnectionDetector;
import th.co.trinityroots.gsbwarehouse.helper.SharedData;
import th.co.trinityroots.gsbwarehouse.helper.ViewHelper;
import th.co.trinityroots.gsbwarehouse.model.M2OField;
import th.co.trinityroots.gsbwarehouse.model.Partner;
import th.co.trinityroots.gsbwarehouse.utility.Constant;
import th.co.trinityroots.gsbwarehouse.utility.OdooModelName;
import th.co.trinityroots.gsbwarehouse.utility.OdooUtility;

public class LoginActivity extends AppCompatActivity implements XMLRPCCallback {

    private EditText serverAddressEdt;
    private EditText databaseEdt;
    private EditText usernameEdt;
    private EditText passwordEdt;
    private Button loginBtn;

    private MaterialDialog mMaterialDialog;

    private String svUrlStr;
    private String dbStr;
    private String userStr;
    private String pwStr;
    private String uid;
    private long authenticateTaskId;
    private long getPartnerIdTaskId;
    private long mBackPressed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Setup view
        bindView();
        setDefaultValueForInputView();

        // Set listener
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                svUrlStr = serverAddressEdt.getText().toString();
                dbStr = databaseEdt.getText().toString();
                userStr = usernameEdt.getText().toString();
                pwStr = passwordEdt.getText().toString();

                View errorFocusView = validateForm();
                if (errorFocusView == null) {
                    ViewHelper viewHelper = new ViewHelper(LoginActivity.this);
                    viewHelper.hideSoftKeyboard(getCurrentFocus());

                    loginProcess();
                } else {
                    errorFocusView.requestFocus();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (mBackPressed + Constant.TIME_INTERVAL > System.currentTimeMillis()) {
            super.onBackPressed();
            return;
        }
        else {
            Toast.makeText(this, getString(R.string.confirm_exit_app), Toast.LENGTH_SHORT).show();
        }
        mBackPressed = System.currentTimeMillis();
    }

    private void bindView() {
        loginBtn = (Button) findViewById(R.id.loginBtn);
        serverAddressEdt = (EditText) findViewById(R.id.serverAddressEdt);
        databaseEdt = (EditText) findViewById(R.id.databaseEdt);
        usernameEdt = (EditText) findViewById(R.id.usernameEdt);
        passwordEdt = (EditText) findViewById(R.id.passwordEdt);
    }

    private void setDefaultValueForInputView() {
        serverAddressEdt.setText(SharedData.getStringFromKey(this, Constant.SERVER_ADDRESS_KEY));
        databaseEdt.setText(SharedData.getStringFromKey(this, Constant.DATABASE_KEY));
        usernameEdt.setText(SharedData.getStringFromKey(this, Constant.USERNAME_KEY));
        passwordEdt.setText(SharedData.getStringFromKey(this, Constant.PASSWORD_KEY));
    }

    private View validateForm() {
        svUrlStr = svUrlStr.toLowerCase().trim();
        dbStr = dbStr.trim();
        userStr = userStr.trim();

        View errorFocusView = null;

        // Check server url
        if (TextUtils.isEmpty(svUrlStr)) {
            serverAddressEdt.setError(getString(R.string.server_url_required));
            errorFocusView = serverAddressEdt;
        }
        else {
            serverAddressEdt.setError(null);
        }

        // Check database name
        if (TextUtils.isEmpty(dbStr)) {
            databaseEdt.setError(getString(R.string.database_required));

            if (errorFocusView == null) {
                errorFocusView = databaseEdt;
            }
        }
        else {
            databaseEdt.setError(null);
        }

        // Check username
        if (TextUtils.isEmpty(userStr)) {
            usernameEdt.setError(getString(R.string.username_required));

            if (errorFocusView == null) {
                errorFocusView = usernameEdt;
            }
        }
        else {
            usernameEdt.setError(null);
        }

        // Check password
        if (TextUtils.isEmpty(pwStr)) {
            passwordEdt.setError(getString(R.string.password_required));

            if (errorFocusView == null) {
                errorFocusView = passwordEdt;
            }
        }
        else {
            passwordEdt.setError(null);
        }

        return errorFocusView;
    }

    private void loginProcess() {
        // Check connection
        ConnectionDetector cd = new ConnectionDetector(this);
        if (cd.isConnected()) {
            try {
                if (mMaterialDialog == null) {
                    mMaterialDialog = new MaterialDialog.Builder(this)
                            .title(R.string.progress_dialog_title)
                            .content(R.string.progress_dialog_loading_msg)
                            .progress(true, 0)
                            .cancelable(false)
                            .build();
                }
                mMaterialDialog.show();

                OdooUtility mOdooUtility = new OdooUtility(svUrlStr, OdooUtility.COMMON_PATH);
                authenticateTaskId = mOdooUtility.login(this, dbStr, userStr, pwStr);
                Log.d("GSB", "Authenticate task id: " + authenticateTaskId);
            } catch (Exception e) {
                Log.d("GSB", "Exception: " + e.getLocalizedMessage());
                mMaterialDialog.dismiss();
                AlertDialogManager.showNoticeAlertDialog(this, getString(R.string.error), getString(R.string.login_fail));
            }
        }
        else {
            AlertDialogManager.showNoticeAlertDialog(this, getString(R.string.error), getString(R.string.no_connection));
        }
    }

    @Override
    public void onResponse(long id, Object result) {
        Log.d("GSB", "Task id = " + id);
        Log.d("GSB", "Response = " + result.toString());

        if (id == authenticateTaskId) { // Authentication
            if (result instanceof Boolean && !((Boolean)result)) { // authentication fail
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mMaterialDialog.dismiss();
                        AlertDialogManager.showNoticeAlertDialog(LoginActivity.this, getString(R.string.error), getString(R.string.incorrect_username_password));
                    }
                });
            } else {
                uid = result.toString();

                SharedData.setStringForKey(this, Constant.SERVER_ADDRESS_KEY, svUrlStr);
                SharedData.setStringForKey(this, Constant.DATABASE_KEY, dbStr);
                SharedData.setStringForKey(this, Constant.USERNAME_KEY, userStr);

                // Get partner_id from res.users
                List conditions = Collections.singletonList(
                        Collections.singletonList(
                                Arrays.asList("id", "=", uid)));

                Map fields = new HashMap<String, Object>() {{
                    put("fields", Collections.singletonList("partner_id"));
                    put("limit", 1);
                }};

                try {
                    OdooUtility odoo = new OdooUtility(svUrlStr, OdooUtility.OBJECT_PATH);

                    @SuppressWarnings("unchecked")
                    long getPartnerIdTaskId = odoo.search_read(this, dbStr, uid, pwStr, OdooModelName.USERS, conditions, fields);

                    this.getPartnerIdTaskId = getPartnerIdTaskId;
                    Log.d("GSB", "getPartnerIdTaskId: " + getPartnerIdTaskId);
                } catch (Exception e) {
                    Log.d("GSB", "Exception: " + e.getLocalizedMessage());

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mMaterialDialog.dismiss();
                            AlertDialogManager.showNoticeAlertDialog(LoginActivity.this, getString(R.string.error), getString(R.string.login_fail));
                        }
                    });
                }
            }
        }

        else if (id == getPartnerIdTaskId) { // Get partner id
            Object[] classObjs = (Object[])result;
            if (classObjs.length == 1) {
                @SuppressWarnings("unchecked")
                Map<String, Object> classObj = (Map<String, Object>)classObjs[0];

                M2OField m2o = OdooUtility.getMany2One(classObj, "partner_id");
                Log.d("GSB", "Partner id: " + m2o.id);

                // Get user detail from res.partner
                List conditions = Collections.singletonList(
                        Collections.singletonList(
                                Arrays.asList("id", "=", m2o.id)));

                Map fields = new HashMap<String, Object>() {{
                    put("fields", Arrays.asList("name", "email", "image_medium"));
                    put("limit", 1);
                }};

                try {
                    OdooUtility odoo = new OdooUtility(svUrlStr, OdooUtility.OBJECT_PATH);

                    @SuppressWarnings("unchecked")
                    long getUserDetailTaskId = odoo.search_read(this, dbStr, uid, pwStr, OdooModelName.PARTNER, conditions, fields);

                    Log.d("GSB", "getUserDetailTaskId: " + getUserDetailTaskId);
                } catch (Exception e) {
                    Log.d("GSB", "Exception: " + e.getLocalizedMessage());

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mMaterialDialog.dismiss();
                            AlertDialogManager.showNoticeAlertDialog(LoginActivity.this, getString(R.string.error), getString(R.string.login_fail));
                        }
                    });
                }
            }
            else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mMaterialDialog.dismiss();
                        AlertDialogManager.showNoticeAlertDialog(LoginActivity.this, getString(R.string.error), getString(R.string.not_found_partner_id));
                    }
                });
            }
        }

        else { // Get user detail
            Object[] classObjs = (Object[])result;
            if (classObjs.length == 1) {
                @SuppressWarnings("unchecked")
                Map<String, Object> classObj = (Map<String, Object>)classObjs[0];
                Partner partner = Partner.parseData(classObj);

                Gson gson = new Gson();
                String json = gson.toJson(partner);

                // Save current login cache.
                CacheManager.saveLoginCache(this, uid, pwStr, json);

                final Intent intent = new Intent(this, MainActivity.class);
                intent.putExtra(Constant.PARTNER_PUT_EXTRA_KEY, partner);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        startActivity(intent);
                        finish();
                    }
                });
            }
            else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mMaterialDialog.dismiss();
                        AlertDialogManager.showNoticeAlertDialog(LoginActivity.this, getString(R.string.error), getString(R.string.not_found_partner_id));
                    }
                });
            }
        }
    }

    @Override
    public void onError(long id, XMLRPCException e) {
        Log.d("GSB", "Task id = " + id);
        Log.d("GSB", "Error = " + e.getLocalizedMessage());

        // Perform on main thread.
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mMaterialDialog.dismiss();
                AlertDialogManager.showNoticeAlertDialog(LoginActivity.this, getString(R.string.error), getString(R.string.login_fail));
            }
        });
    }

    @Override
    public void onServerError(long id, XMLRPCServerException e) {
        Log.d("GSB", "Task id = " + id);
        Log.d("GSB", "Server Error = " + e.getLocalizedMessage());

        // Perform on main thread.
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mMaterialDialog.dismiss();
                AlertDialogManager.showNoticeAlertDialog(LoginActivity.this, getString(R.string.error), getString(R.string.server_error));
            }
        });
    }

}
