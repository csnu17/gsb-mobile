package th.co.trinityroots.gsbwarehouse.model;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;

import java.util.Map;

import th.co.trinityroots.gsbwarehouse.utility.Constant;
import th.co.trinityroots.gsbwarehouse.helper.SharedData;
import th.co.trinityroots.gsbwarehouse.utility.OdooUtility;

/**
 * Partner Model
 */
public class Partner implements Parcelable {

    private Integer id;
    private String name;
    private String email;
    private String image_medium;
    private long expireTs;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImage_medium() {
        return image_medium;
    }

    public void setImage_medium(String image_medium) {
        this.image_medium = image_medium;
    }

    public long getExpireTs() {
        return expireTs;
    }

    public void setExpireTs(long expireTs) {
        this.expireTs = expireTs;
    }

    public static Partner parseData(Map<String, Object> classObj) {
        Partner partner = new Partner();

        partner.setId(OdooUtility.getInteger(classObj, "id"));
        partner.setName(OdooUtility.getString(classObj, "name"));
        partner.setEmail(OdooUtility.getString(classObj, "email"));
        partner.setImage_medium(OdooUtility.getString(classObj, "image_medium"));
        partner.setExpireTs(System.currentTimeMillis() / 1000 + Constant.LOGIN_SESSION_DURATION);

        return partner;
    }

    public static Partner getCurrentPartner(Context context) {
        String json = SharedData.getStringFromKey(context, Constant.CURRENT_PARTNER_KEY);
        Gson gson = new Gson();
        return gson.fromJson(json, Partner.class);
    }

    public Partner() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.name);
        dest.writeString(this.email);
        dest.writeString(this.image_medium);
        dest.writeLong(this.expireTs);
    }

    protected Partner(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.name = in.readString();
        this.email = in.readString();
        this.image_medium = in.readString();
        this.expireTs = in.readLong();
    }

    public static final Creator<Partner> CREATOR = new Creator<Partner>() {
        public Partner createFromParcel(Parcel source) {
            return new Partner(source);
        }

        public Partner[] newArray(int size) {
            return new Partner[size];
        }
    };

}
