package th.co.trinityroots.gsbwarehouse.helper;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Get and set pref.
 */
public class SharedData {

    private static final String MyPreferences = "MyPrefs";
    private static SharedPreferences prefs;

    public static String getStringFromKey(Context c, String key) {
        prefs = c.getSharedPreferences(MyPreferences, Context.MODE_PRIVATE);
        return prefs.getString(key, "");
    }

    public static String getStringFromKey(Context c, String key, String defaultValue) {
        prefs = c.getSharedPreferences(MyPreferences, Context.MODE_PRIVATE);
        return prefs.getString(key, defaultValue);
    }

    public static void setStringForKey(Context c, String key, String value) {
        prefs = c.getSharedPreferences(MyPreferences, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static void clearAll(Context c) {
        prefs = c.getSharedPreferences(MyPreferences, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.apply();
    }

}
