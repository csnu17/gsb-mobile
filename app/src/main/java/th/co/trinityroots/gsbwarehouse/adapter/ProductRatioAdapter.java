package th.co.trinityroots.gsbwarehouse.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import th.co.trinityroots.gsbwarehouse.R;

public class ProductRatioAdapter extends BaseAdapter {

    private String[] productsRatio;
    private final LayoutInflater inflater;

    public ProductRatioAdapter(Context context) {
        initDataSet(context);
        this.inflater = LayoutInflater.from(context);
    }

    private void initDataSet(Context context) {
        productsRatio = new String[] {
                context.getString(R.string.nav_incoming_product),
                context.getString(R.string.nav_outgoing_product),
                context.getString(R.string.nav_check_product)
        };
    }

    public void updateDataSet(Context context) {
        initDataSet(context);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return productsRatio.length;
    }

    @Override
    public String getItem(int i) {
        return productsRatio[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.product_ratio_list_item, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.name.setText(getItem(position));

        return convertView;
    }

    private class ViewHolder {
        TextView name;

        public ViewHolder(View convertView) {
            name = (TextView) convertView.findViewById(R.id.mProductRatioNameTv);
        }
    }
}
