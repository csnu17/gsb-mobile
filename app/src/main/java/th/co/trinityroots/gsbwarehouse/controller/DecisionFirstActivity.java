package th.co.trinityroots.gsbwarehouse.controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import th.co.trinityroots.gsbwarehouse.helper.CacheManager;
import th.co.trinityroots.gsbwarehouse.model.Partner;
import th.co.trinityroots.gsbwarehouse.utility.Constant;

public class DecisionFirstActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Get current partner
        Partner partner = Partner.getCurrentPartner(this);

        Intent intent;

        // Check is login
        if (partner == null) {
            intent = new Intent(this, LoginActivity.class);
        }
        else {
            long expireTs = partner.getExpireTs();

            // Check expire timestamp
            if (CacheManager.isExpireWithTimestamp(expireTs)) { // Expire, go to Login page
                CacheManager.clearCache(this);
                intent = new Intent(this, LoginActivity.class);
            }
            else { // Use cache data
                intent = new Intent(this, MainActivity.class);
                intent.putExtra(Constant.PARTNER_PUT_EXTRA_KEY, partner);
            }
        }

        startActivity(intent);
        finish();
    }

}
