package th.co.trinityroots.gsbwarehouse.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import io.realm.RealmResults;
import th.co.trinityroots.gsbwarehouse.R;
import th.co.trinityroots.gsbwarehouse.helper.DatabaseManager;
import th.co.trinityroots.gsbwarehouse.model.DeliveryProduct;
import th.co.trinityroots.gsbwarehouse.model.ProductTemplate;
import th.co.trinityroots.gsbwarehouse.model.ScannedDOProduct;
import th.co.trinityroots.gsbwarehouse.utility.OdooUtility;

public class DeliveryProductAdapter extends BaseAdapter {

    private List<DeliveryProduct> dataSet = Collections.emptyList();
    private RealmResults<ScannedDOProduct> scannedDOProducts;
    private final LayoutInflater inflater;
    private Context mContext;

    public DeliveryProductAdapter(Context context) {
        this.inflater = LayoutInflater.from(context);
        mContext = context;
    }

    public void updateList(RealmResults<DeliveryProduct> dataSet,
                           RealmResults<ScannedDOProduct> scannedDOProducts) {
        this.dataSet = dataSet;
        this.scannedDOProducts = scannedDOProducts;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return dataSet.size();
    }

    @Override
    public DeliveryProduct getItem(int i) {
        return dataSet.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.delivery_product_list_item, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        DeliveryProduct deliveryProduct = getItem(position);

        // Product name
        DatabaseManager databaseManager = new DatabaseManager();
        ProductTemplate productTemplate = databaseManager.queryFromId(ProductTemplate.class, deliveryProduct.getProduct_id());
        if (productTemplate != null) {
            viewHolder.productNameTv.setText(productTemplate.getName());
        }
        else {
            viewHolder.productNameTv.setText("");
        }
        databaseManager.close();

        // Location
        String path = OdooUtility.findLocationPath(deliveryProduct.getGuide_loc());
        viewHolder.productGuideLocTv.setText(mContext.getString(R.string.delivery_product_location, path));

        // Lot
        viewHolder.productLotTv.setText(mContext.getString(R.string.lot_no, deliveryProduct.getGuide_sn()));

        // Scanned/Max qty
        int scanned_qty = 0;
        for (ScannedDOProduct scannedDOProduct : scannedDOProducts) {
            if (scannedDOProduct.getDeliveryProduct().getProduct_id().equals(deliveryProduct.getProduct_id())) {
                scanned_qty += scannedDOProduct.getScan_qty();
            }
        }
        long max_qty = deliveryProduct.getProduct_uom_qty();
        String qtyText = scanned_qty + "/" + max_qty;
        viewHolder.productQtyTv.setText(qtyText);

        return convertView;
    }

    private class ViewHolder {
        TextView productNameTv;
        TextView productGuideLocTv;
        TextView productLotTv;
        TextView productQtyTv;

        public ViewHolder(View convertView) {
            productNameTv = (TextView) convertView.findViewById(R.id.product_name_tv);
            productGuideLocTv = (TextView) convertView.findViewById(R.id.product_guide_loc);
            productLotTv = (TextView) convertView.findViewById(R.id.product_lot_tv);
            productQtyTv = (TextView) convertView.findViewById(R.id.product_qty_tv);
        }
    }

}
