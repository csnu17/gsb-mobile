package th.co.trinityroots.gsbwarehouse.helper;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.TypedValue;

import th.co.trinityroots.gsbwarehouse.R;

public class ColorHelper {

    public static int fetchAccentColor(Context context) {
        TypedValue typedValue = new TypedValue();
        TypedArray a = context.obtainStyledAttributes(typedValue.data, new int[] { R.attr.colorAccent });
        int color = a.getColor(0, 0);
        a.recycle();
        return color;
    }

}
