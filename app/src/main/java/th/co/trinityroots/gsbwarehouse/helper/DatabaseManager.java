package th.co.trinityroots.gsbwarehouse.helper;

import android.content.Context;
import android.util.Log;

import java.util.List;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmObject;
import io.realm.RealmResults;
import th.co.trinityroots.gsbwarehouse.enums.ReferenceStatus;
import th.co.trinityroots.gsbwarehouse.model.DeliveryOrder;
import th.co.trinityroots.gsbwarehouse.model.DeliveryProduct;
import th.co.trinityroots.gsbwarehouse.model.ProductAction;
import th.co.trinityroots.gsbwarehouse.model.Ratio;
import th.co.trinityroots.gsbwarehouse.model.ReferenceOrder;
import th.co.trinityroots.gsbwarehouse.model.ReferenceOrderStatus;
import th.co.trinityroots.gsbwarehouse.model.ScannedDOProduct;
import th.co.trinityroots.gsbwarehouse.model.ScannedProduct;
import th.co.trinityroots.gsbwarehouse.model.StockWarehouse;
import th.co.trinityroots.gsbwarehouse.utility.CallParams;
import th.co.trinityroots.gsbwarehouse.utility.Constant;
import th.co.trinityroots.gsbwarehouse.utility.OdooUtility;

/**
 * Realm database manager
 */
public class DatabaseManager {

    private Realm realm;

    public DatabaseManager() {
        realm = Realm.getDefaultInstance();
    }

    public static void setupDatabase(Context context) {
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(context)
                .name("gsb.realm")
                .deleteRealmIfMigrationNeeded()
                .build();

        Realm.setDefaultConfiguration(realmConfiguration);

        // Delete last update time if Realm is deleted cause by migration.
        Realm realm = Realm.getDefaultInstance();
        if (realm.isEmpty()) {
            SharedData.setStringForKey(context, Constant.LATEST_SYNC_MASTER_DATA_KEY, null);
            SharedData.setStringForKey(context, Constant.LATEST_SYNC_DO_KEY, null);
        }

        Log.d("GSB", "Realm Path: " + realm.getPath());
        realm.close();
    }

    public static void deleteDatabase() {
        // Remove Realm file
        Realm realm = Realm.getDefaultInstance();
        RealmConfiguration defaultConfig = realm.getConfiguration();
        while (!realm.isClosed()) {
            realm.close();
        }
        Realm.deleteRealm(defaultConfig);
    }

    public <E extends RealmObject> RealmResults<E> query(Class<E> clazz) {
        return realm.where(clazz).findAll();
    }

    public <E extends RealmObject> E queryFromId(Class<E> clazz, int id) {
        return realm.where(clazz).equalTo("id", id).findFirst();
    }

    public <E extends RealmObject> RealmResults<E> queryFromUserId(Class<E> clazz, int user_id) {
        return realm.where(clazz).equalTo("user_id", user_id).findAll();
    }

    public <E extends RealmObject> E copyToStandaloneObject(E realmObject) {
        return realm.copyFromRealm(realmObject);
    }

    public <E extends RealmObject> void clear(E realmObject) {
        realm.beginTransaction();
        realmObject.removeFromRealm();
        realm.commitTransaction();
    }

    public <E extends RealmObject> void clearAll(RealmResults<E> results) {
        realm.beginTransaction();
        results.clear();
        realm.commitTransaction();
    }

    public <E extends RealmObject> int findNextId(Class<E> clazz) {
        int nextId;
        RealmResults<E> results = query(clazz);
        if (results.size() == 0) {
            nextId = 1;
        }
        else {
            nextId = results.max("id").intValue() + 1;
        }
        return nextId;
    }

    public void close() {
        realm.close();
    }

    // Product Action
    public void createOrUpdateProductAction() {
        final String[] names = new String[] {"Incoming Shipments", "Delivery Orders", "Inventory Adjustments"};

        realm.beginTransaction();

        int id = 1;
        for (String name : names) {
            ProductAction productAction = new ProductAction();
            productAction.setId(id);
            productAction.setName(name);
            realm.copyToRealmOrUpdate(productAction);
            id++;
        }

        realm.commitTransaction();
    }

    // Ratio
    public void createDefaultRatioIfNeed(int user_id) {
        final String ratioTitle[] = {"1:1", "1:5", "1:10", "1:15", "1:25"};
        final Integer multipliers[] = {1, 5, 10, 15, 25};

        RealmResults<Ratio> ratios = queryFromUserId(Ratio.class, user_id);
        if (ratios.size() == 0) {
            realm.beginTransaction();

            // Incoming shipments, Delivery orders, Inventory Adjustments
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < ratioTitle.length; j++) {
                    int id = findNextId(Ratio.class);

                    Ratio ratio = realm.createObject(Ratio.class);
                    ratio.setId(id);
                    ratio.setRatioTitle(ratioTitle[j]);
                    ratio.setMultiplier(multipliers[j]);
                    ratio.setActive(true);
                    ratio.setProduct_action_id(i + 1);
                    ratio.setUser_id(user_id);
                }
            }

            realm.commitTransaction();
        }
    }

    public RealmResults<Ratio> getRatios(boolean active, int product_action_id, int user_id) {
        return realm.where(Ratio.class)
                .equalTo("active", active)
                .equalTo("product_action_id", product_action_id)
                .equalTo("user_id", user_id)
                .findAll();
    }

    // Reference Order status
    public void createOrUpdateReferenceOrderStatus() {
        String[] statuses = new String[] {"unfinished", "finished", "unpicked", "picked", "unknown"};

        int id = 1;
        realm.beginTransaction();
        for (String status : statuses) {
            ReferenceOrderStatus orderStatus = new ReferenceOrderStatus();
            orderStatus.setId(id);
            orderStatus.setName(status);
            realm.copyToRealmOrUpdate(orderStatus);
            id++;
        }
        realm.commitTransaction();
    }

    // Incoming Shipments and Inventory Adjustments
    public RealmResults<ScannedProduct> getScannedProducts(int order_id) {
        return realm.where(ScannedProduct.class)
                .equalTo("referenceOrder.id", order_id)
                .findAll();
    }

    public RealmResults<ScannedProduct> getScannedProducts(int order_id, int location_id) {
        return realm.where(ScannedProduct.class)
                .equalTo("referenceOrder.id", order_id)
                .equalTo("location.id", location_id)
                .findAll();
    }

    public void removeScannedProductQty(ScannedProduct scannedProduct) {
        realm.beginTransaction();
        if (scannedProduct.getQuantity() <= 1) {
            scannedProduct.removeFromRealm();
        }
        else {
            scannedProduct.setQuantity(scannedProduct.getQuantity() - 1);
        }
        realm.commitTransaction();
    }

    public void addScannedProductQty(ScannedProduct scannedProduct) {
        realm.beginTransaction();
        scannedProduct.setQuantity(scannedProduct.getQuantity() + 1);
        realm.commitTransaction();
    }

    // Reference Order
    public RealmResults<ReferenceOrder> getReferenceOrders(int product_action_id, int user_id) {
        return realm.where(ReferenceOrder.class)
                .equalTo("product_action_id", product_action_id)
                .equalTo("user_id", user_id)
                .findAll();
    }

    public RealmResults<ReferenceOrder> getReferenceOrders(int product_action_id, int ref_status_id, int user_id) {
        return realm.where(ReferenceOrder.class)
                .equalTo("product_action_id", product_action_id)
                .equalTo("ref_status_id", ref_status_id)
                .equalTo("user_id", user_id)
                .findAll();
    }

    public ReferenceOrder getReferenceOrder(int order_id) {
        return realm.where(ReferenceOrder.class)
                .equalTo("id", order_id)
                .findFirst();
    }

    public ReferenceOrder updateReferenceOrder(int order_id, int ref_status_id) {
        realm.beginTransaction();
        ReferenceOrder referenceOrder = getReferenceOrder(order_id);
        referenceOrder.setRef_status_id(ref_status_id);
        realm.commitTransaction();
        return referenceOrder;
    }

    // Delivery Order
    public void createOrUpdateDeliveryOrder(Map<String, Object> dataMap) {
        DeliveryOrder deliveryOrder = new DeliveryOrder();
        deliveryOrder.setId(OdooUtility.getInteger(dataMap, "id"));
        deliveryOrder.setOrder_name(OdooUtility.getString(dataMap, "name"));
        deliveryOrder.setDate(OdooUtility.getString(dataMap, "date"));
        deliveryOrder.setMin_date(OdooUtility.getString(dataMap, "min_date"));
        deliveryOrder.setWrite_date(OdooUtility.getString(dataMap, "write_date"));
        deliveryOrder.setPartner_name(OdooUtility.getMany2One(dataMap, "partner_id").value);
        deliveryOrder.setRoute_name(OdooUtility.getMany2One(dataMap, "route_id").value);
        deliveryOrder.setUser_id(CallParams.USER_ID);
        deliveryOrder.setStatus_id(ReferenceStatus.UNPICKED.getNumericType());
        deliveryOrder.setOrigin(OdooUtility.getString(dataMap, "origin"));
        deliveryOrder.setSequence(OdooUtility.getInteger(dataMap, "sequence"));

        realm.beginTransaction();
        realm.copyToRealmOrUpdate(deliveryOrder);
        realm.commitTransaction();
    }

    public void createOrUpdateDeliveryProducts(List<DeliveryProduct> deliveryProductList) {
        realm.beginTransaction();

        for (DeliveryProduct item : deliveryProductList) {
            DeliveryProduct existingProduct = realm.where(DeliveryProduct.class)
                    .equalTo("order_id", item.getOrder_id())
                    .equalTo("product_id", item.getProduct_id())
                    .findFirst();

            if (existingProduct == null) { // Insert new product
                realm.copyToRealmOrUpdate(item);
            }
            else { // Update product_uom_qty
                existingProduct.setProduct_uom_qty(existingProduct.getProduct_uom_qty() + item.getProduct_uom_qty());
            }
        }

        realm.commitTransaction();
    }

    public RealmResults<DeliveryProduct> getDeliveryProductFromDOId(int do_id) {
        return realm.where(DeliveryProduct.class)
                .equalTo("order_id", do_id)
                .findAll();
    }

    public RealmResults<ScannedDOProduct> getScannedDOProductsFromDOId(int do_id) {
        return realm.where(ScannedDOProduct.class)
                .equalTo("deliveryProduct.order_id", do_id)
                .findAll();
    }

    public void addScannedDOProductQty(ScannedDOProduct scannedDOProduct, int qty) {
        realm.beginTransaction();
        scannedDOProduct.setScan_qty(scannedDOProduct.getScan_qty() + qty);
        realm.commitTransaction();
    }

    public void removeScannedDOProductQty(ScannedDOProduct scannedDOProduct, int qty) {
        realm.beginTransaction();
        if (scannedDOProduct.getScan_qty() <= qty) {
            scannedDOProduct.removeFromRealm();
        }
        else {
            scannedDOProduct.setScan_qty(scannedDOProduct.getScan_qty() - qty);
        }
        realm.commitTransaction();
    }

    public ScannedDOProduct addNewScannedDOProduct(DeliveryProduct deliveryProduct, String lot_number, int qty) {
        int nextId = findNextId(ScannedDOProduct.class);
        ScannedDOProduct scannedDOProduct = new ScannedDOProduct();
        scannedDOProduct.setId(nextId);
        scannedDOProduct.setDeliveryProduct(deliveryProduct);
        scannedDOProduct.setLot_number(lot_number);
        scannedDOProduct.setScan_qty(qty);

        realm.beginTransaction();
        ScannedDOProduct realmScannedDOProduct = realm.copyToRealmOrUpdate(scannedDOProduct);
        realm.commitTransaction();

        return realmScannedDOProduct;
    }

    public DeliveryOrder changeDOStatus(int do_id, int status_id) {
        DeliveryOrder deliveryOrder = queryFromId(DeliveryOrder.class, do_id);
        realm.beginTransaction();
        deliveryOrder.setStatus_id(status_id);
        realm.commitTransaction();

        return deliveryOrder;
    }

    public RealmResults<DeliveryOrder> getDeliveryOrders(int user_id, int status_id) {
        return realm.where(DeliveryOrder.class)
                .equalTo("user_id", user_id)
                .equalTo("status_id", status_id)
                .findAll();
    }

    // Warehouse
    public StockWarehouse queryWarehouse(int lot_stock_id) {
        return realm.where(StockWarehouse.class)
                .equalTo("lot_stock_id", lot_stock_id)
                .findFirst();
    }

}
