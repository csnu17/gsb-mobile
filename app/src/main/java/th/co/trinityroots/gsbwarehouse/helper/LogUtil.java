package th.co.trinityroots.gsbwarehouse.helper;

import android.util.Log;

import th.co.trinityroots.gsbwarehouse.BuildConfig;

public class LogUtil {

    public static final String LOG_TAG = "GSB";

    public static void print(String message) {
        if (BuildConfig.DEBUG) {
            Log.d(LOG_TAG, message);
        }
    }

}
