package th.co.trinityroots.gsbwarehouse.utility;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.timroes.axmlrpc.XMLRPCCallback;
import de.timroes.axmlrpc.XMLRPCException;
import de.timroes.axmlrpc.XMLRPCServerException;
import io.realm.Realm;
import th.co.trinityroots.gsbwarehouse.R;
import th.co.trinityroots.gsbwarehouse.interfaces.SyncProductListener;
import th.co.trinityroots.gsbwarehouse.interfaces.SyncSettingListener;
import th.co.trinityroots.gsbwarehouse.interfaces.SyncStockLocationListener;
import th.co.trinityroots.gsbwarehouse.interfaces.SyncStockWarehouseListener;
import th.co.trinityroots.gsbwarehouse.model.ProductTemplate;
import th.co.trinityroots.gsbwarehouse.model.Setting;
import th.co.trinityroots.gsbwarehouse.model.StockLocation;
import th.co.trinityroots.gsbwarehouse.model.StockWarehouse;

/**
 * Contain method that perform sync master data process.
 */
public class SyncMasterDataUtility {

    private Context mContext;

    private SyncProductListener mSyncProductListener;
    private SyncStockWarehouseListener mSyncStockWarehouseListener;
    private SyncStockLocationListener mSyncStockLocationListener;

    public Realm mRealm;

    public SyncMasterDataUtility(Context mContext) {
        this.mContext = mContext;
        mRealm = Realm.getDefaultInstance();
    }

    public void syncProduct(SyncProductListener listener) {
        mSyncProductListener = listener;

        List conditions = Collections.singletonList(
                Collections.singletonList(
                        Arrays.asList("type", "=", "product")));

        Map fields = new HashMap<String, Object>() {{
            put("fields", Arrays.asList("name", "barcode"));
            put("order", "id");
        }};

        try {
            OdooUtility odoo = new OdooUtility(CallParams.serverAddress, OdooUtility.OBJECT_PATH);
            @SuppressWarnings("unchecked")
            long getProductTaskId = odoo.search_read(getProductCallback, CallParams.database,
                    CallParams.uid, CallParams.password, OdooModelName.PRODUCT, conditions, fields);
            Log.d("GSB", "getProductTaskId: " + getProductTaskId);
        } catch (Exception e) {
            Log.d("GSB", "Exception: " + e.getLocalizedMessage());
            mSyncProductListener.onFailure(mContext.getString(R.string.sync_product_error));
        }
    }

    public void syncStockWarehouse(SyncStockWarehouseListener listener) {
        mSyncStockWarehouseListener = listener;

        List conditions = Collections.emptyList();

        Map fields = new HashMap<String, Object>() {{
            put("fields", Arrays.asList("name", "lot_stock_id"));
            put("order", "id");
        }};

        try {
            OdooUtility odoo = new OdooUtility(CallParams.serverAddress, OdooUtility.OBJECT_PATH);
            @SuppressWarnings("unchecked")
            long getStockWarehouseTaskId = odoo.search_read(getStockWarehouseCallback, CallParams.database,
                    CallParams.uid, CallParams.password, OdooModelName.WAREHOUSE, conditions, fields);
            Log.d("GSB", "getStockWarehouseTaskId: " + getStockWarehouseTaskId);
        } catch (Exception e) {
            Log.d("GSB", "Exception: " + e.getLocalizedMessage());
            mSyncStockWarehouseListener.onFailure(mContext.getString(R.string.sync_stock_warehouse_error));
        }
    }

    public void syncStockLocation(SyncStockLocationListener listener) {
        mSyncStockLocationListener = listener;

        List conditions = Collections.singletonList(
                Arrays.asList(
                        Arrays.asList("usage", "=", "internal"),
                        Arrays.asList("active", "=", true)));

        Map fields = new HashMap<String, Object>() {{
            put("fields", Arrays.asList("name", "location_id"));
            put("order", "id");
        }};

        try {
            OdooUtility odoo = new OdooUtility(CallParams.serverAddress, OdooUtility.OBJECT_PATH);
            @SuppressWarnings("unchecked")
            long getStockLocationTaskId = odoo.search_read(getStockLocationCallback, CallParams.database,
                    CallParams.uid, CallParams.password, OdooModelName.LOCATION, conditions, fields);
            Log.d("GSB", "getStockLocationTaskId: " + getStockLocationTaskId);
        } catch (Exception e) {
            Log.d("GSB", "Exception: " + e.getLocalizedMessage());
            mSyncStockLocationListener.onFailure(mContext.getString(R.string.sync_stock_location_error));
        }
    }

    public void syncSetting(final SyncSettingListener listener) {
        List conditions = Collections.singletonList(
                Collections.singletonList(
                        Arrays.asList("id", "=", 1)));

        Map fields = new HashMap<String, Object>() {{
            put("fields", Arrays.asList(
                    "back_order",
                    "allow_qty_incoming",
                    "allow_qty_outgoing",
                    "allow_qty_inventory",
                    "product_digit_start",
                    "product_digit_end",
                    "lot_digit_start",
                    "lot_digit_end"
            ));
            put("order", "id");
        }};

        try {
            OdooUtility odoo = new OdooUtility(CallParams.serverAddress, OdooUtility.OBJECT_PATH);
            @SuppressWarnings("unchecked")
            long getSettingTaskId = odoo.search_read(new XMLRPCCallback() {

                 @Override
                 public void onResponse(long id, Object result) {
                     Log.d("GSB", "Task id = getSettingTaskId");
                     Log.d("GSB", "Response = " + result.toString());

                     Object[] classObjs = (Object[])result;
                     if (classObjs.length > 0) {
                         final List<Setting> settings = Setting.parseData(classObjs);

                         mRealm.executeTransaction(new Realm.Transaction() {
                             @Override
                             public void execute(Realm bgRealm) {
                                 /*
                                 // Delete old setting in Realm
                                 RealmResults<Setting> realmResults = bgRealm.where(Setting.class).findAll();
                                 if (realmResults.size() > 0) {
                                     realmResults.clear();
                                 }
                                 */

                                 bgRealm.copyToRealmOrUpdate(settings);
                             }
                         }, new Realm.Transaction.Callback() {
                             @Override
                             public void onSuccess() {
                                 ((Activity) mContext).runOnUiThread(new Runnable() {
                                     @Override
                                     public void run() {
                                         listener.onSuccess();
                                     }
                                 });
                             }

                             @Override
                             public void onError(Exception e) {
                                 Log.d("GSB", "Realm save setting exception: " + e.getLocalizedMessage());

                                 ((Activity) mContext).runOnUiThread(new Runnable() {
                                     @Override
                                     public void run() {
                                         listener.onFailure(mContext.getString(R.string.realm_save_setting_fail));
                                     }
                                 });
                             }
                         });
                     }
                     else {
                         ((Activity)mContext).runOnUiThread(new Runnable() {
                             @Override
                             public void run() {
                                 listener.onFailure(mContext.getString(R.string.setting_not_found));
                             }
                         });
                     }
                 }

                 @Override
                 public void onError(long id, XMLRPCException error) {
                     Log.d("GSB", "Task id = getSettingTaskId");
                     Log.d("GSB", "Error = " + error.getLocalizedMessage());

                     ((Activity)mContext).runOnUiThread(new Runnable() {
                         @Override
                         public void run() {
                             listener.onFailure(mContext.getString(R.string.sync_setting_error));
                         }
                     });
                 }

                 @Override
                 public void onServerError(long id, XMLRPCServerException error) {
                     Log.d("GSB", "Task id = getSettingTaskId");
                     Log.d("GSB", "Server Error = " + error.getLocalizedMessage());

                     ((Activity)mContext).runOnUiThread(new Runnable() {
                         @Override
                         public void run() {
                             listener.onFailure(mContext.getString(R.string.server_error));
                         }
                     });
                 }

             }, CallParams.database, CallParams.uid, CallParams.password, OdooModelName.COMPANY, conditions, fields);

            Log.d("GSB", "getSettingTaskId: " + getSettingTaskId);
        } catch (Exception e) {
            Log.d("GSB", "Sync Setting exception: " + e.getLocalizedMessage());
            listener.onFailure(mContext.getString(R.string.sync_setting_error));
        }
    }

    private final XMLRPCCallback getProductCallback = new XMLRPCCallback() {
        @Override
        public void onResponse(long id, Object result) {
            Log.d("GSB", "Task id = getProductTaskId");
            Log.d("GSB", "Response = " + result.toString());

            Object[] classObjs = (Object[])result;
            if (classObjs.length > 0) {
                List<ProductTemplate> productTemplates = ProductTemplate.parseData(classObjs);
                saveProductTemplateToRealm(productTemplates);
            }
            else {
                ((Activity)mContext).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mSyncProductListener.onFailure(mContext.getString(R.string.product_not_found));
                    }
                });
            }
        }

        @Override
        public void onError(long id, XMLRPCException error) {
            Log.d("GSB", "Task id = getProductTaskId");
            Log.d("GSB", "Error = " + error.getLocalizedMessage());

            ((Activity)mContext).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mSyncProductListener.onFailure(mContext.getString(R.string.sync_product_error));
                }
            });
        }

        @Override
        public void onServerError(long id, XMLRPCServerException error) {
            Log.d("GSB", "Task id = getProductTaskId");
            Log.d("GSB", "Server Error = " + error.getLocalizedMessage());

            ((Activity)mContext).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mSyncProductListener.onFailure(mContext.getString(R.string.server_error));
                }
            });
        }
    };

    private final XMLRPCCallback getStockWarehouseCallback = new XMLRPCCallback() {
        @Override
        public void onResponse(long id, Object result) {
            Log.d("GSB", "Task id = getStockWarehouseTaskId");
            Log.d("GSB", "Response = " + result.toString());

            Object[] classObjs = (Object[])result;
            if (classObjs.length > 0) {
                List<StockWarehouse> stockWarehouses = StockWarehouse.parseData(classObjs);
                saveStockWarehouseToRealm(stockWarehouses);
            }
            else {
                ((Activity)mContext).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mSyncStockWarehouseListener.onFailure(mContext.getString(R.string.stock_warehouse_not_found));
                    }
                });
            }
        }

        @Override
        public void onError(long id, XMLRPCException error) {
            Log.d("GSB", "Task id = getStockWarehouseTaskId");
            Log.d("GSB", "Error = " + error.getLocalizedMessage());

            ((Activity)mContext).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mSyncStockWarehouseListener.onFailure(mContext.getString(R.string.sync_stock_warehouse_error));
                }
            });
        }

        @Override
        public void onServerError(long id, XMLRPCServerException error) {
            Log.d("GSB", "Task id = getStockWarehouseTaskId");
            Log.d("GSB", "Server Error = " + error.getLocalizedMessage());

            ((Activity)mContext).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mSyncStockWarehouseListener.onFailure(mContext.getString(R.string.server_error));
                }
            });
        }
    };

    private final XMLRPCCallback getStockLocationCallback = new XMLRPCCallback() {
        @Override
        public void onResponse(long id, Object result) {
            Log.d("GSB", "Task id = getStockLocationTaskId");
            Log.d("GSB", "Response = " + result.toString());

            Object[] classObjs = (Object[])result;
            if (classObjs.length > 0) {
                List<StockLocation> stockLocations = StockLocation.parseData(classObjs);
                saveStockLocationToRealm(stockLocations);
            }
            else {
                ((Activity)mContext).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mSyncStockLocationListener.onFailure(mContext.getString(R.string.stock_location_not_found));
                    }
                });
            }
        }

        @Override
        public void onError(long id, XMLRPCException error) {
            Log.d("GSB", "Task id = getStockLocationTaskId");
            Log.d("GSB", "Error = " + error.getLocalizedMessage());

            ((Activity)mContext).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mSyncStockLocationListener.onFailure(mContext.getString(R.string.sync_stock_location_error));
                }
            });
        }

        @Override
        public void onServerError(long id, XMLRPCServerException error) {
            Log.d("GSB", "Task id = getStockLocationTaskId");
            Log.d("GSB", "Server Error = " + error.getLocalizedMessage());

            ((Activity)mContext).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mSyncStockLocationListener.onFailure(mContext.getString(R.string.server_error));
                }
            });
        }
    };

    private void saveProductTemplateToRealm(final List<ProductTemplate> productTemplates) {
        mRealm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm bgRealm) {
                /*
                // Delete old product in Realm
                RealmResults<ProductTemplate> realmResults = bgRealm.where(ProductTemplate.class).findAll();
                if (realmResults.size() > 0) {
                    realmResults.clear();
                }
                */

                bgRealm.copyToRealmOrUpdate(productTemplates);
            }
        }, new Realm.Transaction.Callback() {
            @Override
            public void onSuccess() {
                ((Activity) mContext).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mSyncProductListener.onSuccess();
                    }
                });
            }

            @Override
            public void onError(Exception e) {
                Log.d("GSB", "Realm save product template exception: " + e.getLocalizedMessage());

                ((Activity) mContext).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mSyncProductListener.onFailure(mContext.getString(R.string.realm_save_product_template_fail));
                    }
                });
            }
        });
    }

    private void saveStockWarehouseToRealm(final List<StockWarehouse> stockWarehouses) {
        mRealm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm bgRealm) {
                /*
                // Delete old warehouse in Realm
                RealmResults<StockWarehouse> realmResults = bgRealm.where(StockWarehouse.class).findAll();
                if (realmResults.size() > 0) {
                    realmResults.clear();
                }
                */

                bgRealm.copyToRealmOrUpdate(stockWarehouses);
            }
        }, new Realm.Transaction.Callback() {
            @Override
            public void onSuccess() {
                ((Activity) mContext).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mSyncStockWarehouseListener.onSuccess();
                    }
                });
            }

            @Override
            public void onError(Exception e) {
                Log.d("GSB", "Realm save warehouse exception: " + e.getLocalizedMessage());

                ((Activity) mContext).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mSyncStockWarehouseListener.onFailure(mContext.getString(R.string.realm_save_warehouse_fail));
                    }
                });
            }
        });
    }

    private void saveStockLocationToRealm(final List<StockLocation> stockLocations) {
        mRealm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm bgRealm) {
                /*
                // Delete old location in Realm
                RealmResults<StockLocation> realmResults = bgRealm.where(StockLocation.class).findAll();
                if (realmResults.size() > 0) {
                    realmResults.clear();
                }
                */

                bgRealm.copyToRealmOrUpdate(stockLocations);
            }
        }, new Realm.Transaction.Callback() {
            @Override
            public void onSuccess() {
                ((Activity) mContext).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mSyncStockLocationListener.onSuccess();
                    }
                });
            }

            @Override
            public void onError(Exception e) {
                Log.d("GSB", "Realm save warehouse location exception: " + e.getLocalizedMessage());

                ((Activity) mContext).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mSyncStockLocationListener.onFailure(mContext.getString(R.string.realm_save_location_fail));
                    }
                });
            }
        });
    }

}
