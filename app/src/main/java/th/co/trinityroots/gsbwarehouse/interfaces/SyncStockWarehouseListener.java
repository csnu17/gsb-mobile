package th.co.trinityroots.gsbwarehouse.interfaces;

public interface SyncStockWarehouseListener {
    void onSuccess();
    void onFailure(String error_msg);
}
