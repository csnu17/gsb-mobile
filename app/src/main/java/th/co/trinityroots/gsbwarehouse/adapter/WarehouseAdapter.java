package th.co.trinityroots.gsbwarehouse.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import io.realm.RealmResults;
import th.co.trinityroots.gsbwarehouse.R;
import th.co.trinityroots.gsbwarehouse.model.StockWarehouse;

public class WarehouseAdapter extends BaseAdapter {

    private List<StockWarehouse> warehouses = Collections.emptyList();
    private final LayoutInflater inflater;

    public WarehouseAdapter(Context context) {
        this.inflater = LayoutInflater.from(context);
    }

    public void updateList(RealmResults<StockWarehouse> warehouses) {
        this.warehouses = warehouses;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return warehouses.size();
    }

    @Override
    public StockWarehouse getItem(int i) {
        return warehouses.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.warehouse_list_item, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        StockWarehouse item = getItem(position);
        viewHolder.name.setText(item.getName());

        return convertView;
    }

    private class ViewHolder {
        TextView name;

        public ViewHolder(View convertView) {
            name = (TextView) convertView.findViewById(R.id.mWarehouseNameTv);
        }
    }
}