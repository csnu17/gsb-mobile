package th.co.trinityroots.gsbwarehouse.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ReferenceOrder extends RealmObject {

    @PrimaryKey
    private int id;

    private String ref_id;
    private int ref_status_id;
    private int product_action_id;
    private int warehouse_id;
    private int user_id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRef_id() {
        return ref_id;
    }

    public void setRef_id(String ref_id) {
        this.ref_id = ref_id;
    }

    public int getRef_status_id() {
        return ref_status_id;
    }

    public void setRef_status_id(int ref_status_id) {
        this.ref_status_id = ref_status_id;
    }

    public int getProduct_action_id() {
        return product_action_id;
    }

    public void setProduct_action_id(int product_action_id) {
        this.product_action_id = product_action_id;
    }

    public int getWarehouse_id() {
        return warehouse_id;
    }

    public void setWarehouse_id(int warehouse_id) {
        this.warehouse_id = warehouse_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }
}
