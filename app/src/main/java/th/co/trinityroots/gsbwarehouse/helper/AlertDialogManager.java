package th.co.trinityroots.gsbwarehouse.helper;

import android.content.Context;
import android.support.v7.app.AlertDialog;

import th.co.trinityroots.gsbwarehouse.R;

/**
 * The class that manage about AlertDialog.
 */
public class AlertDialogManager {

    // Alert Dialog with only 'OK' button for notice something to user.
    public static void showNoticeAlertDialog(Context context, String title, String message) {
        // Create dialog builder
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        // Set property
        alertDialogBuilder.setTitle(title)
                .setMessage(message)
                .setPositiveButton(context.getString(R.string.ok), null);

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        // show it
        alertDialog.show();
    }

}
