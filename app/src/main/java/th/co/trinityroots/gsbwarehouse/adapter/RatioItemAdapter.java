package th.co.trinityroots.gsbwarehouse.adapter;

import android.content.Context;
import android.support.v7.widget.AppCompatCheckBox;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import th.co.trinityroots.gsbwarehouse.R;
import th.co.trinityroots.gsbwarehouse.helper.AlertDialogManager;
import th.co.trinityroots.gsbwarehouse.model.Ratio;

public class RatioItemAdapter extends BaseAdapter {

    private List<Ratio> dataSet = Collections.emptyList();
    private final LayoutInflater inflater;
    private Context mContext;

    public RatioItemAdapter(Context context) {
        this.inflater = LayoutInflater.from(context);
        mContext = context;
    }

    public void updateList(RealmResults<Ratio> dataSet) {
        this.dataSet = dataSet;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return dataSet.size();
    }

    @Override
    public Ratio getItem(int i) {
        return dataSet.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.ratio_list_item, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        // Clear previous checkbox listener
        viewHolder.ratioItemCb.setOnCheckedChangeListener(null);

        final Ratio item = getItem(position);
        viewHolder.ratioTitleTv.setText(item.getRatioTitle());
        viewHolder.ratioItemCb.setChecked(item.getActive());

        viewHolder.ratioItemCb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                int countActive = getCountActive();
                // Keep at least one ratio
                if (!isChecked && countActive <= 1) {
                    AlertDialogManager.showNoticeAlertDialog(mContext, mContext.getString(R.string.error),
                            mContext.getString(R.string.cant_deselect_ratio));
                    compoundButton.setChecked(true);
                    return ;
                }

                if (!isChecked || countActive < 5) {
                    Realm realm = Realm.getDefaultInstance();
                    realm.beginTransaction();
                    item.setActive(isChecked);
                    realm.commitTransaction();
                    realm.close();
                }
                else {
                    AlertDialogManager.showNoticeAlertDialog(mContext, mContext.getString(R.string.error),
                            mContext.getString(R.string.cant_select_ratio));
                    compoundButton.setChecked(false);
                }
            }
        });

        return convertView;
    }

    private int getCountActive() {
        int countActive = 0;
        for (Ratio ratio : dataSet) {
            if (ratio.getActive()) {
                countActive++;
            }
        }

        return countActive;
    }

    private class ViewHolder {
        TextView ratioTitleTv;
        AppCompatCheckBox ratioItemCb;

        public ViewHolder(View convertView) {
            ratioTitleTv = (TextView) convertView.findViewById(R.id.ratioTitleTv);
            ratioItemCb = (AppCompatCheckBox) convertView.findViewById(R.id.ratioItemCb);
        }
    }
}
