package th.co.trinityroots.gsbwarehouse.controller.check_product;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import io.realm.Realm;
import io.realm.RealmResults;
import th.co.trinityroots.gsbwarehouse.R;
import th.co.trinityroots.gsbwarehouse.enums.ProductAction;
import th.co.trinityroots.gsbwarehouse.enums.ReferenceStatus;
import th.co.trinityroots.gsbwarehouse.helper.AlertDialogManager;
import th.co.trinityroots.gsbwarehouse.helper.ViewHelper;
import th.co.trinityroots.gsbwarehouse.model.ReferenceOrder;
import th.co.trinityroots.gsbwarehouse.utility.CallParams;

public class FillReferenceFragment extends Fragment {

    private EditText referenceNoEdt;
    private Realm mRealm;

    private String refStr;
    private ViewHelper mViewHelper;
    private ProductAction mProductAction;

    public static FillReferenceFragment newInstance(ProductAction mProductAction) {
        FillReferenceFragment f = new FillReferenceFragment();
        f.mProductAction = mProductAction;
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_fill_reference, container, false);
        TextInputLayout orderTIL = (TextInputLayout) rootView.findViewById(R.id.orderTIL);
        TextView referenceHeaderTv = (TextView) rootView.findViewById(R.id.referenceHeaderTv);

        mViewHelper = new ViewHelper(getActivity());

        if (mProductAction == ProductAction.INCOMING_SHIPMENTS) {
            orderTIL.setHint(getString(R.string.hint_incoming_input));
            referenceHeaderTv.setText(getString(R.string.incoming_reference));
        }
        else {
            orderTIL.setHint(getString(R.string.hint_reference_input));
            referenceHeaderTv.setText(getString(R.string.inventory_reference));
        }

        referenceNoEdt = (EditText) rootView.findViewById(R.id.referenceNoEdt);
        Button nextStepBtn = (Button) rootView.findViewById(R.id.nextStepBtn);
        nextStepBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                processToNextStep();
            }
        });

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        mViewHelper.showSoftKeyboard(referenceNoEdt);
        mRealm = Realm.getDefaultInstance();
    }

    @Override
    public void onStop() {
        super.onStop();
        mRealm.close();
    }

    private void processToNextStep() {
        // Validate input
        refStr = referenceNoEdt.getText().toString().trim();
        if (TextUtils.isEmpty(refStr)) {
            AlertDialogManager.showNoticeAlertDialog(getActivity(), getString(R.string.error),
                    getString(R.string.inventory_ref_require));
            return;
        }

        RealmResults<ReferenceOrder> referenceOrders = mRealm.where(ReferenceOrder.class)
                .equalTo("product_action_id", mProductAction.getNumericType())
                .equalTo("ref_id", refStr)
                .equalTo("user_id", CallParams.USER_ID)
                .findAll();

        // Check existing ref_id in same product_action_id in same user_id
        if (referenceOrders.size() == 0) {
            mViewHelper.hideSoftKeyboard(referenceNoEdt);

            // Create dialog builder
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
            // Set property
            alertDialogBuilder.setTitle(getString(R.string.confirm))
                    .setMessage(getString(R.string.confirm_inventory_ref_no_msg, refStr))
                    .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            ReferenceOrder referenceOrder = mRealm.copyFromRealm(createNewReferenceOrder());

                            WarehouseFragment childFragment = WarehouseFragment.newInstance(referenceOrder);
                            CheckProductFragment parentFragment = (CheckProductFragment) getParentFragment();
                            parentFragment.swapChildFragment(childFragment);
                        }
                    })
                    .setNegativeButton(getString(R.string.cancel), null);

            // create alert dialog
            AlertDialog alert = alertDialogBuilder.create();

            // show it
            alert.show();
        }
        else {
            AlertDialogManager.showNoticeAlertDialog(getActivity(), getString(R.string.error),
                    getString(R.string.duplicate_ref_id, refStr));
        }
    }

    private ReferenceOrder createNewReferenceOrder() {
        int nextId;
        RealmResults<ReferenceOrder> allReferenceOrders = mRealm.allObjects(ReferenceOrder.class);
        if (allReferenceOrders.size() == 0) {
            nextId = 1;
        }
        else {
            nextId = allReferenceOrders.max("id").intValue() + 1;
        }

        mRealm.beginTransaction();
        ReferenceOrder referenceOrder = mRealm.createObject(ReferenceOrder.class);
        referenceOrder.setId(nextId);
        referenceOrder.setRef_id(refStr);
        referenceOrder.setRef_status_id(ReferenceStatus.UNFINISHED.getNumericType());
        referenceOrder.setProduct_action_id(mProductAction.getNumericType());
        referenceOrder.setWarehouse_id(0);
        referenceOrder.setUser_id(CallParams.USER_ID);
        mRealm.commitTransaction();

        return referenceOrder;
    }

}
