package th.co.trinityroots.gsbwarehouse.utility;

import android.util.Log;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.timroes.axmlrpc.XMLRPCCallback;
import de.timroes.axmlrpc.XMLRPCClient;
import th.co.trinityroots.gsbwarehouse.helper.DatabaseManager;
import th.co.trinityroots.gsbwarehouse.model.M2OField;
import th.co.trinityroots.gsbwarehouse.model.StockLocation;
import th.co.trinityroots.gsbwarehouse.model.StockWarehouse;

/**
 * Connect Odoo webservice.
 */
public class OdooUtility {

    public static final String COMMON_PATH = "common";
    public static final String OBJECT_PATH = "object";
    private static final int requestTimeOut = 60; // Seconds

    private XMLRPCClient client;

    public OdooUtility(String serverAddress, String path) {
        try {
            if (! (serverAddress.startsWith("http://") || serverAddress.startsWith("https://"))) {
                serverAddress = "http://" + serverAddress;
            }

            URL url = new URL(serverAddress + "/xmlrpc/2/" + path);
            client = new XMLRPCClient(url);
            client.setTimeout(requestTimeOut);
        } catch (Exception e) {
            Log.e("Odoo Utility: ", e.getLocalizedMessage());
        }
    }

    public long checkServerVersion(XMLRPCCallback listener) {
        return client.callAsync(listener, "version");
    }

    public long login(XMLRPCCallback listener, String db, String username, String password) {
        Map<String, Object> emptyMap = new HashMap<String, Object>();
        return client.callAsync(listener, "authenticate", db, username, password, emptyMap);
    }

    public long search_read(XMLRPCCallback listener, String db, String uid, String password
            , String object, List conditions, Map<String, List> fields)
    {
        return client.callAsync(listener, "execute_kw", db, Integer.parseInt(uid), password, object, "search_read", conditions, fields);
    }

    public long fields_get(String db, String uid, String password, String object,
                           List conditions, Map<String, List> fields, XMLRPCCallback listener) {
        return client.callAsync(listener, "execute_kw", db, Integer.parseInt(uid), password, object, "fields_get", conditions, fields);
    }

    public long create(XMLRPCCallback listener, String db, String uid, String password, String object, List data) {
        return client.callAsync(listener, "execute_kw", db, Integer.parseInt(uid), password, object, "create", data);
    }

    public long exec(XMLRPCCallback listener, String db, String uid, String password, String object, String method, List data) {
        return client.callAsync(listener, "execute_kw", db, Integer.parseInt(uid), password, object, method, data);
    }

    public long update(XMLRPCCallback listener, String db, String uid, String password, String object, List data) {
        return client.callAsync(listener, "execute_kw", db, Integer.parseInt(uid), password, object, "write", data);
    }

    public long delete(XMLRPCCallback listener, String db, String uid, String password, String object, List data) {
        return client.callAsync(listener, "execute_kw", db, Integer.parseInt(uid), password, object, "unlink", data);
    }

    public void cancelTask(long taskId) {
        if (client != null) {
            client.cancel(taskId);
        }
    }

    public static M2OField getMany2One(Map<String, Object> classObj, String fieldName) {
        Integer fieldId = 0;
        String fieldValue = "";
        M2OField res = new M2OField();

        if (classObj.get(fieldName) instanceof Object[]) {
            Object[] field = (Object[])classObj.get(fieldName);

            if (field.length > 0) {
                fieldId = (Integer)field[0];
                fieldValue = (String)field[1];
            }
        }
        res.id = fieldId;
        res.value = fieldValue;

        return res;
    }

    public static List getOne2Many(Map<String, Object> classObj, String fieldName) {
        List res = new ArrayList();
        Object[] field = (Object[])classObj.get(fieldName);

        for (int i = 0; i < field.length; i++) {
            res.add(i, field[i]);
        }

        return res;
    }

    public static String getString(Map<String, Object> classObj, String fieldName) {
        String res = "";
        if (classObj.get(fieldName) instanceof String) {
            res = (String)classObj.get(fieldName);
        }

        return res;
    }

    public static Double getDouble(Map<String, Object> classObj, String fieldName) {
        Double res = 0.0;
        if (classObj.get(fieldName) instanceof Double) {
            res = (Double)classObj.get(fieldName);
        }

        return res;
    }

    public static Integer getInteger(Map<String, Object> classObj, String fieldName) {
        Integer res = 0;
        if (classObj.get(fieldName) instanceof Integer) {
            res = (Integer)classObj.get(fieldName);
        }

        return res;
    }

    public static Boolean getBoolean(Map<String, Object> classObj, String fieldName) {
        Boolean res = false;
        if (classObj.get(fieldName) instanceof Boolean) {
            res = (Boolean)classObj.get(fieldName);
        }

        return res;
    }

    public static String findLocationPath(int loc_id) {
        List<String> pathList = new ArrayList<>();
        DatabaseManager databaseManager = new DatabaseManager();

        int root_id = loc_id;
        do {
            StockLocation location = databaseManager.queryFromId(StockLocation.class, loc_id);
            if (location != null) {
                root_id = loc_id;
                loc_id = location.getLocation_id();
                pathList.add(location.getName());
            }
            else {
                break;
            }
        } while (true);

        StockWarehouse stockWarehouse = databaseManager.queryWarehouse(root_id);
        if (stockWarehouse != null) {
            pathList.add(stockWarehouse.getName());
        }

        String path = "";
        for (int i = pathList.size() - 1; i >= 0; i--) {
            path += pathList.get(i);
            if (i > 0) {
                path += "/";
            }
        }

        databaseManager.close();
        return path;
    }

    /* fields_get
    Map fields = new HashMap<String, Object>() {{
            put("attributes", Arrays.asList("string", "help", "type"));
        }};

        new OdooUtility(CallParams.serverAddress, OdooUtility.OBJECT_PATH).fields_get(CallParams.database,
                CallParams.uid, CallParams.password, OdooModelName.USERS, Collections.emptyList(), fields, new XMLRPCCallback() {
                    @Override
                    public void onResponse(long id, Object result) {
                        Log.d("GSB", result.toString());
                    }

                    @Override
                    public void onError(long id, XMLRPCException error) {

                    }

                    @Override
                    public void onServerError(long id, XMLRPCServerException error) {

                    }
                });
     */

}
