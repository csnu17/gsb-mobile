package th.co.trinityroots.gsbwarehouse.controller.outgoing_product;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import io.realm.RealmResults;
import th.co.trinityroots.gsbwarehouse.R;
import th.co.trinityroots.gsbwarehouse.adapter.DeliveryProductAdapter;
import th.co.trinityroots.gsbwarehouse.controller.barcode_scanner.DOBarcodeScannerFragment;
import th.co.trinityroots.gsbwarehouse.helper.DatabaseManager;
import th.co.trinityroots.gsbwarehouse.helper.DateHelper;
import th.co.trinityroots.gsbwarehouse.helper.PackageManager;
import th.co.trinityroots.gsbwarehouse.model.DeliveryOrder;
import th.co.trinityroots.gsbwarehouse.model.DeliveryProduct;
import th.co.trinityroots.gsbwarehouse.model.ReferenceOrderStatus;
import th.co.trinityroots.gsbwarehouse.model.ScannedDOProduct;

public class DeliveryOrderDetailFragment extends Fragment implements View.OnClickListener {

    private LinearLayout doFooterView;
    private Button viewScanListButton;

    private DeliveryProductAdapter deliveryProductAdapter;
    private DatabaseManager databaseManager;

    private DeliveryOrder standaloneDeliveryOrder;

    public static DeliveryOrderDetailFragment newInstance(DeliveryOrder standaloneDeliveryOrder) {
        DeliveryOrderDetailFragment f = new DeliveryOrderDetailFragment();
        f.standaloneDeliveryOrder = standaloneDeliveryOrder;
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_delivery_order_detail, container, false);

        TextView doNumberTv = (TextView) rootView.findViewById(R.id.doNumberTv);
        TextView doStatusTv = (TextView) rootView.findViewById(R.id.doStatusTv);
        TextView customerNameTv = (TextView) rootView.findViewById(R.id.customerNameTv);
        TextView orderDateTv = (TextView) rootView.findViewById(R.id.orderDateTv);
        TextView dueDateTv = (TextView) rootView.findViewById(R.id.dueDateTv);
        TextView sequenceTv = (TextView) rootView.findViewById(R.id.sequenceTv);

        ListView deliveryProductLv = (ListView) rootView.findViewById(R.id.deliveryProductLv);
        doFooterView = (LinearLayout) rootView.findViewById(R.id.doFooterView);
        Button scanBarcodeButton = (Button) rootView.findViewById(R.id.scanBarcodeButton);
        viewScanListButton = (Button) rootView.findViewById(R.id.viewScanListBtn);

        doNumberTv.setText(standaloneDeliveryOrder.getOrigin());
        sequenceTv.setText(String.valueOf(standaloneDeliveryOrder.getSequence()));
        customerNameTv.setText(getString(R.string.customer, standaloneDeliveryOrder.getPartner_name()));

        String orderDateText = DateHelper.formatDateString(standaloneDeliveryOrder.getDate(), "yy-MM-dd HH:mm:ss",
                "dd/MM/yyyy HH:mm", "Asia/Bangkok");
        orderDateTv.setText(getString(R.string.order_date, orderDateText));

        String dueDateText = DateHelper.formatDateString(standaloneDeliveryOrder.getMin_date(), "yy-MM-dd HH:mm:ss",
                "dd/MM/yyyy HH:mm", "Asia/Bangkok");
        dueDateTv.setText(getString(R.string.due_date, dueDateText));

        DatabaseManager databaseManager = new DatabaseManager();
        String do_status_key = databaseManager.queryFromId(ReferenceOrderStatus.class, standaloneDeliveryOrder.getStatus_id()).getName();
        String do_status_text = getString(R.string.reference_status,
                getString(PackageManager.getStringIdentifier(getActivity(), do_status_key)));
        doStatusTv.setText(do_status_text);
        databaseManager.close();

        deliveryProductAdapter = new DeliveryProductAdapter(getActivity());
        deliveryProductLv.setAdapter(deliveryProductAdapter);

        scanBarcodeButton.setOnClickListener(this);
        viewScanListButton.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        databaseManager = new DatabaseManager();
        RealmResults<DeliveryProduct> deliveryProducts = databaseManager.getDeliveryProductFromDOId(standaloneDeliveryOrder.getId());
        RealmResults<ScannedDOProduct> scannedDOProducts = databaseManager.getScannedDOProductsFromDOId(standaloneDeliveryOrder.getId());
        deliveryProductAdapter.updateList(deliveryProducts, scannedDOProducts);

        if (deliveryProducts.size() <= 0) {
            doFooterView.setVisibility(View.GONE);
        }
        else {
            if (scannedDOProducts.size() <= 0) {
                viewScanListButton.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        databaseManager.close();
        doFooterView.setVisibility(View.VISIBLE);
        viewScanListButton.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.scanBarcodeButton:
                DOBarcodeScannerFragment fragment = DOBarcodeScannerFragment.newInstance(standaloneDeliveryOrder);
                OutgoingProductFragment parent = (OutgoingProductFragment) getParentFragment();
                parent.swapChildFragment(fragment);
                break;

            case R.id.viewScanListBtn:
                DeliveryScanListFragment childFragment = DeliveryScanListFragment.newInstance(standaloneDeliveryOrder);
                OutgoingProductFragment parentFragment = (OutgoingProductFragment) getParentFragment();
                parentFragment.swapChildFragment(childFragment);
                break;
        }
    }
}
