package th.co.trinityroots.gsbwarehouse.helper;

import android.content.Context;

public class PackageManager {

    public static int getStringIdentifier(Context pContext, String pString){
        return pContext.getResources().getIdentifier(pString, "string", pContext.getPackageName());
    }

}
