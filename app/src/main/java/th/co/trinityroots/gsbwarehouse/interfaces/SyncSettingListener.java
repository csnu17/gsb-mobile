package th.co.trinityroots.gsbwarehouse.interfaces;

public interface SyncSettingListener {
    void onSuccess();
    void onFailure(String error_msg);
}
