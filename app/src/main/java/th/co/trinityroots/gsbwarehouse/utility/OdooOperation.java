package th.co.trinityroots.gsbwarehouse.utility;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.timroes.axmlrpc.XMLRPCCallback;
import de.timroes.axmlrpc.XMLRPCException;
import de.timroes.axmlrpc.XMLRPCServerException;
import io.realm.RealmResults;
import th.co.trinityroots.gsbwarehouse.R;
import th.co.trinityroots.gsbwarehouse.controller.MainActivity;
import th.co.trinityroots.gsbwarehouse.enums.ProductAction;
import th.co.trinityroots.gsbwarehouse.enums.ReferenceStatus;
import th.co.trinityroots.gsbwarehouse.helper.ConnectionDetector;
import th.co.trinityroots.gsbwarehouse.helper.DatabaseManager;
import th.co.trinityroots.gsbwarehouse.interfaces.CreateDeliveryOrderCallback;
import th.co.trinityroots.gsbwarehouse.interfaces.CreateIncomingCallBack;
import th.co.trinityroots.gsbwarehouse.interfaces.CreateInventoryCallBack;
import th.co.trinityroots.gsbwarehouse.interfaces.GetDOCallback;
import th.co.trinityroots.gsbwarehouse.model.DeliveryOrder;
import th.co.trinityroots.gsbwarehouse.model.DeliveryProduct;
import th.co.trinityroots.gsbwarehouse.model.ReferenceOrder;
import th.co.trinityroots.gsbwarehouse.model.ScannedDOProduct;
import th.co.trinityroots.gsbwarehouse.model.ScannedProduct;
import th.co.trinityroots.gsbwarehouse.model.StockWarehouse;

public class OdooOperation {

    private OdooUtility odoo_object;
    private Context mContext;
    private DatabaseManager databaseManager;

    // Inventory Adjustment
    private int countCreateInventoryTask;
    private String inventoryTaskErrorMsg;
    private CreateInventoryCallBack mCreateInventoryCallBack;
    private List<Integer> inventoryOrderIdSuccess = new ArrayList<>();

    // Incoming Shipment
    private int countCreateIncomingTask;
    private String incomingTaskErrorMsg;
    private CreateIncomingCallBack mCreateIncomingCallBack;
    private List<Integer> incomingOrderIdSuccess = new ArrayList<>();

    // Delivery Order
    // Get DO
    private GetDOCallback mGetDOCallback;
    private int countGetDOTask = 0;
    private List<String> deleteDOsName;
    private List<DeliveryProduct> tmpDeliveryProducts = new ArrayList<>();
    // Create DO
    private int countCreateDOTask;
    private String createDOTaskErrorMsg;
    private CreateDeliveryOrderCallback createDeliveryOrderCallback;
    private List<Integer> createDOIdSuccess = new ArrayList<>();

    public OdooOperation(Context context) {
        odoo_object = new OdooUtility(CallParams.serverAddress, OdooUtility.OBJECT_PATH);
        mContext = context;
    }

    // Inventory Adjustment
    public void createInventory(CreateInventoryCallBack mCreateInventoryCallBack) {
        this.mCreateInventoryCallBack = mCreateInventoryCallBack;

        ConnectionDetector cd = new ConnectionDetector(mContext);
        if (!cd.isConnected()) {
            mCreateInventoryCallBack.onFailure(mContext.getString(R.string.no_connection));
            return;
        }

        databaseManager = new DatabaseManager();
        RealmResults<ReferenceOrder> referenceOrders = databaseManager.getReferenceOrders(ProductAction.INVENTORY_ADJUSTMENTS.getNumericType(),
                ReferenceStatus.FINISHED.getNumericType(), CallParams.USER_ID);

        if (referenceOrders.size() == 0) {
            mCreateInventoryCallBack.onFailure(mContext.getString(R.string.must_have_at_least_finished_order));
            return;
        }

        countCreateInventoryTask = referenceOrders.size();

        for (ReferenceOrder referenceOrder : referenceOrders) {
            final int order_id = referenceOrder.getId();
            ArrayList<Object> obj_inventory = new ArrayList<>();
            ArrayList<Object> obj_line = new ArrayList<>();

            RealmResults<ScannedProduct> scannedProducts = databaseManager.getScannedProducts(referenceOrder.getId());
            for (ScannedProduct scannedProduct : scannedProducts) {
                ArrayList<Object> obj_product = new ArrayList<>();
                obj_product.add(0);
                obj_product.add(false);

                HashMap<String, Object> product_detail = new HashMap<>();
                product_detail.put("product_id", scannedProduct.getProduct().getId());
                product_detail.put("lot_name", scannedProduct.getLot_number());
                product_detail.put("product_qty", scannedProduct.getQuantity());
                product_detail.put("location_id", scannedProduct.getLocation().getId());

                obj_product.add(product_detail);
                obj_line.add(obj_product);
            }

            StockWarehouse warehouse = databaseManager.queryFromId(StockWarehouse.class, referenceOrder.getWarehouse_id());

            HashMap<String, Object> data = new HashMap<>();
            data.put("name", referenceOrder.getRef_id());
            data.put("location_id", warehouse.getLot_stock_id());
            data.put("line_ids", obj_line);

            obj_inventory.add(new ArrayList<>());
            obj_inventory.add(data);

            Log.d("GSB", "obj_inventory: " + obj_inventory.toString());

            odoo_object.exec(new XMLRPCCallback() {
                @Override
                    public void onResponse(long id, Object response) {
                        Log.d("GSB", "Success: " + response.toString());

                        DatabaseManager bgDatabaseManager = new DatabaseManager();
                        RealmResults<ScannedProduct> scannedProducts = bgDatabaseManager.getScannedProducts(order_id);
                        bgDatabaseManager.clearAll(scannedProducts);
                        bgDatabaseManager.close();
                        inventoryOrderIdSuccess.add(order_id);

                        countCreateInventoryTask--;
                        callbackWhenAllInventoryTasksAreFinished();
                    }

                     @Override
                     public void onServerError(long id, XMLRPCServerException error) {
                         Log.d("GSB", "Server Error: " + error.getLocalizedMessage());

                         inventoryTaskErrorMsg = mContext.getString(R.string.server_error);
                         countCreateInventoryTask--;
                         callbackWhenAllInventoryTasksAreFinished();
                     }

                     @Override
                     public void onError(long id, XMLRPCException error) {
                         Log.d("GSB", "Error: " + error.getLocalizedMessage());

                         inventoryTaskErrorMsg = mContext.getString(R.string.create_inventory_fail);
                         countCreateInventoryTask--;
                         callbackWhenAllInventoryTasksAreFinished();
                     }

            }, CallParams.database, CallParams.uid, CallParams.password, OdooModelName.INVENTORY,
                    OdooModelName.ANDROID_CREATE_METHOD, obj_inventory);
        }
    }

    private void callbackWhenAllInventoryTasksAreFinished() {
        if (countCreateInventoryTask == 0) {
            ((MainActivity) mContext).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    for (Integer order_id : inventoryOrderIdSuccess) {
                        ReferenceOrder referenceOrder = databaseManager.getReferenceOrder(order_id);
                        databaseManager.clear(referenceOrder);
                    }
                    databaseManager.close();

                    if (inventoryTaskErrorMsg == null) {
                        mCreateInventoryCallBack.onSuccess();
                    }
                    else {
                        mCreateInventoryCallBack.onFailure(inventoryTaskErrorMsg);
                    }
                }
            });
        }
    }

    // Incoming Shipment
    public void createIncoming(CreateIncomingCallBack mCreateIncomingCallBack) {
        this.mCreateIncomingCallBack = mCreateIncomingCallBack;

        ConnectionDetector cd = new ConnectionDetector(mContext);
        if (!cd.isConnected()) {
            mCreateIncomingCallBack.onFailure(mContext.getString(R.string.no_connection));
            return;
        }

        databaseManager = new DatabaseManager();
        RealmResults<ReferenceOrder> referenceOrders = databaseManager.getReferenceOrders(ProductAction.INCOMING_SHIPMENTS.getNumericType(),
                ReferenceStatus.FINISHED.getNumericType(), CallParams.USER_ID);

        if (referenceOrders.size() == 0) {
            mCreateIncomingCallBack.onFailure(mContext.getString(R.string.must_have_at_least_finished_order));
            return;
        }

        countCreateIncomingTask = referenceOrders.size();

        for (final ReferenceOrder referenceOrder : referenceOrders) {
            final int order_id = referenceOrder.getId();
            final ArrayList<Object> obj_line = new ArrayList<>();

            RealmResults<ScannedProduct> scannedProducts = databaseManager.getScannedProducts(referenceOrder.getId());
            for (ScannedProduct scannedProduct : scannedProducts) {
                ArrayList<Object> obj_product = new ArrayList<>();
                obj_product.add(0);
                obj_product.add(false);

                HashMap<String, Object> product_detail = new HashMap<>();
                product_detail.put("product_id", scannedProduct.getProduct().getId());
                product_detail.put("lot_name", scannedProduct.getLot_number());
                product_detail.put("product_uom_qty", scannedProduct.getQuantity());
                product_detail.put("location_dest_id", scannedProduct.getLocation().getId());

                obj_product.add(product_detail);
                obj_line.add(obj_product);
            }
            Log.d("GSB", "obj_line: " + obj_line.toString());

            List data = Collections.singletonList(new HashMap<String, Object>() {
                {
                    put("origin", referenceOrder.getRef_id());
                    put("move_lines", obj_line);
                }
            });

            odoo_object.exec(new XMLRPCCallback() {
                 @Override
                 public void onResponse(long id, Object response) {
                     Log.d("GSB", "Success: " + response.toString());

                     DatabaseManager bgDatabaseManager = new DatabaseManager();
                     RealmResults<ScannedProduct> scannedProducts = bgDatabaseManager.getScannedProducts(order_id);
                     bgDatabaseManager.clearAll(scannedProducts);
                     bgDatabaseManager.close();
                     incomingOrderIdSuccess.add(order_id);

                     countCreateIncomingTask--;
                     callbackWhenAllIncomingTasksAreFinished();
                 }

                 @Override
                 public void onServerError(long id, XMLRPCServerException error) {
                     Log.d("GSB", "Server Error: " + error.getLocalizedMessage());

                     incomingTaskErrorMsg = mContext.getString(R.string.server_error);
                     countCreateIncomingTask--;
                     callbackWhenAllIncomingTasksAreFinished();
                 }

                 @Override
                 public void onError(long id, XMLRPCException error) {
                     Log.d("GSB", "Error: " + error.getLocalizedMessage());

                     incomingTaskErrorMsg = mContext.getString(R.string.create_incoming_fail);
                     countCreateIncomingTask--;
                     callbackWhenAllIncomingTasksAreFinished();
                 }
            }, CallParams.database, CallParams.uid, CallParams.password, OdooModelName.PICKING,
                    OdooModelName.ANDROID_CREATE_METHOD, data);
        }
    }

    private void callbackWhenAllIncomingTasksAreFinished() {
        if (countCreateIncomingTask == 0) {
            ((MainActivity) mContext).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    for (Integer order_id : incomingOrderIdSuccess) {
                        ReferenceOrder referenceOrder = databaseManager.getReferenceOrder(order_id);
                        databaseManager.clear(referenceOrder);
                    }
                    databaseManager.close();

                    if (incomingTaskErrorMsg == null) {
                        mCreateIncomingCallBack.onSuccess();
                    }
                    else {
                        mCreateIncomingCallBack.onFailure(incomingTaskErrorMsg);
                    }
                }
            });
        }
    }

    // Delivery Order
    // Get Delivery Order
    public void getDOData(GetDOCallback mGetDOCallback) {
        this.mGetDOCallback = mGetDOCallback;

        ConnectionDetector cd = new ConnectionDetector(mContext);
        if (!cd.isConnected()) {
            this.mGetDOCallback.onFailure(mContext.getString(R.string.no_connection));
            return;
        }

        Map fields = new HashMap<String, Object>() {{
            put("fields", Arrays.asList("name", "date", "min_date", "write_date", "partner_id",
                    "route_id", "move_lines", "origin", "sequence"));
        }};

        List conditions = Collections.singletonList(Arrays.asList(
                    Arrays.asList("state", "in", Arrays.asList("confirmed", "partially_available")),
                    Arrays.asList("assign_id", "=", CallParams.USER_ID)
                )
        );

        odoo_object.search_read(new XMLRPCCallback() {
            @Override
            public void onResponse(long id, Object response) {
                Object[] objects = (Object[]) response;
                deleteDOsName = deleteDisappearDO(objects);

                if (objects.length > 0) {
                    countGetDOTask = objects.length;

                    for (Object data : objects) {
                        countGetDOTask--;

                        @SuppressWarnings("unchecked")
                        Map<String, Object> dataMap = (Map<String, Object>) data;

                        // Check need to update data or not
                        DatabaseManager databaseManager = new DatabaseManager();
                        int delivery_order_id = OdooUtility.getInteger(dataMap, "id");
                        String write_date = OdooUtility.getString(dataMap, "write_date");

                        DeliveryOrder deliveryOrder = databaseManager.queryFromId(DeliveryOrder.class, delivery_order_id);
                        if (deliveryOrder == null || !write_date.equalsIgnoreCase(deliveryOrder.getWrite_date())) {
                            Log.d("GSB", "Create or Update Delivery Order Id: " + delivery_order_id);

                            // Clear old scanned do product if need
                            RealmResults<ScannedDOProduct> scannedDOProducts = databaseManager.getScannedDOProductsFromDOId(delivery_order_id);
                            if (scannedDOProducts.size() > 0) {
                                databaseManager.clearAll(scannedDOProducts);
                            }
                            // Clear old delivery product if need
                            RealmResults<DeliveryProduct> deliveryProducts = databaseManager.getDeliveryProductFromDOId(delivery_order_id);
                            if (deliveryProducts.size() > 0) {
                                databaseManager.clearAll(deliveryProducts);
                            }
                            databaseManager.createOrUpdateDeliveryOrder(dataMap);

                            databaseManager.close();

                            List move_lines = OdooUtility.getOne2Many(dataMap, "move_lines");
                            if (move_lines.size() > 0) {
                                countGetDOTask += move_lines.size();
                                for (Object line_id : move_lines) {
                                    move_line((Integer) line_id, delivery_order_id);
                                }
                            }
                            else {
                                callbackWhenAllGetDOTasksAreFinished();
                            }
                        }
                        else {
                            Log.d("GSB", "Not need to update DO Id: " + delivery_order_id);
                            databaseManager.close();
                            callbackWhenAllGetDOTasksAreFinished();
                        }
                    }
                }
                else {
                    callbackWhenAllGetDOTasksAreFinished();
                }
            }

            @Override
            public void onServerError(long id, XMLRPCServerException error) {
                Log.d("GSB", "Server Error: " + error.getLocalizedMessage());

                callbackWhenAllGetDOTasksAreFinished();
            }

            @Override
            public void onError(long id, XMLRPCException error) {
                Log.d("GSB", "Error: " + error.getLocalizedMessage());

                callbackWhenAllGetDOTasksAreFinished();
            }

        }, CallParams.database, CallParams.uid, CallParams.password, OdooModelName.PICKING,
            conditions, fields);
    }

    private void move_line(Integer id, final int order_id) {
        Map fields = new HashMap<String, Object>() {{
            put("fields", Arrays.asList("product_id", "product_uom_qty", "guide_loc", "guide_sn"));
        }};

        List conditions = Collections.singletonList(Collections.singletonList(
                Arrays.asList("id", "=", id)
        ));

        odoo_object.search_read(new XMLRPCCallback() {
            @Override
            public void onResponse(long id, Object response) {
                Object[] objects = (Object[]) response;

                for (Object data : objects) {
                    @SuppressWarnings("unchecked")
                    Map<String, Object> dataMap = (Map<String, Object>) data;
                    tmpDeliveryProducts.add(DeliveryProduct.parseData(order_id, dataMap));
                }

                countGetDOTask--;
                callbackWhenAllGetDOTasksAreFinished();
            }

            @Override
            public void onServerError(long id, XMLRPCServerException error) {
                Log.d("GSB", "Server Error: " + error.getLocalizedMessage());

                countGetDOTask--;
                callbackWhenAllGetDOTasksAreFinished();
            }

            @Override
            public void onError(long id, XMLRPCException error) {
                Log.d("GSB", "Error: " + error.getLocalizedMessage());

                countGetDOTask--;
                callbackWhenAllGetDOTasksAreFinished();
            }

        }, CallParams.database, CallParams.uid, CallParams.password, OdooModelName.MOVE,
                conditions, fields);
    }

    private void callbackWhenAllGetDOTasksAreFinished() {
        if (countGetDOTask == 0) {
            ((MainActivity) mContext).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (tmpDeliveryProducts.size() > 0) {
                        DatabaseManager databaseManager = new DatabaseManager();
                        databaseManager.createOrUpdateDeliveryProducts(tmpDeliveryProducts);
                        databaseManager.close();
                    }

                    mGetDOCallback.onSuccess(deleteDOsName);
                }
            });
        }
    }

    private List<String> deleteDisappearDO(Object[] objects) {
        List<String> deletedDOsName = new ArrayList<>();

        DatabaseManager databaseManager = new DatabaseManager();
        RealmResults<DeliveryOrder> doObjs = databaseManager.queryFromUserId(DeliveryOrder.class, CallParams.USER_ID);

        for (int i = 0; i < doObjs.size(); i++) {
            DeliveryOrder doObj = doObjs.get(i);
            boolean needToDelete = true;

            for (Object data : objects) {
                @SuppressWarnings("unchecked")
                Map<String, Object> dataMap = (Map<String, Object>) data;
                int do_id = OdooUtility.getInteger(dataMap, "id");

                if (doObj.getId() == do_id) {
                    needToDelete = false;
                    break;
                }
            }

            if (needToDelete) { // Delete product inside this DO and then delete DO.
                // Clear old scanned do product if need
                RealmResults<ScannedDOProduct> scannedDOProducts = databaseManager.getScannedDOProductsFromDOId(doObj.getId());
                if (scannedDOProducts.size() > 0) {
                    databaseManager.clearAll(scannedDOProducts);
                }
                // Clear delivery product if need
                RealmResults<DeliveryProduct> deliveryProducts = databaseManager.getDeliveryProductFromDOId(doObj.getId());
                if (deliveryProducts.size() > 0) {
                    databaseManager.clearAll(deliveryProducts);
                }

                deletedDOsName.add(doObj.getOrder_name());
                databaseManager.clear(doObj);
                i--;
            }
        }

        databaseManager.close();
        return deletedDOsName;
    }

    // Create Delivery Order
    public void createDeliveryOrder(CreateDeliveryOrderCallback createDeliveryOrderCallback) {
        this.createDeliveryOrderCallback = createDeliveryOrderCallback;

        databaseManager = new DatabaseManager();
        RealmResults<DeliveryOrder> deliveryOrders = databaseManager.getDeliveryOrders(CallParams.USER_ID,
                ReferenceStatus.PICKED.getNumericType());

        countCreateDOTask = deliveryOrders.size();
        for (DeliveryOrder deliveryOrder : deliveryOrders) {
            final int do_id = deliveryOrder.getId();
            final ArrayList<Object> obj_line = new ArrayList<>();

            RealmResults<ScannedDOProduct> scannedDOProducts = databaseManager.getScannedDOProductsFromDOId(do_id);
            for (ScannedDOProduct scannedDOProduct : scannedDOProducts) {
                HashMap<String, Object> obj_product = new HashMap<>();
                obj_product.put("product_id", scannedDOProduct.getDeliveryProduct().getProduct_id());
                obj_product.put("lot_name", scannedDOProduct.getLot_number());
                obj_product.put("product_qty", scannedDOProduct.getScan_qty());

                obj_line.add(obj_product);
            }

            List data = Collections.singletonList(new HashMap<String, Object>() {
                {
                    put("id", do_id);
                    put("lines", obj_line);
                }
            });
            Log.d("GSB", "Data for DO's id: " + do_id);
            Log.d("GSB", data.toString());

            odoo_object.exec(new XMLRPCCallback() {
                 @Override
                 public void onResponse(long id, Object response) {
                     Log.d("GSB", "Success: " + response.toString());

                     DatabaseManager bgDatabaseManager = new DatabaseManager();
                     // Clear scanned do product
                     RealmResults<ScannedDOProduct> scannedDOProducts = bgDatabaseManager.getScannedDOProductsFromDOId(do_id);
                     if (scannedDOProducts.size() > 0) {
                         bgDatabaseManager.clearAll(scannedDOProducts);
                     }
                     // Clear delivery product
                     RealmResults<DeliveryProduct> deliveryProducts = bgDatabaseManager.getDeliveryProductFromDOId(do_id);
                     if (deliveryProducts.size() > 0) {
                         bgDatabaseManager.clearAll(deliveryProducts);
                     }
                     bgDatabaseManager.close();
                     createDOIdSuccess.add(do_id);

                     countCreateDOTask--;
                     callbackWhenAllDOTasksAreFinished();
                 }

                 @Override
                 public void onServerError(long id, XMLRPCServerException error) {
                     Log.d("GSB", "Server Error: " + error.getLocalizedMessage());

                     createDOTaskErrorMsg = mContext.getString(R.string.server_error);
                     countCreateDOTask--;
                     callbackWhenAllDOTasksAreFinished();
                 }

                 @Override
                 public void onError(long id, XMLRPCException error) {
                     Log.d("GSB", "Error: " + error.getLocalizedMessage());

                     createDOTaskErrorMsg = mContext.getString(R.string.create_do_fail);
                     countCreateDOTask--;
                     callbackWhenAllDOTasksAreFinished();
                 }

            }, CallParams.database, CallParams.uid, CallParams.password, OdooModelName.PICKING,
                    OdooModelName.ANDROID_CREATE_DO, data);
        }
    }

    private void callbackWhenAllDOTasksAreFinished() {
        if (countCreateDOTask == 0) {
            ((MainActivity) mContext).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    for (Integer do_id : createDOIdSuccess) {
                        DeliveryOrder deliveryOrder = databaseManager.queryFromId(DeliveryOrder.class, do_id);
                        databaseManager.clear(deliveryOrder);
                    }
                    databaseManager.close();

                    if (createDOTaskErrorMsg == null) {
                        createDeliveryOrderCallback.onSuccess();
                    }
                    else {
                        createDeliveryOrderCallback.onFailure(createDOTaskErrorMsg);
                    }
                }
            });
        }
    }

}
