package th.co.trinityroots.gsbwarehouse.enums;

public enum ProductAction {
    INCOMING_SHIPMENTS(1),
    DELIVERY_ORDERS(2),
    INVENTORY_ADJUSTMENTS(3);

    private int type;

    ProductAction(int i) {
        this.type = i;
    }

    public int getNumericType() {
        return type;
    }
}
