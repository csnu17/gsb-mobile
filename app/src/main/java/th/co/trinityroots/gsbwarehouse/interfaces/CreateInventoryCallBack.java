package th.co.trinityroots.gsbwarehouse.interfaces;

public interface CreateInventoryCallBack {
    void onSuccess();
    void onFailure(String error_msg);
}
