package th.co.trinityroots.gsbwarehouse.controller.barcode_scanner;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import io.realm.Realm;
import io.realm.RealmResults;
import th.co.trinityroots.gsbwarehouse.R;
import th.co.trinityroots.gsbwarehouse.controller.check_product.CheckProductFragment;
import th.co.trinityroots.gsbwarehouse.controller.check_product.ScannedProductFragment;
import th.co.trinityroots.gsbwarehouse.enums.ProductAction;
import th.co.trinityroots.gsbwarehouse.helper.ColorHelper;
import th.co.trinityroots.gsbwarehouse.helper.LogUtil;
import th.co.trinityroots.gsbwarehouse.model.ProductTemplate;
import th.co.trinityroots.gsbwarehouse.model.Ratio;
import th.co.trinityroots.gsbwarehouse.model.ReferenceOrder;
import th.co.trinityroots.gsbwarehouse.model.ScannedProduct;
import th.co.trinityroots.gsbwarehouse.model.Setting;
import th.co.trinityroots.gsbwarehouse.model.StockLocation;
import th.co.trinityroots.gsbwarehouse.model.StockWarehouse;
import th.co.trinityroots.gsbwarehouse.utility.CallParams;
import th.co.trinityroots.gsbwarehouse.utility.Constant;

public class BarcodeScannerFragment extends Fragment implements View.OnClickListener {

    private TextView mProductNameTv;
    private TextView mProductQtyTv;
    private Button mRatioBtn[] = new Button[5];

    private Realm mRealm;
    private RealmResults<Ratio> ratios;
    private RealmResults<ProductTemplate> products;
    private RealmResults<ScannedProduct> scannedProducts;
    private Setting setting;
    private ReferenceOrder order;

    private String breadcrumb_text;
    private StockWarehouse warehouse;
    private StockLocation location;
    private Integer multiplier;
    private Integer selectedRatioBtnIndex;

    private BroadcastReceiver mReceiver;
    private IntentFilter mFilter;

    public static BarcodeScannerFragment newInstance(ReferenceOrder order, StockWarehouse warehouse,
                                                     StockLocation location,
                                                     String breadcrumb_text) {
        BarcodeScannerFragment f = new BarcodeScannerFragment();
        f.warehouse = warehouse;
        f.location = location;
        f.breadcrumb_text = breadcrumb_text;
        f.order = order;
        return f;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mFilter = new IntentFilter();
        mFilter.addAction(Constant.SCANNER_INTENT_ACTION);

        mReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent != null &&
                        Constant.SCANNER_INTENT_ACTION.equalsIgnoreCase(intent.getAction()) &&
                        setting != null) {
                    String barcode = intent.getStringExtra(Constant.SCANNER_INTENT_STRING_EXTRA);
                    LogUtil.print("barcode: " + barcode);
                    processBarcode(barcode);
                }
                else {
                    showAlertScanBarcodeFail();
                }
            }
        };
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_barcode_scanner, container, false);
        mProductNameTv = (TextView) rootView.findViewById(R.id.productNameTv);
        mProductQtyTv = (TextView) rootView.findViewById(R.id.productQtyTv);
        Button mCompleteBtn = (Button) rootView.findViewById(R.id.completeBtn);
        mRatioBtn[0] = (Button) rootView.findViewById(R.id.ratio1Btn);
        mRatioBtn[1] = (Button) rootView.findViewById(R.id.ratio2Btn);
        mRatioBtn[2] = (Button) rootView.findViewById(R.id.ratio3Btn);
        mRatioBtn[3] = (Button) rootView.findViewById(R.id.ratio4Btn);
        mRatioBtn[4] = (Button) rootView.findViewById(R.id.ratio5Btn);

        mCompleteBtn.setOnClickListener(this);
        for (Button btn : mRatioBtn) {
            btn.setOnClickListener(this);
        }

        return rootView;
    }

    private void processBarcode(String barcodeNumber) {
        int product_start_index = setting.getProduct_digit_start() - 1;
        int product_end_index = setting.getProduct_digit_end();

        String productBarcode;
        try {
            productBarcode = barcodeNumber.substring(product_start_index, product_end_index);
        } catch (Exception e) {
            Log.d("GSB", "Substring product fail: " + e.getLocalizedMessage());
            productBarcode = null;
        }

        ProductTemplate selectedProduct = null;
        for (ProductTemplate product : products) {
            if (product.getBarcode().equalsIgnoreCase(productBarcode)) {
                selectedProduct = product;
                break;
            }
        }

        // Check the barcode is valid
        if (selectedProduct != null) { // valid barcode
            // Find product's lot
            int lot_start_index = setting.getLot_digit_start() - 1;
            int lot_end_index = setting.getLot_digit_end();

            String lotNumber;
            try {
                lotNumber = barcodeNumber.substring(lot_start_index, lot_end_index);
            } catch (Exception e) {
                Log.d("GSB", "Substring lot fail: " + e.getLocalizedMessage());
                lotNumber = null;
            }

            if (lotNumber != null) { // valid lot's number
                if (order.getProduct_action_id() == ProductAction.INVENTORY_ADJUSTMENTS.getNumericType() ||
                        order.getProduct_action_id() == ProductAction.INCOMING_SHIPMENTS.getNumericType()) {
                    ScannedProduct scannedProduct = saveOrUpdateScannedProduct(selectedProduct, lotNumber);
                    mProductNameTv.setText(scannedProduct.getProduct().getName());
                    mProductQtyTv.setText(getString(R.string.total_scan, String.valueOf(scannedProduct.getQuantity())));
                }
            }
            else { // invalid lot's number
                showAlertScanBarcodeFail();
            }
        }
        else { // invalid barcode
            showAlertScanBarcodeFail();
        }
    }

    private ScannedProduct saveOrUpdateScannedProduct(ProductTemplate selectedProduct, String lot_number) {
        for (ScannedProduct scannedProduct : scannedProducts) {
            if (scannedProduct.getProduct().getId().equals(selectedProduct.getId()) &&
                    scannedProduct.getLot_number().equalsIgnoreCase(lot_number)) {

                // Exist product, only update quantity
                mRealm.beginTransaction();
                scannedProduct.setQuantity(scannedProduct.getQuantity() + multiplier);
                mRealm.commitTransaction();

                return scannedProduct;
            }
        }

        // No existing product, insert new scanned product
        Integer nextId;
        RealmResults<ScannedProduct> allScannedProducts = mRealm.where(ScannedProduct.class).findAll();
        if (allScannedProducts.size() == 0) {
            nextId = 1;
        }
        else {
            nextId = allScannedProducts.max("id").intValue() + 1;
        }

        ScannedProduct scannedProduct = new ScannedProduct();
        scannedProduct.setId(nextId);
        scannedProduct.setReferenceOrder(order);
        scannedProduct.setLocation(location);
        scannedProduct.setProduct(selectedProduct);
        scannedProduct.setLot_number(lot_number);
        scannedProduct.setQuantity(multiplier);
        scannedProduct.setBreadcrumb_text(breadcrumb_text);

        mRealm.beginTransaction();
        ScannedProduct scannedProduct1 = mRealm.copyToRealmOrUpdate(scannedProduct);
        mRealm.commitTransaction();

        return scannedProduct1;
    }

    private void showAlertScanBarcodeFail() {
        // Create dialog builder
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        // Set property
        alertDialogBuilder.setTitle(getString(R.string.error))
                .setMessage(getString(R.string.not_found_product_for_this_barcode))
                .setPositiveButton(getString(R.string.ok), null);

        // create alert dialog
        AlertDialog mAlertDialog = alertDialogBuilder.create();
        // show it
        mAlertDialog.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(mReceiver, mFilter);
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(mReceiver);
    }

    @Override
    public void onStart() {
        super.onStart();

        mRealm = Realm.getDefaultInstance();
        products = mRealm.allObjects(ProductTemplate.class);
        setting = mRealm.where(Setting.class).equalTo("id", 1).findFirst();

        ratios = mRealm.where(Ratio.class)
                .equalTo("active", true)
                .equalTo("product_action_id", order.getProduct_action_id())
                .equalTo("user_id", CallParams.USER_ID)
                .findAll();
        ratios.sort("multiplier");

        if (multiplier == null) {
            multiplier = ratios.get(0).getMultiplier();
            selectedRatioBtnIndex = 0;
        }
        mRatioBtn[selectedRatioBtnIndex].setTextColor(ColorHelper.fetchAccentColor(getActivity()));

        for (int i = 0; i < ratios.size(); i++) {
            mRatioBtn[i].setText(ratios.get(i).getRatioTitle());
            mRatioBtn[i].setVisibility(View.VISIBLE);
        }

        scannedProducts = mRealm.where(ScannedProduct.class)
                .equalTo("referenceOrder.id", order.getId())
                .equalTo("location.id", location.getId())
                .findAll();
    }

    @Override
    public void onStop() {
        super.onStop();
        mRealm.close();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.completeBtn:
                if (order.getProduct_action_id() == ProductAction.INCOMING_SHIPMENTS.getNumericType() ||
                        order.getProduct_action_id() == ProductAction.INVENTORY_ADJUSTMENTS.getNumericType()) {
                    ScannedProductFragment targetFragment = ScannedProductFragment.newInstance(order, warehouse, location
                            , breadcrumb_text);
                    CheckProductFragment parent = (CheckProductFragment) getParentFragment();
                    parent.swapChildFragment(targetFragment);
                }

                break;

            case R.id.ratio1Btn:
            case R.id.ratio2Btn:
            case R.id.ratio3Btn:
            case R.id.ratio4Btn:
            case R.id.ratio5Btn:
                for (int i = 0; i < ratios.size(); i++) {
                    if (view == mRatioBtn[i]) {
                        multiplier = ratios.get(i).getMultiplier();
                        mRatioBtn[i].setTextColor(ColorHelper.fetchAccentColor(getActivity()));
                        selectedRatioBtnIndex = i;
                    }
                    else {
                        mRatioBtn[i].setTextColor(Color.WHITE);
                    }
                }
        }
    }
}
