package th.co.trinityroots.gsbwarehouse.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import io.realm.RealmResults;
import th.co.trinityroots.gsbwarehouse.R;
import th.co.trinityroots.gsbwarehouse.enums.ReferenceStatus;
import th.co.trinityroots.gsbwarehouse.model.ReferenceOrder;

public class ReferenceOrderAdapter extends BaseAdapter {

    private List<ReferenceOrder> dataSet = Collections.emptyList();
    private final LayoutInflater inflater;
    private Context mContext;

    public ReferenceOrderAdapter(Context context) {
        this.inflater = LayoutInflater.from(context);
        mContext = context;
    }

    public void updateList(RealmResults<ReferenceOrder> referenceOrders) {
        this.dataSet = referenceOrders;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return dataSet.size();
    }

    @Override
    public ReferenceOrder getItem(int i) {
        return dataSet.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.reference_order_list_item, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        ReferenceOrder referenceOrder = getItem(position);
        viewHolder.referenceIdTv.setText(referenceOrder.getRef_id());

        String statusText;
        if (referenceOrder.getRef_status_id() == ReferenceStatus.UNFINISHED.getNumericType()) {
            statusText = mContext.getString(R.string.reference_status, mContext.getString(R.string.unfinished));
        } else if (referenceOrder.getRef_status_id() == ReferenceStatus.FINISHED.getNumericType()) {
            statusText = mContext.getString(R.string.reference_status, mContext.getString(R.string.finished));
        }
        else {
            statusText = mContext.getString(R.string.reference_status, mContext.getString(R.string.unknown));
        }
        viewHolder.referenceStatusTv.setText(statusText);

        return convertView;
    }

    private class ViewHolder {
        TextView referenceIdTv;
        TextView referenceStatusTv;

        public ViewHolder(View convertView) {
            referenceIdTv = (TextView) convertView.findViewById(R.id.referenceIdTv);
            referenceStatusTv = (TextView) convertView.findViewById(R.id.referenceStatusTv);
        }
    }
}
