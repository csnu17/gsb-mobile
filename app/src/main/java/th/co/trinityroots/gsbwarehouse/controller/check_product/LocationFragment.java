package th.co.trinityroots.gsbwarehouse.controller.check_product;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import io.realm.Realm;
import io.realm.RealmResults;
import th.co.trinityroots.gsbwarehouse.R;
import th.co.trinityroots.gsbwarehouse.adapter.LocationAdapter;
import th.co.trinityroots.gsbwarehouse.controller.barcode_scanner.BarcodeScannerFragment;
import th.co.trinityroots.gsbwarehouse.model.ReferenceOrder;
import th.co.trinityroots.gsbwarehouse.model.StockLocation;
import th.co.trinityroots.gsbwarehouse.model.StockWarehouse;

public class LocationFragment extends Fragment {

    private LocationAdapter adapter;
    private Realm realm;

    private ReferenceOrder order;
    private String previous_breadcrumb_text;
    private StockWarehouse warehouse;
    private StockLocation location;

    public static LocationFragment newInstance(ReferenceOrder order, StockWarehouse warehouse,
            StockLocation location, String previous_breadcrumb_text) {
        LocationFragment f = new LocationFragment();
        f.warehouse = warehouse;
        f.location = location;
        f.previous_breadcrumb_text = previous_breadcrumb_text;
        f.order = order;
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_location, container, false);
        ListView mLocationLv = (ListView) rootView.findViewById(R.id.locationLv);

        TextView mBreadcrumbTv = (TextView) rootView.findViewById(R.id.breadcrumbTv);
        mBreadcrumbTv.setText(previous_breadcrumb_text);

        // Create Adapter and set to ListView
        adapter = new LocationAdapter(getActivity());
        mLocationLv.setAdapter(adapter);
        mLocationLv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                StockLocation selectedLocation = realm.copyFromRealm(adapter.getItem(i));
                Fragment childFragment;

                RealmResults<StockLocation> nextResult = realm.where(StockLocation.class).equalTo("location_id",
                        selectedLocation.getId()).findAll();
                if (nextResult.size() > 0) {
                    childFragment = LocationFragment.newInstance(order, warehouse, selectedLocation,
                            previous_breadcrumb_text + " / " + selectedLocation.getName());
                } else {
                    childFragment = BarcodeScannerFragment.newInstance(order, warehouse, selectedLocation,
                            previous_breadcrumb_text + " / " + selectedLocation.getName());
                }

                CheckProductFragment parentFragment = (CheckProductFragment) getParentFragment();
                parentFragment.swapChildFragment(childFragment);
            }
        });

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        realm = Realm.getDefaultInstance();
        RealmResults<StockLocation> result;
        if (location != null) {
            result = realm.where(StockLocation.class).equalTo("location_id", location.getId()).findAll();
        }
        else {
            result = realm.where(StockLocation.class).equalTo("location_id", warehouse.getLot_stock_id()).findAll();
        }
        adapter.updateList(result);
    }

    @Override
    public void onStop() {
        super.onStop();
        realm.close();
    }

}
