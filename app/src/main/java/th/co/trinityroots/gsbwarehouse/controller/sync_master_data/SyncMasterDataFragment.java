package th.co.trinityroots.gsbwarehouse.controller.sync_master_data;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;

import io.realm.Realm;
import th.co.trinityroots.gsbwarehouse.R;
import th.co.trinityroots.gsbwarehouse.helper.AlertDialogManager;
import th.co.trinityroots.gsbwarehouse.helper.ConnectionDetector;
import th.co.trinityroots.gsbwarehouse.helper.DateHelper;
import th.co.trinityroots.gsbwarehouse.helper.SharedData;
import th.co.trinityroots.gsbwarehouse.interfaces.SyncProductListener;
import th.co.trinityroots.gsbwarehouse.interfaces.SyncSettingListener;
import th.co.trinityroots.gsbwarehouse.interfaces.SyncStockLocationListener;
import th.co.trinityroots.gsbwarehouse.interfaces.SyncStockWarehouseListener;
import th.co.trinityroots.gsbwarehouse.utility.Constant;
import th.co.trinityroots.gsbwarehouse.utility.SyncMasterDataUtility;

public class SyncMasterDataFragment extends Fragment {

    private TextView mLastUpdateDatetimeTv;
    private MaterialDialog mMaterialProgressDialog;

    private int mCounterTask;
    private String mErrorMsg;
    private SyncMasterDataUtility mSyncMasterDataUtility;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_sync_master_data, container, false);

        mLastUpdateDatetimeTv = (TextView) rootView.findViewById(R.id.lastUpdateDatetimeTv);
        String latestSyncMasterData = SharedData.getStringFromKey(getActivity(),
                Constant.LATEST_SYNC_MASTER_DATA_KEY, getString(R.string.default_sync_master_data));
        mLastUpdateDatetimeTv.setText(latestSyncMasterData);

        Button syncDataBtn = (Button) rootView.findViewById(R.id.syncDataButton);
        syncDataBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ConnectionDetector cd = new ConnectionDetector(getActivity());
                if (cd.isConnected()) {
                    if (mMaterialProgressDialog == null) {
                        mMaterialProgressDialog = new MaterialDialog.Builder(getActivity())
                                .title(R.string.progress_dialog_title)
                                .content(R.string.progress_dialog_loading_msg)
                                .progress(true, 0)
                                .cancelable(false)
                                .build();
                    }
                    mMaterialProgressDialog.show();

                    syncMasterData();
                } else {
                    AlertDialogManager.showNoticeAlertDialog(getActivity(), getString(R.string.error), getString(R.string.no_connection));
                }
            }
        });

        return rootView;
    }

    private void syncMasterData() {
        mCounterTask = 4;
        mErrorMsg = null;

        if (mSyncMasterDataUtility == null) {
            mSyncMasterDataUtility = new SyncMasterDataUtility(getActivity());
        }
        else if (mSyncMasterDataUtility.mRealm == null || mSyncMasterDataUtility.mRealm.isClosed()) {
            mSyncMasterDataUtility.mRealm = Realm.getDefaultInstance();
        }

        mSyncMasterDataUtility.syncProduct(new SyncProductListener() {
            @Override
            public void onSuccess() {
                Log.d("GSB", "Sync Product success");
                mCounterTask--;
                allTasksAreFinished();
            }

            @Override
            public void onFailure(String error_msg) {
                Log.d("GSB", "Sync Product fail: " + error_msg);
                mCounterTask--;
                mErrorMsg = error_msg;
                allTasksAreFinished();
            }
        });

        mSyncMasterDataUtility.syncStockWarehouse(new SyncStockWarehouseListener() {
            @Override
            public void onSuccess() {
                Log.d("GSB", "Sync Warehouse success");
                mCounterTask--;
                allTasksAreFinished();
            }

            @Override
            public void onFailure(String error_msg) {
                Log.d("GSB", "Sync Warehouse fail: " + error_msg);
                mCounterTask--;
                mErrorMsg = error_msg;
                allTasksAreFinished();
            }
        });

        mSyncMasterDataUtility.syncStockLocation(new SyncStockLocationListener() {
            @Override
            public void onSuccess() {
                Log.d("GSB", "Sync Location success");
                mCounterTask--;
                allTasksAreFinished();
            }

            @Override
            public void onFailure(String error_msg) {
                Log.d("GSB", "Sync Location fail: " + error_msg);
                mCounterTask--;
                mErrorMsg = error_msg;
                allTasksAreFinished();
            }
        });

        mSyncMasterDataUtility.syncSetting(new SyncSettingListener() {
            @Override
            public void onSuccess() {
                Log.d("GSB", "Sync Setting success");
                mCounterTask--;
                allTasksAreFinished();
            }

            @Override
            public void onFailure(String error_msg) {
                Log.d("GSB", "Sync Setting fail: " + error_msg);
                mCounterTask--;
                mErrorMsg = error_msg;
                allTasksAreFinished();
            }
        });
    }

    private void allTasksAreFinished() {
        if (mCounterTask == 0) { // All tasks are finished.
            mMaterialProgressDialog.dismiss();

            if (mErrorMsg == null) { // Success
                AlertDialogManager.showNoticeAlertDialog(getActivity(), getString(R.string.progress_dialog_title), getString(R.string.sync_master_data_success));

                String currentDateAndTime = DateHelper.getCurrentDateTime("dd/MM/yyyy HH:mm", "Asia/Bangkok");
                SharedData.setStringForKey(getActivity(), Constant.LATEST_SYNC_MASTER_DATA_KEY, currentDateAndTime);
                mLastUpdateDatetimeTv.setText(currentDateAndTime);
                Log.d("GSB", "Realm Path: " + mSyncMasterDataUtility.mRealm.getPath());
            }
            else { // Some task is failed.
                AlertDialogManager.showNoticeAlertDialog(getActivity(), getString(R.string.error), mErrorMsg);
            }

            if (mSyncMasterDataUtility.mRealm != null) {
                mSyncMasterDataUtility.mRealm.close();
            }
        }
    }
}
