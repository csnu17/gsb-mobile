package th.co.trinityroots.gsbwarehouse.utility;

/**
 * Contain parameters use when XMLRPC call method will be executed.
 */
public class CallParams {

    public static String serverAddress;
    public static String database;
    public static String password;
    public static String uid;
    public static int USER_ID;

}
