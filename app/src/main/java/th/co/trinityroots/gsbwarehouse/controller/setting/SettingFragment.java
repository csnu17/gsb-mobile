package th.co.trinityroots.gsbwarehouse.controller.setting;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import th.co.trinityroots.gsbwarehouse.R;

public class SettingFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_setting, container, false);

        if (rootView.findViewById(R.id.setting_fragment_container) != null) {
            if (savedInstanceState != null) {
                return null;
            }

            FirstSettingFragment f = new FirstSettingFragment();
            getChildFragmentManager().beginTransaction().add(R.id.setting_fragment_container, f).commit();
        }

        return rootView;
    }

    public void swapChildFragment(Fragment childFragment) {
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.replace(R.id.setting_fragment_container, childFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

}
