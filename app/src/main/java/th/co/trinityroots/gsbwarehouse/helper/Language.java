package th.co.trinityroots.gsbwarehouse.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.util.Log;

import java.util.Locale;

/**
 * Language class
 */
public class Language {

    public static final String LANG_TH = "th";
    public static final String LANG_EN = "en";
    public static final String LANG_PREF_KEY = "GSBLanguageKey";
    private static final String MyPreferences = "MyPrefs";

    public static void changeLang(Context context, String lang) {
        if (lang == null || lang.trim().equalsIgnoreCase("")) {
            return ;
        }

        Locale myLocale = new Locale(lang);
        Locale.setDefault(myLocale);
        Configuration config = new Configuration();
        config.locale = myLocale;
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());

        saveLocale(context, lang);
    }

    private static void saveLocale(Context context, String lang) {
        SharedPreferences prefs = context.getSharedPreferences(MyPreferences, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(LANG_PREF_KEY, lang);
        editor.apply();

        Log.d("GSB", "Current language = " + lang);
    }

    public static void loadLocale(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(MyPreferences, Context.MODE_PRIVATE);
        String language = prefs.getString(LANG_PREF_KEY, LANG_TH);
        changeLang(context, language);
    }

    public static boolean isTH(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(MyPreferences, Context.MODE_PRIVATE);
        String language = prefs.getString(LANG_PREF_KEY, "");

        return LANG_TH.equalsIgnoreCase(language);
    }

}
