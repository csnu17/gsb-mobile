package th.co.trinityroots.gsbwarehouse.interfaces;

public interface CreateIncomingCallBack {
    void onSuccess();
    void onFailure(String error_msg);
}
