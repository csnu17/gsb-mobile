package th.co.trinityroots.gsbwarehouse.controller;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import io.realm.Realm;
import io.realm.RealmResults;
import th.co.trinityroots.gsbwarehouse.R;
import th.co.trinityroots.gsbwarehouse.controller.check_product.CheckProductFragment;
import th.co.trinityroots.gsbwarehouse.controller.outgoing_product.OutgoingProductFragment;
import th.co.trinityroots.gsbwarehouse.controller.setting.SettingFragment;
import th.co.trinityroots.gsbwarehouse.controller.sync_master_data.SyncMasterDataFragment;
import th.co.trinityroots.gsbwarehouse.enums.ProductAction;
import th.co.trinityroots.gsbwarehouse.helper.AlertDialogManager;
import th.co.trinityroots.gsbwarehouse.helper.BitmapHelper;
import th.co.trinityroots.gsbwarehouse.helper.CacheManager;
import th.co.trinityroots.gsbwarehouse.helper.DatabaseManager;
import th.co.trinityroots.gsbwarehouse.helper.SharedData;
import th.co.trinityroots.gsbwarehouse.helper.ViewHelper;
import th.co.trinityroots.gsbwarehouse.model.Partner;
import th.co.trinityroots.gsbwarehouse.model.ProductTemplate;
import th.co.trinityroots.gsbwarehouse.model.Setting;
import th.co.trinityroots.gsbwarehouse.model.StockLocation;
import th.co.trinityroots.gsbwarehouse.model.StockWarehouse;
import th.co.trinityroots.gsbwarehouse.utility.CallParams;
import th.co.trinityroots.gsbwarehouse.utility.Constant;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout mDrawer;
    private Toolbar mToolbar;
    private NavigationView mNavigationView;
    private AlertDialog mAlertDialogConfirmLogout;

    private Partner mPartner;
    private int selectedItemId;
    private ProductAction mProductAction;

    private long mBackPressed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mPartner = getIntent().getParcelableExtra(Constant.PARTNER_PUT_EXTRA_KEY);

        CallParams.serverAddress = SharedData.getStringFromKey(this, Constant.SERVER_ADDRESS_KEY);
        CallParams.database = SharedData.getStringFromKey(this, Constant.DATABASE_KEY);
        CallParams.password = SharedData.getStringFromKey(this, Constant.PASSWORD_KEY);
        CallParams.uid = SharedData.getStringFromKey(this, Constant.UID_KEY);
        CallParams.USER_ID = Integer.parseInt(CallParams.uid);

        Realm realm = Realm.getDefaultInstance();

        // Notify user is they still don't sync master data or found error
        RealmResults<ProductTemplate> productTemplates = realm.where(ProductTemplate.class).findAll();
        RealmResults<StockWarehouse> stockWarehouses = realm.where(StockWarehouse.class).findAll();
        RealmResults<StockLocation> stockLocations = realm.where(StockLocation.class).findAll();
        RealmResults<Setting> settings = realm.where(Setting.class).findAll();
        if (productTemplates.size() == 0 || stockWarehouses.size() == 0 ||
                stockLocations.size() == 0 || settings.size() == 0) {
            AlertDialogManager.showNoticeAlertDialog(this, getString(R.string.app_name), getString(R.string.master_data_not_found));
        }
        realm.close();

        setupView();
        addFirstFragment(savedInstanceState);

        initialRealmObject();
    }

    private void initialRealmObject() {
        DatabaseManager databaseManager = new DatabaseManager();
        databaseManager.createOrUpdateProductAction();
        databaseManager.createDefaultRatioIfNeed(CallParams.USER_ID);
        databaseManager.createOrUpdateReferenceOrderStatus();
        databaseManager.close();
    }

    private void setupView() {
        selectedItemId = R.id.nav_incoming_product;

        // Navigation side menu
        mNavigationView = (NavigationView) findViewById(R.id.nav_view);
        mNavigationView.setNavigationItemSelectedListener(this);

        // Set default selected menu item
        MenuItem selectedItem = mNavigationView.getMenu().findItem(selectedItemId);
        selectedItem.setChecked(true);

        // Set value for view
        TextView mNameTv = (TextView) mNavigationView.getHeaderView(0).findViewById(R.id.nameTv);
        mNameTv.setText(mPartner.getName());
        TextView mEmailTv = (TextView) mNavigationView.getHeaderView(0).findViewById(R.id.emailTv);
        mEmailTv.setText(mPartner.getEmail());

        ImageView profileImv = (ImageView) mNavigationView.getHeaderView(0).findViewById(R.id.profileImv);
        Bitmap bitmap = BitmapHelper.getBitmapImage(mPartner.getImage_medium());
        if (bitmap != null) {
            profileImv.setImageBitmap(bitmap);
        }

        // Toolbar
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle(selectedItem.getTitle());
        setSupportActionBar(mToolbar);

        // Drawer
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawer, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            @Override
            public void onDrawerStateChanged(int newState) {
                super.onDrawerStateChanged(newState);

                if (getCurrentFocus() != null) {
                    ViewHelper viewHelper = new ViewHelper(MainActivity.this);
                    viewHelper.hideSoftKeyboard(getCurrentFocus());
                }
            }
        };
        mDrawer.setDrawerListener(toggle);
        toggle.syncState();
    }

    private void addFirstFragment(Bundle savedInstanceState) {
        // Check that the activity is using the layout version with
        // the fragment_container FrameLayout
        if (findViewById(R.id.fragment_container) != null) {
            // However, if we're being restored from a previous state,
            // then we don't need to do anything and should return or else
            // we could end up with overlapping fragments.
            if (savedInstanceState != null) {
                return;
            }

            // Create a new Fragment to be placed in the activity layout
            Fragment firstFragment = getFragmentFromSelectedItemId();
            if (firstFragment == null) {
                return;
            }

            // Add the fragment to the 'fragment_container' FrameLayout
            getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, firstFragment).commit();
        }
    }

    public void swapFragment() {
        Fragment mTargetFragment = getFragmentFromSelectedItemId();
        if (mTargetFragment == null) {
            return;
        }

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack so the user can navigate back
        transaction.replace(R.id.fragment_container, mTargetFragment);
//        transaction.addToBackStack(null);
        // Commit the transaction
        transaction.commit();
    }

    private Fragment getFragmentFromSelectedItemId() {
        switch (selectedItemId) {
            case R.id.nav_incoming_product:
                mProductAction = ProductAction.INCOMING_SHIPMENTS;
                return new CheckProductFragment(); // reuse from Inventory Adjustment

            case R.id.nav_outgoing_product:
                mProductAction = ProductAction.DELIVERY_ORDERS;
                return new OutgoingProductFragment();

            case R.id.nav_check_product:
                mProductAction = ProductAction.INVENTORY_ADJUSTMENTS;
                return new CheckProductFragment();

            case R.id.nav_sync_data:
                return new SyncMasterDataFragment();

            case R.id.nav_setting:
                return new SettingFragment();

            default: return null;
        }
    }

    @Override
    public void onBackPressed() {
        if (mDrawer.isDrawerOpen(GravityCompat.START)) {
            mDrawer.closeDrawer(GravityCompat.START);
        } else {
            FragmentManager fm = getSupportFragmentManager();
            for (Fragment frag : fm.getFragments()) {
                if (frag != null && frag.isVisible()) {
                    FragmentManager childFm = frag.getChildFragmentManager();
                    if (childFm.getBackStackEntryCount() > 0) {
                        childFm.popBackStack();
                        return;
                    }
                }
            }

            if (mBackPressed + Constant.TIME_INTERVAL > System.currentTimeMillis()) {
                super.onBackPressed();
                return;
            }
            else {
                Toast.makeText(this, getString(R.string.confirm_exit_app), Toast.LENGTH_SHORT).show();
            }
            mBackPressed = System.currentTimeMillis();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int itemId = item.getItemId();
        switch (itemId) {
            case R.id.nav_incoming_product:
            case R.id.nav_outgoing_product:
            case R.id.nav_check_product:
            case R.id.nav_sync_data:
            case R.id.nav_setting:
                mDrawer.closeDrawer(GravityCompat.START);
                mToolbar.setTitle(item.getTitle());
                selectedItemId = itemId;
                swapFragment();
                break;

            case R.id.nav_logout:
                confirmLogout();
        }

        return true;
    }

    private void confirmLogout() {
        // Create dialog builder
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // Set property
        alertDialogBuilder.setTitle(getString(R.string.confirm_logout_title))
                .setMessage(getString(R.string.confirm_logout_msg))
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        performLogoutProcess();
                    }
                })
                .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mAlertDialogConfirmLogout.cancel();
                    }
                });

        // create alert dialog
        mAlertDialogConfirmLogout = alertDialogBuilder.create();
        mAlertDialogConfirmLogout.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                mNavigationView.setCheckedItem(selectedItemId);
            }
        });

        // show it
        mAlertDialogConfirmLogout.show();
    }

    private void performLogoutProcess() {
        CacheManager.clearCache(this);

        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    public void updateLabel() {
        // Get all menus
        MenuItem incoming_product_item = mNavigationView.getMenu().findItem(R.id.nav_incoming_product);
        MenuItem outgoing_product_item = mNavigationView.getMenu().findItem(R.id.nav_outgoing_product);
        MenuItem check_product_item = mNavigationView.getMenu().findItem(R.id.nav_check_product);
        MenuItem sync_data_item = mNavigationView.getMenu().findItem(R.id.nav_sync_data);
        MenuItem setting_item = mNavigationView.getMenu().findItem(R.id.nav_setting);
        MenuItem logout_item = mNavigationView.getMenu().findItem(R.id.nav_logout);

        // Change text for all menus
        incoming_product_item.setTitle(getString(R.string.nav_incoming_product));
        outgoing_product_item.setTitle(getString(R.string.nav_outgoing_product));
        check_product_item.setTitle(getString(R.string.nav_check_product));
        sync_data_item.setTitle(getString(R.string.nav_sync_data));
        setting_item.setTitle(getString(R.string.nav_setting));
        logout_item.setTitle(getString(R.string.nav_logout));

        // Change title for Toolbar to current active menu's title.
        MenuItem selectedItem = mNavigationView.getMenu().findItem(selectedItemId);
        mToolbar.setTitle(selectedItem.getTitle());
    }

    public void setToolbarTitle(String title) {
        mToolbar.setTitle(title);
    }

    public ProductAction getProductAction() {
        return mProductAction;
    }

}
