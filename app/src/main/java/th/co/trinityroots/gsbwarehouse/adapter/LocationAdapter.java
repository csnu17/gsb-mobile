package th.co.trinityroots.gsbwarehouse.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import io.realm.RealmResults;
import th.co.trinityroots.gsbwarehouse.R;
import th.co.trinityroots.gsbwarehouse.model.StockLocation;

public class LocationAdapter extends BaseAdapter {

    private List<StockLocation> locations = Collections.emptyList();
    private final LayoutInflater inflater;

    public LocationAdapter(Context context) {
        this.inflater = LayoutInflater.from(context);
    }

    public void updateList(RealmResults<StockLocation> locations) {
        locations.sort("name");
        this.locations = locations;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return locations.size();
    }

    @Override
    public StockLocation getItem(int i) {
        return locations.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.location_list_item, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        StockLocation item = getItem(position);
        viewHolder.name.setText(item.getName());

        return convertView;
    }

    private class ViewHolder {
        TextView name;

        public ViewHolder(View convertView) {
            name = (TextView) convertView.findViewById(R.id.mLocationNameTv);
        }
    }
}
