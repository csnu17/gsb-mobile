package th.co.trinityroots.gsbwarehouse.interfaces;

public interface SyncStockLocationListener {
    void onSuccess();
    void onFailure(String error_msg);
}
